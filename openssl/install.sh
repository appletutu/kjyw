#!/bin/bash
clear
echo ------------------------------------------
echo        CentOS7 openssl升级到1.1.1d
echo        生产环境使用前请做好测试
echo        https://www.openssl.org/source/
echo ------------------------------------------
sleep 3s
echo $(date +%F-%T) 安装openssl进程开始
sleep 1s
clear
echo step1:下载openssl
wget https://www.openssl.org/source/openssl-1.1.1s.tar.gz
sleep 3s
echo step2:安装依赖gcc make  perl
yum install -y gcc make  perl
echo step3:解压openssl
tar zxvf openssl-1.1.1s.tar.gz
echo step3:切换到编译目录进行编译安装openssl
cd openssl-1.1.1s
./config --prefix=/usr/local/openssl
make
make install
echo step4:切换到上层目录查看openssl版本
cd ..
openssl version
echo $(date +%F-%T)  安装openssl结束.
echo.
echo.
echo.
echo $(date +%F-%T) 配置openssl
echo 备份原来的openssl
mv /usr/bin/openssl /usr/bin/openssl.bak
mv /usr/lib64/openssl /usr/lib64/openssl.bak
mv /usr/lib64/libssl.so /usr/lib64/libssl.so.bak
mv /usr/include/openssl /usr/include/openssl.bak

#添加软连接
ln -s /usr/local/openssl/bin/openssl /usr/bin/openssl
ln -s /usr/local/openssl/include/openssl /usr/include/openssl
ln -s /usr/local/openssl/lib/libssl.so /usr/lib64/libssl.so
echo "/usr/local/openssl/lib" >> /etc/ld.so.conf
ln -s /usr/local/openssl/lib/libssl.so.a /usr/lib64/libssl.so.1.1
ln -s /usr/local/openssl/lib/libcrypto.so.q /usr/lib64/libcrypto.so.1.1
ln -sf /usr/local/openssl/lib/libssl.so.1.1 /usr/lib64/libssl.so.1.1
ln -sf /usr/local/openssl/lib/libcrypto.so.1.1 /usr/lib64/libcrypto.so.1.1


ldconfig -v
openssl version
