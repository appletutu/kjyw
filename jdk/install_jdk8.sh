#!/bin/bash
#卸载环境中存在的jdk版本
for i in $(rpm -qa | grep jdk | grep -v grep)
do
  echo "Deleting rpm -> "$i
  rpm -e --nodeps $i
done
 
if [[ ! -z $(rpm -qa | grep jdk | grep -v grep) ]];
then
  echo "-->Failed to remove the defult Jdk."
else
  tar -zxvf jdk-8u291-linux-x64.tar.gz -C /usr/local/
  #配置环境变量
  if ! grep "JAVA_HOME=/usr/local/jdk1.8.0_291" /etc/profile
  then
      echo "export JAVA_HOME=/usr/local/jdk1.8.0_291" >>/etc/profile
      echo -e 'export PATH=$JAVA_HOME/bin:$PATH'>>/etc/profile
      echo -e 'export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar'>>/etc/profile
      source /etc/profile
  fi
fi
echo "java version:"
java -version
