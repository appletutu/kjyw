#!/bin/bash


echo "### 检测 JAVA 环境 ###"
if [ -x "$JAVA_HOME/bin/java" ]; then
  JAVA="$JAVA_HOME/bin/java"
else
  JAVA=`which java`
fi

if [ ! -x "$JAVA" ]; then
  echo " JAVA 未安装，请先安装 JAVA 并设置 JAVA_HOME "
  exit 1
else
  echo "### JAVA_INSTALL_DIR :$JAVA ###"
fi