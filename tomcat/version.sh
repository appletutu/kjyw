#! /bin/sh
echo 'Author            :liangping'
echo 'Version           :3.0'
echo 'Last modified     :2022-06-09 09:30'
echo 'Description       :检查当前 tomcat、war版本和更新时间'
echo 'execute command   :命令行参数赋值:sh version.sh'

TOMCATROOT_PATH=$(cd $(dirname $0); pwd)
for tomcatPath in $TOMCATROOT_PATH/*
do
    if [ -d "$tomcatPath" ]
      then
        echo '※※※※※※※※※※※※※※※※※※※※※※※※※ 开始检查：'${tomcatPath}
        echo '- Tomcat 版本：'
        ${tomcatPath}/bin/version.sh | grep 'Server number:'
        sleep 2
        echo '- Tomcat 配置：'
        cat ${tomcatPath}/conf/server.xml | grep yunmas | tr -s '\n'
        sleep 2
        echo '- yunmas war 包修改时间：'
        stat ${tomcatPath}/yunmasapp/* | grep -E "Modify|File"
        sleep 2
        echo '- yunmas war 包部署时间：'
        stat ${tomcatPath}/webapps/* | grep -E "Modify|File"
    fi
done
echo '※※※※※※※※※※※※※※※※※※※※※※※※※ end.'


