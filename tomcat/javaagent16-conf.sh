#!/bin/sh
TOMCATROOT_PATH=$(cd $(dirname $0); pwd)
for tomcatPath in $TOMCATROOT_PATH/*
do
  if [ -d "$tomcatPath" ]
    then
      echo '※ 开始配置 JAVA_OPTS：'${tomcatPath}
      sed -i "125iJAVA_OPTS='-server -Xmx2g -Xms2g -Xmn1g -XX:+UseParallelGC -XX:-UseAdaptiveSizePolicy -XX:SurvivorRatio=6 -XX:MetaspaceSize=256m -XX:MaxMetaspaceSize=256m -javaagent:${tomcatPath}/javaagent/jmx_prometheus_javaagent-0.16.1.jar=30018:${tomcatPath}/javaagent/tomcat.yaml'" ${tomcatPath}/bin/catalina.sh
      sleep 2
      echo '※ JAVA_OPTS 配置结束.'
  fi
done
echo '※ all clean end.'


