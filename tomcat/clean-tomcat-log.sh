#!/bin/bash
TOMCATROOT_PATH=$(cd $(dirname $0); pwd)
for tomcatPath in $TOMCATROOT_PATH/*
do
  if [ -d "$tomcatPath" ]
    then
      echo '※ 开始清理日志：'${tomcatPath}
      find ${tomcatPath}/logs -name "*.*" -exec rm -rf {} \;
      cat /dev/null > ${tomcatPath}/logs/catalina.out
      sleep 2
      echo '※ 日志清理结束.'
  fi
done
echo '※ all clean end.'

