echo '步骤1 停止全部 tomcat 服务...'
/home/yunmas/tm-stop-all
sleep 1
echo '步骤2 覆盖升级 tomcat...'
/home/yunmas/appBase/update-tomcat2.sh
sleep 2
echo '步骤3 升级 yunmas_redis.war...'
/home/yunmas/deploy-yunmas-server2 t0 redis
sleep 2
echo '步骤4 升级 yunmas_smstime.war...'
/home/yunmas/deploy-yunmas-server2 t1 smstimer
sleep 3
echo '步骤5 重启全部 tomcat0-redis 服务...'
/home/yunmas/tm-restart-redis.sh t0
sleep 1
echo '步骤5 重启全部 tomcat1-smstimer 服务...'
/home/yunmas/tm-restart-smstimer.sh t1
