#! /bin/sh
echo 'Author            :liangping'
echo 'Version           :3.0'
echo 'Last modified     :2022-06-09 09:30'
echo 'Description       :批量停止当前目录下的 tomcat'
echo 'execute command   :命令行参数赋值:sh stop.sh'


tomcat_pid=`ps -ef | grep tomcat | grep -v grep | awk '{print $2}'`
for i in $tomcat_pid 
do
        kill -9 $i 2>&1 >/dev/null
        echo "Process ID:$i Shutdown Successfully"
done


