#! /bin/sh
echo 'Author            :liangping'
echo 'Version           :3.0'
echo 'Last modified     :2022-06-09 09:30'
echo 'Description       :批量启动当前目录下的 tomcat'
echo 'execute command   :命令行参数赋值:sh start.sh'


TOMCATROOT_PATH=$(cd $(dirname $0); pwd)
for tomcatPath in $TOMCATROOT_PATH/*
do
    if [ -d "$tomcatPath" ]
       then
            ${tomcatPath}/bin/startup.sh #启动tomcat服务
            sleep 2
            echo  ${tomcatPath} '启动成功'
    else
            echo '不存在 tomcatPath！'
    fi
done

ps -ef | grep tomcat | grep -v grep | awk '{print $1" "$2" "$5" "$7" "$8" "$27" "$31}'

