#! /bin/sh
echo -e "\n"
TOMCATROOT_PATH=$(cd $(dirname $0); pwd)
for tomcatPath in $TOMCATROOT_PATH/*
do
  if [ -d "$tomcatPath" ]
    then
      echo '※※※※※※※※※※※※※※※※※※※※※※※※※ 开始检查：'${tomcatPath}
      echo '- Tomcat 版本：'
      ${tomcatPath}/bin/version.sh | grep 'Server number:'
      sleep 2
      echo '- Tomcat 配置：'
      cat ${tomcatPath}/conf/server.xml | grep yunmas | tr -s '\n' | sed 's/^[ \t]*//g'
      cat ${tomcatPath}/conf/server.xml | grep port | tr -s '\n' | sed 's/^[ \t]*//g'
      sleep 2
      echo '- yunmas war 包修改时间：'
      stat ${tomcatPath}/yunmasapp/* | grep -E "Modify|File"
      sleep 2
      echo '- yunmas war 包部署时间：'
      stat ${tomcatPath}/webapps/* | grep -E "Modify|File"
      echo '※※※※※※※※※※※※※※※※※※※※※※※※※ ' ${tomcatPath} '检查结束.'
      echo -e "\n"
  fi
done
echo '※※※※※※※※※※※※※※※※※※※※※※※※※ all check end.'


