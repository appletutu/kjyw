#!/bin/bash
# operating environment ：CentOS Linux release 7.9.2009 (Core)
# chkconfig: 2345 80 50
# Auther:liangp

# 解压 tomcat 安装包（yunmas-tomcat.tar 需要提前删除server.xml，防止覆盖生产上的配置）
#tar -xvf yunmas-tomcat-*.tar
# -o覆盖不提示 -q不显示解压过程 -d制定解压目录
unzip -oq yunmas-tomcat-*.zip -d yunmas-tomcat

# 循环复制更新 tomcat
TOMCATROOT_PATH="/home/yunmas/tomcat1/*"
for TOMCATINSTANCE_PATH in $TOMCATROOT_PATH
do
    if [ -d "$TOMCATINSTANCE_PATH" ];then
        # 清理 tomcat 缓存

        rm -rf $TOMCATINSTANCE_PATH/work/*
        rm -rf $TOMCATINSTANCE_PATH/temp/*

        # 更新 Tomcat （新版覆盖方式）
        cp -rf yunmas-tomcat/* $TOMCATINSTANCE_PATH/
        chmod -R ug+x $TOMCATINSTANCE_PATH//bin/*.sh
        echo -e "$TOMCATINSTANCE_PATH update complete "
    # else
    #     echo -e "$TOMCATINSTANCE_PATH  not a directory"
    fi
done
echo -e "========== All updates complete. =========="



