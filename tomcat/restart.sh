#! /bin/sh
#! /bin/sh
echo 'Author            :liangping'
echo 'Version           :3.0'
echo 'Last modified     :2022-06-09 09:30'
echo 'Description       :批量重启当前目录下的 tomcat'
echo 'execute command   :命令行参数赋值:sh restart.sh'

sh stop.sh
sleep 2
sh start.sh