#! /bin/sh
echo 'Author            :liangping'
echo 'Version           :3.0'
echo 'Last modified     :2022-06-09 09:30'
echo 'Description       :查看 tomcat 日志'
echo 'execute command   :命令行参数赋值:sh start.sh'


TOMCATROOT_PATH=$PWD
case "$1" in
0)
    tail -f $TOMCATROOT_PATH/tomcat0/logs/catalina.out
;;
1)
    tail -f $TOMCATROOT_PATH/tomcat1/logs/catalina.out
;;
2)
    tail -f $TOMCATROOT_PATH/tomcat2/logs/catalina.out
;;
3)
    tail -f $TOMCATROOT_PATH/tomcat3/logs/catalina.out
;;
*)
    echo '输入参数有误，请输入 sh log.sh {0|1|2|3} '
;;
esac

