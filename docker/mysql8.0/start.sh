docker run \
  --privileged=true \
  -d \
  -p 3306:3306 \
  -e TZ=Asia/Shanghai \
  -e LANG=C.UTF-8 \
  --name mysql8 \
  -v $PWD/mysql_data:/var/lib/mysql \
  -v $PWD/my.cnf:/etc/mysql/my.cnf \
  -e MYSQL_ROOT_PASSWORD=123456 \
  --restart=always \
  mysql:8.0.33
