docker run \
  -d \
  --privileged=true \
  --name rmqbroker-demo \
  -e "NAMESRV_ADDR=192.168.0.225:9876" \
  -e "BROKER_IP1=192.168.0.225" \
  -e "BROKER_NAME=broker-a" \
  -e "BROKER_PORT=10911" \
  -e "MAX_POSSIBLE_HEAP=200000000" \
  -e TZ=Asia/Shanghai \
  -v /usr/share/zoneinfo/Asia/Shanghai/:/usr/share/zoneinfo/Asia/Shanghai/ \
  -v $PWD/broker/logs:/root/logs \
  -v $PWD/broker/store:/root/store \
  --restart=always \
  --net=host \
  apache/rocketmq:4.9.7 sh mqbroker autoCreateTopicEnable=true

