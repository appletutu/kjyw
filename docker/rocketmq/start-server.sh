docker run \
  -d \
  --privileged=true \
  --name rmqnameserver-demo \
  -p 9876:9876 \
  -e TZ=Asia/Shanghai \
  -v /usr/share/zoneinfo/Asia/Shanghai/:/usr/share/zoneinfo/Asia/Shanghai/ \
  -v $PWD/nameserver/logs:/root/logs \
  -v $PWD/nameserver/store:/root/store \
  -e "MAX_POSSIBLE_HEAP=100000000" \
  --restart=always \
  apache/rocketmq:4.9.7 sh mqnamesrv autoCreateTopicEnable=true
