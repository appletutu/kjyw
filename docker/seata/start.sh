docker run \
  --name seata-demo \
  -d \
  -e SEATA_IP=192.168.0.225 \
  -e SEATA_PORT=8091 \
  -v $PWD/conf/registry.conf:/seata-server/resources/registry.conf \
  -v $PWD/conf/file.conf:/seata-server/resources/file.conf \
  -v $PWD/logs:/root/logs \
  --privileged=true \
  --net=host \
 seataio/seata-server:2.0.0
