docker run \
  -d \
  --name influxdb-demo \
  --network=host \
  --restart always \
  -e DOCKER_INFLUXDB_INIT_USERNAME=influxdb  \
  -e DOCKER_INFLUXDB_INIT_PASSWORD=olp5dJU1rvuyTNZj \
  --privileged=true \
  -v $PWD/data:/var/lib/influxdb \
  -td influxdb:1.8.0
