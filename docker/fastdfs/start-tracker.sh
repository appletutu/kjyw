docker run \
  -d \
  --network=host \
  --name fastdfs-tracker \
  -v $PWD/tracker:/var/fdfs \
  --restart=always \
  fastdfs:5.11 tracker
