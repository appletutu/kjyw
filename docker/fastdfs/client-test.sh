#1.进入容器查看或修改配置
docker exec -it fastdfs-tracker bash
cd /etc/fdfs/
ls
vi client.conf           # 修改配置
fdfs_monitor client.conf # 测试连接

docker exec -it fastdfs-storage bash
cd /etc/fdfs/ 
vi storage.conf          # 修改默认端口，默认端口是8888，也可以不进行更改。

cd /usr/local/nginx
vi nginx.conf            # 如果上边改了默认端口，这里也要作相应修改
#listen 8888;

docker stop storage
docker start storage


#2.开放8888,22122,23000端口,云服务器防火墙策略也要开(注意:如果关闭防火墙,云服务器策略也要开)
firewall-cmd --zone=public --add-port=8888/tcp --permanent
firewall-cmd --zone=public --add-port=22122/tcp --permanent
firewall-cmd --zone=public --add-port=23000/tcp --permanent

firewall-cmd --reload
firewall-cmd --zone=public --list-ports
systemctl restart firewalld  # 重启防火墙
systemctl start firewalld    # 开启防火墙
systemctl status firewalld   # 防火墙状态

####Windows下添加防火墙策略
netsh advfirewall firewall add rule name="Allow TCP Port 8888" protocol=TCP dir=in localport=8888 action=allow
netsh advfirewall firewall add rule name="Allow TCP Port 22122" protocol=TCP dir=in localport=22122 action=allow
netsh advfirewall firewall add rule name="Allow TCP Port 23000" protocol=TCP dir=in localport=23000 action=allow

sc stop MpsSvc
sc start MpsSvc
sc stop MpsSvc && sc start MpsSvc


#3.测试文件上传下载
# 测试：浏览器访问 http:192.168.0.111:8888
# 进入容器
docker exec -it tracker bash
# 创建测试文件
echo "this is a test txt" > test.txt
# 上传至服务器
fdfs_upload_file /etc/fdfs/client.test.txt
# 上传成功，返回：group1/M00/00/00/CtM3BF84iz2AWE_JAAAACBfWGpM793.txt
# 挂载到宿主机 data/fastdfs/storage，所以去宿主机目录看看没有上传的文件
cd data/fastdfs/storage
ls
# 测试访问文件
curl -i http://127.0.0.1:8888/group1/M00/00/00/CtM3BF84iz2AWE_JAAAACBfWGpM793.txt


# 修改Nginx配置
docker cp fastdfs-storage:/etc/nginx/conf/nginx.conf /data/fastdfs/nginx/