docker run \
  -d \
  --network=host \
  --name fastdfs-storage \
  -e TRACKER_SERVER=192.168.0.111:22122 \
  -v $PWD/storage:/var/fdfs \
  -e GROUP_NAME=group1 \
  --restart=always \
  fastdfs:5.11 storage
