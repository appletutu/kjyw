docker rm -f jenkins-huaweiyun
docker run \
  -u root \
  -d \
  -p 48081:8080 \
  -p 50000:50000 \
  --privileged \
  -e TZ=Asia/Shanghai \
  -e JAVA_TOOL_OPTIONS="-Dfile.encoding=UTF-8" \
  -v $PWD/jenkins_home:/var/jenkins_home \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /usr/bin/docker:/usr/bin/docker \
  -v /usr/sbin/obsutil:/usr/sbin/obsutil \
  -v "$HOME"/.docker:/root/.docker \
  -v /usr/lib64/libltdl.so.7:/usr/lib/x86_64-linux-gnu/libltdl.so.7 \
  -v /data/minio/minio_data:/data \
  -e JAVA_OPTS="-Dorg.jenkinsci.plugins.durabletask.BourneShellScript.HEARTBEAT_CHECK_INTERVAL=3600 -Duser.timezone=Asia/Shanghai" \
  -e DEPLOY_ENV=huaweiyun \
  --name jenkins-huaweiyun \
  --restart=always \
  jenkins/jenkins:2.369-centos7
