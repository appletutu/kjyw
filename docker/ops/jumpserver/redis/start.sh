docker run \
  -d \
  -p 6379:6379 \
  -e TZ=Asia/Shanghai \
  --name redis-jumpserver \
  -v $PWD/redis_conf/redis.conf:/etc/redis/redis.conf \
  -v $PWD/redis_data:/data \
  --restart=always \
  redis:6.2.6
