docker run \
  -d \
  -u root \
  --privileged=true \
  --name zabbix-web-nginx-pgsql \
  -t \
  -e ZBX_SERVER_HOST="192.168.0.127" \
  -e DB_SERVER_HOST="192.168.0.127" \
  -e POSTGRES_USER="zabbix" \
  -e POSTGRES_PASSWORD="Cqmyg14dss" \
  -e POSTGRES_DB="zabbix" \
  -p 38080:8080 \
  --restart=always \
  zabbix/zabbix-web-nginx-pgsql:7.0.1-centos
