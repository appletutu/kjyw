docker run \
  -d \
  --privileged=true \
  -p 3000:3000 \
  -e TZ=Asia/Shanghai \
  --name grafana \
  -v $PWD/grafana_data:/var/lib/grafana \
  --restart=always \
  grafana/grafana:11.1.4
