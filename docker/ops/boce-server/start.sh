docker run \
  -d \
  --restart=always \
  -p 3001:3001 \
  -v $PWD/uptime-kuma-data:/app/data \
  --name uptime-kuma \
  louislam/uptime-kuma:1.23.11
