docker run \
  --privileged=true \
  -d \
  -p 13306:3306 \
  -e TZ=Asia/Shanghai \
  -e LANG=C.UTF-8 \
  --name mysql5.7 \
  -v $PWD/mysql_data:/var/lib/mysql \
  -v $PWD/mysql_conf:/etc/mysql \
  -e MYSQL_ROOT_PASSWORD=3333p7bSC66660SJL \
  --restart=always \
  mysql:5.7.36


docker run \
  --privileged=true \
  -p 13306:3306 \
  -e TZ=Asia/Shanghai \
  -e LANG=C.UTF-8 \
  --name mysql5.7 \
  -v $PWD/mysql_data:/var/lib/mysql \
  -v $PWD/mysql_conf:/etc/mysql/conf.d \
  -e MYSQL_ROOT_PASSWORD=123456 \
  --restart=always \
  mysql:5.7.44