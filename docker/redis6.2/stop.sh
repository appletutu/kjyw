APP_NAME=redis-demo
docker logs ${APP_NAME} > logs/`date +%Y%m%d%H%M%S`.log 2>&1
cd logs
tar -zcvf `date +%Y%m%d%H%M%S`.log.tar.gz `date +%Y%m%d%H%M%S`.log
rm -rf `date +%Y%m%d%H%M%S`.log
sleep 3
docker stop ${APP_NAME}
docker rm -f ${APP_NAME}
