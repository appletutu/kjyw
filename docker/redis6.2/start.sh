docker run \
  -d \
  -p 6379:6379 \
  -e TZ=Asia/Shanghai \
  --name redis-demo \
  -v $PWD/redis_data:/data \
  --restart=always \
  redis:6.2.6 --requirepass "tLZ35sRUqxZYrXU3" --appendonly yes
