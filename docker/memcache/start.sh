docker run \
  -d \
  --privileged=true \
  -e TZ=Asia/Shanghai \
  -e CACHESIZE=4096 \
  --name memcached-demo \
  --restart=always \
  --net=host \
  memcached:1.6.15
