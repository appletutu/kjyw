#!/bin/bash

#-----------------------------------------------------------------------
#
# Objectives:
# 1. Return all tags for a specified image name from docker hub
# Required:
# 1. Image name
# 2. [Image tag] (default: All)
#
# Author: Liangping
# Create: 2023.7.5
#
#-----------------------------------------------------------------------
API_Url='https://hub-mirror.c.163.com/v2/library'
DEFAULT_NAME="nginx"

function Usage(){
cat << HELP

Usage: docker-tags NAME[:TAG]

docker-tags list all tags for docker image on a remote registry.

Example:
    docker-tags (default nginx)
    docker-tags nginx
    docker-tags nginx:1.15.8
HELP
}

ARG=$1
if [[ "$ARG" =~ "-h" ]];then
    Usage
    exit 0
fi

function GetTags(){
    image=$1
    tag=$2
    ret=`curl -s ${API_Url}/${image}/tags/list`
    tag_list=`echo $ret | sed -e 's/.*\[//g' -e 's/"//g' -e 's/\]\}$//g' -e 's/,/\n/g'`
    if [ -z "$tag" ];then
        echo -e "$tag_list"
    else
        echo -e "$tag_list" | grep -w "$tag"
    fi
}

if [ -z $ARG ] || [[ ${ARG:0:1} == ":" ]];then
    images=`echo $DEFAULT_NAME | awk '{print $1}' | grep -v "NAME"`
else
    images=`echo $ARG | awk -F: '{print $1}'`
fi
tag=`echo $ARG | awk -F: '{print $2}'`

for i in ${images}
do
    tags=`GetTags $i $tag`
    count=`echo $tags | wc -w`
    if [[ $count -gt 0 ]];then
        echo -e "IMAGE [$i:$tag]:"
        echo -e "$tags"
        echo
    fi
done