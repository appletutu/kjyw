#!/bin/bash

# 检查
hasMaven(){
  MAVEN_VERSION=$(mvn -version)
  echo "${MAVEN_VERSION}"
  if [[ ! $MAVEN_VERSION ]]
  then
    return 0;
  fi
  return 1;
}

hasMaven

if [ $? != 1 ]
then
  echo "Not Found maven"
  echo "Installing maven..."
  tar -zxvf apache-maven-3.8.1-bin.tar.gz -C /usr/local/
  #配置环境变量
  PROFILE=$(cat /etc/profile)
  PATH='export PATH=$PATH'
  REPLACE='export PATH=$PATH:$MAVEN_HOME/bin'
  #是否有已经存在其他环境变量的配置路径
  if [[ $PROFILE == *$PATH* ]]
  #添加变量路径
  then echo "${PROFILE/$PATH/$REPLACE}" > /etc/profile
  #向文本末尾追追加路径
  else echo "$REPLACE" >> /etc/profile
  fi
  echo "export MAVEN_HOME=/usr/local/apache-maven-3.8.1" >> /etc/profile
  tail -6 /etc/profile

  echo "conf..."
  #更新配置文件
  source /etc/profile
  #查看mvn版本
  echo "mvn version:"
  mvn -version
  
  #配置文件(配置本地库，阿里云镜像)
  echo "conf settings.xml..."
  mkdir /usr/local/apache-maven-3.8.1/repo
  mv /usr/local/apache-maven-3.8.1/conf/settings.xml /usr/local/apache-maven-3.8.1/conf/settings.xml.bak
  cp -rf settings.xml /usr/local/apache-maven-3.8.1/conf/
  mvn -version
fi

