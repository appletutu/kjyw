#!/bin/bash
###
 # @Author: liangpingguo liangping880105@163.com
 # @Date: 2024-08-20 10:42:30
 # @LastEditors: liangpingguo liangping880105@163.com
 # @LastEditTime: 2024-08-20 10:43:04
 # @FilePath: .\mysql\mysql8_install.sh
 # @Description: 自动安装 mysql8
### 
mysql_version=mysql-8.0.32-linux-glibc2.12-x86_64

install_d=/data/


groupadd mysql
useradd -G mysql -g mysql mysql

if [ -f ${mysql_version}.tar.xz ];then
echo '压缩包已存在'
else
echo '压缩包不存在'
fi

if [ -d ${install_d} ];then
echo '安装目录存在'
else
mkdir ${install_d}
fi

if [ -d ${install_d}${mysql_version} ];then
echo '已经解压'
else
tar zxvf ${mysql_version}.tar.xz -C ${install_d}
fi

mv ${install_d}${mysql_version} ${install_d}mysql

chown -R mysql:mysql ${install_d}mysql

mkdir -p ${install_d}mysql/{data,log}

${install_d}mysql/bin/mysqld  --initialize --user=mysql --datadir=${install_d}mysql/data
${install_d}mysql/bin/mysql_ssl_rsa_setup --datadir=${install_d}mysql

echo '[mysqld]' > /etc/my.cnf
echo port=3306 >> /etc/my.cnf
echo socket=/data/mysql/data/mysql.sock >> /etc/my.cnf
echo lower_case_table_names=1 >> /etc/my.cnf
echo max_connections=2000 >> /etc/my.cnf
echo max_user_connections=2000 >> /etc/my.cnf
echo basedir=/data/mysql >> /etc/my.cnf
echo datadir=/data/mysql/data >> /etc/my.cnf

cp ${install_d}mysql/support-files/mysql.server /etc/init.d/mysqld

sed -i "46c basedir=${install_d}mysql" /etc/init.d/mysqld
sed -i "47c datadir=${install_d}mysql/data" /etc/init.d/mysqld

ln -s ${install_d}mysql/data/mysql.sock /tmp/mysql.sock
chown mysql:mysql /tmp/mysql.sock

service mysqld start
