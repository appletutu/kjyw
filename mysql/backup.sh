###
 # @Author: liangpingguo liangping880105@163.com
 # @Date: 2024-06-29 15:14:20
 # @LastEditors: liangpingguo liangping880105@163.com
 # @LastEditTime: 2024-06-29 17:31:08
 # @FilePath: \kjyw\mysql\backup.sh
 # @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
### 
#!/bin/bash
#数据库信息
dbuser='root'
dbpasswd='123456'
dbname='dbname1 dbname2 dbname3'
dbaddress='192.168.1.101'
#备份时间
backtime=$(date '+%Y%m%d%H%M%S')
#备份路径
datapath=/data/backup/data
logpath=/data/backup/logs
#如果备份的路径文件夹存在就使用，否则创建
[ ! -d "$datapath"  ]  && mkdir -p "$datapath" 
[ ! -d "$logpath"  ]  && mkdir -p "$logpath" 

echo "$backtime,备份数据库表$dbname1开始" >> $logpath/log.log
#正式备份数据库
for table in $dbname;
do
    echo "$(date '+%Y-%m-%d %H:%M:%S'),$table开始备份..." >> $logpath/log.log
    /usr/bin/mysqldump -u$dbuser -p$dbpasswd -h$dbaddress $table | gzip > $datapath/$table-$backtime.dump.gz
    #备份成功后操作
    if [ "$?" == 0 ];then
        echo "$(date '+%Y-%m-%d %H:%M:%S'),$table备份成功..." >> $logpath/log.log
        cd $datapath
        #删除30天前的备份，只保存30天
        find $datapath -name "*.dump.gz" -type f -mtime +30 -exec rm -rf {} \; > /dev/null 2>&1
        echo "$(date '+%Y-%m-%d %H:%M:%S'),$table备份完成!!" >> $logpath/log.log
    else
        echo "$(date '+%Y-%m-%d %H:%M:%S'),$table备份失败!!" >> $logpath/log.log
    fi
done
