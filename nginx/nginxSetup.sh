#/bin/bash
# 卸载旧版本的openssl
yum remove -y openssl openssl-devel


# 编译环境及必备库
yum -y install zlib zlib-devel pcre pcre-devel gcc gcc-c++ libevent libevent-devel perl perl-devel unzip net-tools wget



#下载源码包openssl
cd /usr/local

wget https://www.openssl.org/source/openssl-1.0.2t.tar.gz

#解压、编译、安装三部曲openssl
mkdir openssl
tar zxf openssl-1.0.2t.tar.gz 
cd openssl-1.0.2t
./config -fPIC --prefix=/usr/local/openssl/ enable-shared && make && make install
echo "export PATH=\$PATH:/usr/local/openssl2/bin"  >>/etc/profile
source /etc/profile
echo "include /usr/local/openssl2/lib" >> /etc/ld.so.conf
ldconfig

#下载源码包nginx
cd ..
wget http://nginx.org/download/nginx-1.16.1.tar.gz


#解压、编译、安装三部曲nginx
tar zxvf nginx-1.16.1.tar.gz
cd nginx-1.16.1
./configure --prefix=/usr/local/nginx --with-http_stub_status_module --with-http_ssl_module  --with-stream --with-pcre --with-openssl=/usr/local/openssl-1.0.2t 
make
make install

#编写systemd service 文件
echo -e "[Unit]\nDescription=The NGINX HTTP and reverse proxy server\nAfter=syslog.target network.target remote-fs.target nss-lookup.target\n\n[Service]\nType=forking\nPIDFile=/usr/local/nginx/logs/nginx.pid\nExecStartPre=/usr/local/nginx/sbin/nginx -t\nExecStart=/usr/local/nginx/sbin/nginx\nExecReload=/usr/local/nginx/sbin/nginx -s reload\nExecStop=/bin/kill -s QUIT $MAINPID\nPrivateTmp=true\n\n[Install]\nWantedBy=multi-user.target" >/lib/systemd/system/nginx.service

#服务重载及开机启动及启动
systemctl daemon-reload
systemctl enable nginx.service
systemctl start nginx.service

#防火墙端口开通
firewall-cmd --permanent --zone=public --add-port=8081/tcp
firewall-cmd --reload

# 清理战场
cd ..  
rm -rf nginx-1.16.1*
rm -rf openssl-1.0.2t*