#!/bin/bash
###
 # @Author: liangpingguo liangping880105@163.com
 # @Date: 2024-09-30 13:35:50
 # @LastEditors: liangpingguo liangping880105@163.com
 # @LastEditTime: 2024-09-30 16:45:13
 # @FilePath: \deploy\conf\switch_to_std.sh
 # @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
### 


# 备份
mv ./nbjb_hvv.conf ./bak/nbjb_hvv.conf_$(date +%Y%m%d_%H%M%S)
mv ./nbjb_ssl_hvv.conf ./bak/nbjb_ssl_hvv.conf_$(date +%Y%m%d_%H%M%S)
# 切换回std标准版
cp ./std/nbjb.conf ./
cp ./std/nbjb_ssl.conf ./

# 重新加载nginx配置
nginx -s reload