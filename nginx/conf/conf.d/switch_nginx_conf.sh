#!/bin/bash

# 定义配置文件路径
conf_dir="/data/nginx/conf"
current_conf="${conf_dir}/aaa.conf"
standard_conf="${conf_dir}/aaa.conf.std"
hvv_conf="${conf_dir}/aaa.conf.hvv"

# 函数：切换到标准配置
switch_to_std() {
    if [ -f "$standard_conf" ]; then
        # 删除旧的软链接
        [ -L "$current_conf" ] && unlink "$current_conf"
        # 创建新的软链接
        ln -s "$standard_conf" "$current_conf"
        echo "Configuration files have been successfully switched to the standard version."
    else
        echo "Standard configuration file does not exist: $standard_conf"
    fi
    # 重新加载Nginx配置
    nginx -s reload
}

# 函数：切换到HVV配置
switch_to_hvv() {
    if [ -f "$hvv_conf" ]; then
        # 删除旧的软链接
        [ -L "$current_conf" ] && unlink "$current_conf"
        # 创建新的软链接
        ln -s "$hvv_conf" "$current_conf"
        echo "Configuration files have been successfully switched to the HVV version."
    else
        echo "HVV configuration file does not exist: $hvv_conf"
    fi
    # 重新加载Nginx配置
    nginx -s reload
}

# 检查参数并执行相应的函数
case "$1" in
    std)
        switch_to_std
        ;;
    hvv)
        switch_to_hvv
        ;;
    *)
        echo "Usage: $0 {std|hvv}"
        exit 1
        ;;
esac