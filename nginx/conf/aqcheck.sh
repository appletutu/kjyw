#!/bin/bash

# Nginx配置文件目录
# NGINX_CONF_DIR="/etc/nginx/conf.d"    # yum安装的配置文件存放目录
NGINX_CONF_DIR="/data/nginx/conf/vhost" # tar安装的配置文件存放目录
# 日志文件存放目录
LOG_DIR="/data/scripts/logs"
# 日志文件全路径
LOG_FILE="$LOG_DIR/check_nginx_conf_aq.log"

# 检查配置文件目录是否存在
if [ ! -d "$NGINX_CONF_DIR" ]; then
    echo "配置文件目录不存在：$NGINX_CONF_DIR"
    exit 1
fi

# 检查日志存放目录是否存在
if [ ! -d "$LOG_DIR" ]; then
    mkdir -p $LOG_DIR
fi

# 获取当前时间戳
start_time=$(date '+%Y-%m-%d %H:%M:%S')
echo "$start_time - 开始检查并注释操作" | tee -a "$LOG_FILE"

# 遍历目录下的所有配置文件，排除.sh文件
for conf_file in "$NGINX_CONF_DIR"/*; do
    # 检查是否为文件且不是.sh文件
    if [ -f "$conf_file" ] && [[ "$conf_file" != *.sh ]]; then
        echo "检查文件：$conf_file" | tee -a "$LOG_FILE"

        # 1.检查是否已经注释掉/minio的location块
        if grep -q "# *location /minio" "$conf_file"; then
            echo "1.已注释掉/minio的location块：$conf_file" | tee -a "$LOG_FILE"
        else
            # 使用sed查找并注释掉/minio的location块
            sed -i '/location \/minio/,/}/ s/^/#/' "$conf_file"

            # 检查sed命令是否执行成功
            if [ $? -eq 0 ]; then
                echo "1.已执行注释/minio的location块：$conf_file" | tee -a "$LOG_FILE"
            else
                echo "1.执行注释/minio的location块失败：$conf_file" | tee -a "$LOG_FILE"
            fi
        fi

        # 2.检查是否已经注释掉/nacos的location块
        if grep -q "# *location /nacos" "$conf_file"; then
            echo "2.已注释掉/nacos的location块：$conf_file" | tee -a "$LOG_FILE"
        else
            # 使用sed查找并注释掉/nacos的location块
            sed -i '/location \/nacos/,/}/ s/^/#/' "$conf_file"

            # 检查sed命令是否执行成功
            if [ $? -eq 0 ]; then
                echo "2.已执行注释/nacos的location块：$conf_file" | tee -a "$LOG_FILE"
            else
                echo "2.执行注释/nacos的location块失败：$conf_file" | tee -a "$LOG_FILE"
            fi
        fi

        # 3.检查是否已经注释掉/xxl-job-admin的location块
        if grep -q "# *location /xxl-job-admin" "$conf_file"; then
            echo "3.已注释掉/xxl-job-admin的location块：$conf_file" | tee -a "$LOG_FILE"
        else
            # 使用sed查找并注释掉/xxl-job-admin的location块
            sed -i '/location \/xxl-job-admin/,/}/ s/^/#/' "$conf_file"

            # 检查sed命令是否执行成功
            if [ $? -eq 0 ]; then
                echo "3.已执行注释/xxl-job-admin的location块：$conf_file" | tee -a "$LOG_FILE"
            else
                echo "3.执行注释/xxl-job-admin的location块失败：$conf_file" | tee -a "$LOG_FILE"
            fi
        fi

    fi
done
# 获取当前时间戳
end_time=$(date '+%Y-%m-%d %H:%M:%S')
echo "$end_time - 检查并注释操作完成======================" | tee -a "$LOG_FILE"
# Nginx配置重载
nginx -s reload
