#!/bin/sh

echo "### 开始安装 Elasticsearch ###"

ES_VERSION=6.8.13

echo "### ES版本号: ${ES_VERSION} ###"
echo "### 插件: head、ik ###"

ES_HOME=/data/elasticsearch
ES_PACKAGE=elasticsearch-${ES_VERSION}.tar.gz
ES_HTTP_PORT=9200
ES_TCP_PORT=9300


echo "### 创建 ${ES_HOME} 目录并解压: ${ES_PACKAGE}  ###"

mkdir -p ${ES_HOME} \
&& tar xzf ${ES_PACKAGE} -C ${ES_HOME} --strip-components=1

echo "### 添加 elasticsearch 用户 ###"
groupadd -r elasticsearch \
&& useradd -r -s /usr/sbin/nologin -M -c "Elasticsearch service user" -g elasticsearch elasticsearch \

echo "### 变更 ${ES_HOME} 所属用户 ###"
chown -R elasticsearch:elasticsearch ${ES_HOME}

echo "### ${ES_HOME}/bin/elasticsearch 添加执行权限 ###"
chmod +x ${ES_HOME}/bin/elasticsearch

echo "### 检测 JAVA 环境 ###"
if [ -x "$JAVA_HOME/bin/java" ]; then
    JAVA="$JAVA_HOME/bin/java"
else
    JAVA=`which java`
fi

if [ ! -x "$JAVA" ]; then
    echo " 安装失败, 请安装 JAVA 并设置 JAVA_HOME "
    exit 1
else
    echo "### JAVA_INSTALL_DIR :$JAVA ###"
fi

echo "### 添加服务启动脚本 /etc/init.d/elasticsearch ###"
cp ./elasticsearch-init /etc/init.d/elasticsearch
sed -i -e 's#^JAVA_HOME=$#JAVA_HOME='$JAVA_HOME'#' /etc/init.d/elasticsearch
sed -i -e 's#^ES_HOME=$#ES_HOME='$ES_HOME'#' /etc/init.d/elasticsearch
chmod +x /etc/init.d/elasticsearch

echo "### 覆盖配置文件 ###"

HTTPPORT=$(awk '/^http.port:/ {print $2}' ./elasticsearch.yml)
if [ ${HTTPPORT} ]; then
    ES_HTTP_PORT=${HTTPPORT}
    echo "### 检测到http.port: ${ES_HTTP_PORT} ###"
fi

cp ./elasticsearch.yml ${ES_HOME}/config/elasticsearch.yml

echo "### 添加开机启动 ###"
chkconfig --add elasticsearch

echo "### 安装成功 ###"

echo "### 启动 Elasticsearch ###"
service elasticsearch start

echo "### 开启防火墙端口 ${ES_HTTP_PORT} , ${ES_TCP_PORT} ### "
firewall-cmd --zone=public --add-port=${ES_HTTP_PORT}/tcp --permanent
firewall-cmd --zone=public --add-port=${ES_TCP_PORT}/tcp --permanent
firewall-cmd --reload

echo "### 可在浏览器输入以下地址测试 ###"

echo "### ES功能 ###"
echo " http://ip:port "
echo "### head插件 ###"
echo " http://ip:port/_plugin/head/ "
echo "### ik插件 ###"
echo " http://ip:port/_analyze?text=测试分词&tokenizer=ik_max_word "


