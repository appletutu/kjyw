#!/bin/bash

read -r -p ">> 请确认是否将 /home/xyyx/*.zip 解压到 /home/xyyx/pic-tmp ? [Y/n] " input

case $input in
    [yY][eE][sS]|[yY])
        echo ">>Yes"
        echo ">> start unpackage : 当前目录下的 /home/xyyx/*.zip 到 /home/xyyx/pic-tmp"
        echo ">> "
        case $1 in
            *zip)
                unzip -oO CP936 /home/xyyx/*.zip -d /home/xyyx/pic-tmp;;
            *rar)
                # unrar x $1 $Temp_Dir;;
                rar x -o+ /home/xyyx/*.rar -C /home/xyyx/pic-tmp;;
            *)
            echo ">> upkg zip or uppkg rar"
            exit 1;;
        esac       
        
        echo ">> "
        echo ">> unpackage end !!!"
        echo ">> "
        echo ">> 查看解压文件:ls -l /home/xyyx/pic-tmp"
        echo ">> "
        ls -l /home/xyyx/pic-tmp
        ;;

    [nN][oO]|[nN])
        echo ">> no"
        exit 1
        ;;

    *)
        echo "Invalid input..."
        exit 1
        ;;
esac



rm -rf /home/xyyx/*.zip
rm -rf /home/xyyx/*.rar
rm -rf /home/xyyx/pic-tmp/*