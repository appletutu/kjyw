dir=/opt
processor=xyyx-web.jar
time=$(date "+%Y%m%d%H%M%S")
total=$(ps -ef | grep $processor | grep -v grep | wc -l)
if [[ $total -ge 1 ]]; then
    ps -ef | grep $processor | grep -v grep | awk '{print "kill -9 "$2}' | sh
    sleep 2s
else
    sleep 2s
fi
if [[ ! -d $dir/logs ]]; then
    mkdir $dir/logs
fi
mv $dir/logs/nohup.log $dir/logs/nohup.log.$time
# nohup java -jar > $dir/logs/web.logs &
nohup java -jar $dir/$processor >$dir/logs/nohup.log 2>&1 &
