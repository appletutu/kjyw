#! /bin/sh
###
 # @Author: liang
 # @Date: 2024-06-29 17:08:55
 # @Description: 
### 

# jar包数组
JARS=("auth-service.jar" "basic-service.jar" "future-gateway.jar" "garden-service.jar" "operation-service.jar" "property-service.jar" "user-service.jar")
# jar包路径
JAR_PATH='/data/project'
# 日志路径
LOG_PATH='/data/project/logs'

for JAR_NAME in ${JARS[@]};do 
    # 使用pgrep查找与JAR名称相关的进程ID  
    # -f 选项允许在完整命令行中搜索字符串  
    PIDS=$(pgrep -f "$JAR_NAME")  
    #PIDS=`ps -ef | grep $JAR_NAME | grep -v grep | awk '{print $2}'`    
    #echo  $PIDS
    # 检查是否找到了进程ID  
    if [[ ! -z "$PIDS" ]]; then
        # 如果找到了多个进程，它们之间由换行符分隔，需要逐行处理  
        for PID in $PIDS; do  
            # 使用ps命令获取进程详细信息，并指定输出字段  
            # 注意：这里的-o选项的字段可能需要根据你的ps版本调整  
            # PID, PPID, CMD 可能是 1, 2, 和由多个字段组成的CMD  
            # $0 代表整行，$NF 代表最后一段词
            ps -p "$PID" -o pid,ppid,args --no-headers | awk '{print $1, $3, $NF}'  

            # 如果知道CMD字段是由空格分隔的，并且你想要第9个词
            #ps -p "$pid" -o pid,ppid,args --no-headers | awk '{split($3, cmd, " "); print $1, $2, cmd[9]}'
            echo "-------------"  
        done  
    else  
        echo "未找到与 $JAR_NAME 相关的进程。"  
    fi  
done  
  
# 所有JAR进程已检查完毕  
echo "所有JAR进程已检查完毕。"  
exit 0