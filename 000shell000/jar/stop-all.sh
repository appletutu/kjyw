#! /bin/sh

# jar包数组
JARS=("auth-service.jar" "basic-service.jar" "future-gateway.jar" "garden-service.jar" "operation-service.jar" "property-service.jar" "user-service.jar")
# jar包路径
JAR_PATH='/data/project'
# 日志路径
LOG_PATH='/data/project/logs'

for JAR_NAME in ${JARS[@]};do 
    # 使用pgrep查找与JAR名称相关的进程ID  
    # -f 选项允许在完整命令行中搜索字符串  
    PIDS=$(pgrep -f "$JAR_NAME")  
    #PIDS=`ps -ef | grep $JAR_NAME | grep -v grep | awk '{print $2}'`    
    #echo  $PIDS
    # 检查是否找到了进程ID  
    if [[ ! -z "$PIDS" ]]; then
        # 如果找到了多个进程，它们之间由换行符分隔，需要逐行处理  
        for PID in $PIDS; do  
            echo "找到与 $JAR_NAME 相关的进程，PID: $PID，正在停止..."  
            # 停止找到的进程  
            kill "$PIDS"  

            # 可选：等待进程完全停止（如果需要）  
            # 这里使用一个小循环来检查进程是否已停止  
            while pgrep -x "$PIDS" >/dev/null; do  
                echo "等待 $PIDS 进程停止..."  
                sleep 1  
            done
            echo "PID $PID 的 $JAR_NAME 进程已停止。"  
        done
    else  
        echo "未找到与 $JAR_NAME 相关的进程。"  
    fi
done

# 所有JAR进程已处理完毕  
echo "所有JAR进程已处理完毕。"  
exit 0