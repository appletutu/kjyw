#!/bin/bash

# 场景一：新增磁盘格式化为逻辑卷后扩容到VG然后扩容至LV
# 场景二：在原磁盘上直接扩容PV大小后，扩容
mount_dir=/app

print_status() {
    GREEN='\033[0;32m'
    RED='\033[0;31m'
    NC='\033[0m'
    local command=$1
    local message=$2

    echo -n "${message}"

    $command &> /dev/null

    if [ $? -eq 0 ]; then
        echo -e "\r${message}    ${GREEN}[done]${NC}"
    else
        echo -e "\r${message}    ${RED}[failed]${NC}"
    fi
}

function check_disk() {
    for file in /sys/class/scsi_host/host*
        do
            echo "- - -" > $file/scan
        done

    sleep 5

    lvmdiskscan
    if [ $? != 0 ];then
        yum -y install lvm2 >> $log_file
        lvmdiskscan
        if [ $? != 0 ];then
            return 1
        fi
    fi

    # 确认目标目录存在
    if [ ! -d "$mount_dir" ]; then
        exit 1
    fi
}

# 记录扩容前的磁盘大小
PRE_EXPANSION=$(df -hT "$mount_dir" | awk -v mp="$mount_dir" '$NF == mp {print $3, $2; exit}' | grep -oP '\b\d+G\b')

function get_disk() {

    all_disks=($(lsblk -dno NAME,TYPE | awk '$2 == "disk" && $1 ~ /^sd/ {print $1}'))
    for disk in "${all_disks[@]}"; do
        mount_points=$(lsblk -no MOUNTPOINT "/dev/$disk" | grep -v '^$')
        if [[ -z $mount_points ]]; then
            new_disks=/dev/$disk
            break
        fi
    done
    sleep 3
    driveLetter=$new_disks

    # 获取设备文件
    DEVICE=$(df "$mount_dir" | awk 'NR==2 {print $1}')

    # 获取LV和VG信息
    if [[ -n "$DEVICE" ]]; then
        LV_INFO=$(lvdisplay "$DEVICE")
        LV_NAME=$(echo "$LV_INFO" | grep "LV Name" | awk '{print $3}')
        VG_NAME=$(echo "$LV_INFO" | grep "VG Name" | awk '{print $3}')
        
        if [[ -n "$LV_NAME" && -n "$VG_NAME" ]]; then
            echo "$LV_NAME $VG_NAME"
        else
            exit 1
        fi
    else
        exit 1
    fi

    # 获取挂载点的文件系统类型
    fs_type=$(df -T | grep -w "$mount_dir" | awk '
    {
      for(i=1; i<=NF; i++) {
        if ($i == "ext3" || $i == "ext4" || $i == "xfs") {
          print $i;
          exit;
        }
      }
    }')
}

function mount_disk() {

    case "$fs_type" in
        ext4)
            format_cmd="mkfs.ext4"
            resize_cmd="resize2fs"
            ;;
        xfs)
            format_cmd="mkfs.xfs"
            resize_cmd="xfs_growfs"
            ;;
        btrfs)
            format_cmd="mkfs.btrfs"
            resize_cmd="btrfs filesystem resize"
            ;;
        *)
            exit 1
            ;;
    esac

    # 检查挂载点是否存在
    if mountpoint -q "$mount_dir"; then
        # 使用 wipefs 命令清除物理卷上的签名
        wipefs --all "$driveLetter"
        # 格式化新盘符
        sudo yes | $format_cmd "$driveLetter" || { exit 1; }
        # 扩展卷组
        sudo yes | vgextend --force "$VG_NAME" "$driveLetter" || { exit 1; }
        # 扩展逻辑卷
        sudo lvextend -l +100%FREE "/dev/$VG_NAME/$LV_NAME" || { exit 1; }
        # 调整文件系统大小
        if [ "$fs_type" = "btrfs" ]; then
            # btrfs 需要指定挂载点
            sudo $resize_cmd "$mount_dir" || { exit 1; }
        else
            sudo $resize_cmd "/dev/$VG_NAME/$LV_NAME" || { exit 1; }
        fi
    else
        return 0
    fi
    sleep 10
    POST_EXPANSION=$(df -hT "$mount_dir" | awk -v mp="$mount_dir" '$NF == mp {print $3, $2; exit}' | grep -oP '\b\d+G\b')
}

function check_expansion(){
    
    # 对比扩容前后的磁盘大小
    if [ "$PRE_EXPANSION" != "$POST_EXPANSION" ]; then
        return 0
    else
        return 1
    fi
}

print_status check_disk  "1. 开始检查磁盘状态，当前容量($mount_dir): $PRE_EXPANSION"
print_status get_disk "2. 开始获取新盘符信息"
print_status mount_disk "3. 开始将新盘符 $driveLetter 扩容至逻辑卷: $DEVICE,类型为:$fs_type"
print_status check_expansion "4. 磁盘扩容成功:存储容量$PRE_EXPANSION 扩容为 $POST_EXPANSION"