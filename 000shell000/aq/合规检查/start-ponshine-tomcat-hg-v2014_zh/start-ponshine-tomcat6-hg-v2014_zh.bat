@echo OFF
COLOR 0A
::设置回显编码方式65001（UTF-8）；936（GB2312）
CHCP 936
rem 鹏信科技window_tomcat_v2014离线采集程序
Title 鹏信科技window_tomcat_v2014离线采集程序
::-----------------------------------------bat start-----------------------------
echo 版权归属：杭州鹏信科技有限公司
echo 检查正在进行，请稍后......

for /f "delims=" %%i in ('hostname') do set n=%%i
for /f "tokens=1,2 delims=:" %%i in ('time/t') do set t=%%i%%j
set fileDate=%date:~0,4%%date:~5,2%%date:~8,2%%t:~0,4%%time:~6,2%

::调用VB脚本自动获取IP和手动输入IP
@set fVBSRandom=%random:~-5%
@set fVBS=ponshine-hg-windows-.%fVBSRandom%.vbs
@rem ---------------------- 调用VB脚本获取IP地址 ----------------------
@echo Function regTest(patern, str) >%fVBS%
@echo Dim regEx, retVal >>%fVBS%
@echo Set regEx = New RegExp >>%fVBS%
@echo regEx.Pattern = patern >>%fVBS%
@echo regEx.IgnoreCase = False >>%fVBS%
@echo retVal = regEx.Test(str) >>%fVBS%
@echo If retVal Then >>%fVBS%
@echo regTest = 1 >>%fVBS%
@echo Else >>%fVBS%
@echo regTest = 0 >>%fVBS%
@echo End If >>%fVBS%
@echo End Function >>%fVBS%
@echo Public Function getRealIpAddress() >>%fVBS%
@echo Set obj = WScript.CreateObject("WSCript.Shell") >>%fVBS%
@echo Set result = obj.Exec("ipconfig") >>%fVBS%
@echo strEcho = result.StdOut.ReadAll >>%fVBS%  
@echo Dim firstInput >>%fVBS%
@echo Dim tempInputIp >>%fVBS%
@echo Dim devRealIp	>>%fVBS%
@echo	Dim autoSearchIp	>>%fVBS%
@echo Const IPREGEX = "((2[0-4][0-9]|25[0-5]|[01]?[0-9][0-9]?)\.){3}(2[0-4][0-9]|25[0-5]|[01]?[0-9][0-9]?)" >>%fVBS%
@echo Const IPV6REGEX = "((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))" >>%fVBS%
@echo firstInput = 0 >>%fVBS%
@echo isCheckip = 1 >>%fVBS%
@echo Set regEx = New RegExp >>%fVBS%
@echo regEx.Pattern = IPREGEX >>%fVBS%
@echo regEx.IgnoreCase = False >>%fVBS%
@echo Set retVala = regEx.Execute(strEcho) >>%fVBS%
@echo Set regExIpv6 = New RegExp >>%fVBS%
@echo regExIpv6.Pattern = IPV6REGEX >>%fVBS%
@echo regExIpv6.IgnoreCase = False >>%fVBS%
@echo Set retValaIpv6 = regExIpv6.Execute(strEcho) >>%fVBS%
@echo If (retVala.Count ^> 0) Then >>%fVBS%
@echo autoSearchIp = retVala.Item(0) >>%fVBS%
@echo ELSEIf (retValaIpv6.Count ^> 0) Then >>%fVBS%
@echo autoSearchIp = retValaIpv6.Item(0) >>%fVBS%
@echo End If >>%fVBS%
@echo Do While (regTest(IPREGEX, devRealIp) = 0 And regTest(IPV6REGEX, devRealIp) = 0) And isCheckip = 1 >>%fVBS%
@echo isCheckip = 1 >>%fVBS%
@echo If Not firstInput = 1 Then >>%fVBS%
@echo firstInput = 1 >>%fVBS%
@echo	If Len(autoSearchIp) ^> 0 Then >>%fVBS%
@echo	devRealIp = InputBox("系统检查到的当前主机IP地址为:" ^& autoSearchIp ^& Chr(13) ^& Chr(10) ^& ",正确请输入Y,否则请输入正确的IP:", "获取设备IP") >>%fVBS%
@echo	Else >>%fVBS%
@echo	devRealIp = InputBox("请输入待检查设备准确的IP:", "获取设备IP") >>%fVBS%
@echo	End If >>%fVBS%
@echo tempInputIp = devRealIp >>%fVBS%
@echo Else >>%fVBS%
@echo	If IsEmpty(tempInputIp) Then >>%fVBS%
@echo	Exit Do >>%fVBS%
@echo	Else >>%fVBS%
@echo	If Len(tempInputIp) = 0 Then >>%fVBS%
@echo	devRealIp = InputBox("请输入正确的IP,上次输入IP为空", "获取设备IP") >>%fVBS%
@echo	Else >>%fVBS%
@echo	devRealIp = InputBox("请输入正确的IP,上次输入IP为:" ^& tempInputIp, "获取设备IP") >>%fVBS%
@echo	End If >>%fVBS%
@echo	End If >>%fVBS%
@echo tempInputIp = devRealIp >>%fVBS%
@echo End If >>%fVBS%
@echo If LCase(Trim(devRealIp)) = "y" Then >>%fVBS%
@echo isCheckip = 0 >>%fVBS%
@echo devRealIp = autoSearchIp >>%fVBS%
@echo getRealIpAddress = 1 >>%fVBS%
@echo End If >>%fVBS%
@echo Loop >>%fVBS%
@echo getRealIpAddress = devRealIp >>%fVBS%
@echo End Function >>%fVBS%
@echo Dim devRealIp >>%fVBS%
@echo devRealIp = getRealIpAddress >>%fVBS%
@echo If (Not IsNull(devRealIp) And Not IsEmpty(devRealIp)) Then >>%fVBS%
@echo WScript.Echo devRealIp >>%fVBS%
@echo End If >>%fVBS%
@rem ---------------------- 调用VB脚本结束 ----------------------
for /f %%a in ('cscript "%fVBS%" //nologo //e:vbscript') do set "realIp=%%a"
@del %fVBS%
if "%realIp%"=="" exit

echo ^>^>^>^>^>^>^>^> 检查初始化开始

::生成文件，并将采集信息写入文件
set result=hg_MW_tomcat 6_Windows_%realIp%_%fileDate%.ponshine

::---------------------------------------VBS start-------------------------------
@set fVBSRandom=%random:~-5%
@set fVBS=ponshine-hg-windows-.%fVBSRandom%.vbs

@echo Dim nowPath >> %fVBS%
@echo Dim filePath >> %fVBS%
@echo Dim objFSO >> %fVBS%
@echo Dim objShell >> %fVBS%
@echo Dim tomcatInstallPath >> %fVBS%
@echo Dim cmdHead >> %fVBS%

@echo Set objShell = wscript.CreateObject("Wscript.Shell") >> %fVBS%
::路径在bat脚本获取，vbs里面无需在另行获取
@echo Set objFSO = CreateObject("Scripting.FileSystemObject") >> %fVBS%
@echo nowPath=createobject("Scripting.FileSystemObject").GetFile(Wscript.ScriptFullName).ParentFolder.Path >> %fVBS%
@echo filePath= Left(objFSO.getFolder(".").Path, 3) ^& "%result%" >> %fVBS%
@echo objFSO.CreateTextFile(filePath) >> %fVBS%

@echo Set shell = CreateObject("Shell.Application") >> %FVBS%
@echo Set tomcatInstallFolder = shell.BrowseForFolder(0, "请选择tomcat的安装目录，如：" ^& Chr(13) ^& "H:\ponshine\tomcat\apache-tomcat-6.0.28\", 0) >> %fVBS%
@echo If tomcatInstallFolder Is Nothing Then WScript.Quit >> %fVBS%
@echo tomcatInstallPath = tomcatInstallFolder.Self.Path >> %fVBS%

@echo cmdHead = "%COMSPEC% /c type  """ ^& tomcatInstallPath ^& """" >>%fVBS%

::写入基础信息
@echo If (objFSO.FileExists(filePath)) Then >>%fVBS%
@echo Set file = objFSO.OpenTextFile(filePath, 8, True, False) >>%fVBS%
@echo file.WriteLine "*#*result_type*#*" >>%fVBS%
@echo file.WriteLine "HG_MW_TOMCAT_WINDOWS" >>%fVBS%
@echo file.WriteLine "*#*result_type*#*" >>%fVBS%
@echo file.WriteLine "*#*host_ip*#*" >>%fVBS%
@echo file.WriteLine "%realIp%" >>%fVBS%
@echo file.WriteLine "*#*host_ip*#*" >>%fVBS%
@echo file.Close >>%fVBS%
@echo End If >>%fVBS%

@echo If (objFSO.FileExists(filePath)) Then >>%fVBS%
@echo Set file = objFSO.OpenTextFile(filePath, 8, True, False) >>%fVBS%
@echo file.WriteLine "*#*asset_port*#*" >>%fVBS%
@echo file.Close >>%fVBS%
@echo End If >>%fVBS%
@echo objShell.Run cmdHead ^& "\conf\server.xml >> " ^& filePath, 0, True >>%fVBS%
@echo If (objFSO.FileExists(filePath)) Then >>%fVBS%
@echo Set file = objFSO.OpenTextFile(filePath, 8, True, False) >>%fVBS%
@echo file.WriteLine "*#*asset_port*#*" >>%fVBS%
@echo file.Close >>%fVBS%
@echo End If >>%fVBS%

echo ^>^>^>^>^>^>^>^> 检查初始化完成
echo ^>^>^>^>^>^>^>^> 生成检查命令开始

::检查项开始
::1359检查是否按照用户分配账号
::1361检查是否删除或锁定与设备运行、维护等工作无关的账号
::1362检查静态口令认证技术
::1363检查是否配置用户最小权限
@echo Wscript.echo ">>>>>>>> '检查是否按照用户分配账号'开始" >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查是否删除或锁定与设备运行、维护等工作无关的账号'开始" >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查静态口令认证技术'开始" >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查是否配置用户最小权限'开始" >>%fVBS%
@echo If (objFSO.FileExists(filePath)) Then >>%fVBS%
@echo Set file = objFSO.OpenTextFile(filePath, 8, True, False) >>%fVBS%
@echo file.WriteLine "#%%1359#%%1361#%%1362#%%1363#%%" >>%fVBS%
@echo file.WriteLine "ponshine_smp_start" >>%fVBS%
@echo file.Close >>%fVBS%
@echo End If >>%fVBS%
@echo objShell.Run cmdHead ^& "\conf\tomcat-users.xml >> " ^& filePath, 0, True >>%fVBS%
@echo If (objFSO.FileExists(filePath)) Then >>%fVBS%
@echo Set file = objFSO.OpenTextFile(filePath, 8, True, False) >>%fVBS%
@echo file.WriteLine "ponshine_smp_stop" >>%fVBS%
@echo file.WriteLine "#%%1359#%%1361#%%1362#%%1363#%%" >>%fVBS%
@echo file.Close >>%fVBS%
@echo End If >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查是否按照用户分配账号'结束" >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查是否删除或锁定与设备运行、维护等工作无关的账号'结束" >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查静态口令认证技术'结束" >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查是否配置用户最小权限'结束" >>%fVBS%

::1364检查是否配置日志功能
::1358检查是否使用HTTPS等加密协议
::1365检查是否支持定时账户自动登出
::1366检查是否更改tomcat服务器默认端口
@echo Wscript.echo ">>>>>>>> '检查是否配置日志功能'开始" >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查是否使用HTTPS等加密协议'开始" >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查是否支持定时账户自动登出'开始" >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查是否更改tomcat服务器默认端口'开始" >>%fVBS%
@echo If (objFSO.FileExists(filePath)) Then >>%fVBS%
@echo Set file = objFSO.OpenTextFile(filePath, 8, True, False) >>%fVBS%
@echo file.WriteLine "#%%1364#%%1358#%%1365#%%1366#%%" >>%fVBS%
@echo file.WriteLine "ponshine_smp_start" >>%fVBS%
@echo file.Close >>%fVBS%
@echo End If >>%fVBS%
@echo objShell.Run cmdHead ^& "\conf\server.xml >> " ^& filePath, 0, True >>%fVBS%
@echo If (objFSO.FileExists(filePath)) Then >>%fVBS%
@echo Set file = objFSO.OpenTextFile(filePath, 8, True, False) >>%fVBS%
@echo file.WriteLine "ponshine_smp_stop" >>%fVBS%
@echo file.WriteLine "#%%1364#%%1358#%%1365#%%1366#%%" >>%fVBS%
@echo file.Close >>%fVBS%
@echo End If >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查是否配置日志功能'结束" >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查是否使用HTTPS等加密协议'结束" >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查是否支持定时账户自动登出'结束" >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查是否更改tomcat服务器默认端口'结束" >>%fVBS%

::1360检查是否禁止tomcat列表显示文件
::1367检查是否支持错误页面重定向
@echo Wscript.echo ">>>>>>>> '检查是否禁止tomcat列表显示文件'开始" >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查是否支持错误页面重定向'开始" >>%fVBS%
@echo If (objFSO.FileExists(filePath)) Then >>%fVBS%
@echo Set file = objFSO.OpenTextFile(filePath, 8, True, False) >>%fVBS%
@echo file.WriteLine "#%%1360#%%1367#%%" >>%fVBS%
@echo file.WriteLine "ponshine_smp_start" >>%fVBS%
@echo file.Close >>%fVBS%
@echo End If >>%fVBS%
@echo objShell.Run cmdHead ^& "\conf\web.xml >> " ^& filePath, 0, True >>%fVBS%
@echo If (objFSO.FileExists(filePath)) Then >>%fVBS%
@echo Set file = objFSO.OpenTextFile(filePath, 8, True, False) >>%fVBS%
@echo file.WriteLine "ponshine_smp_stop" >>%fVBS%
@echo file.WriteLine "#%%1360#%%1367#%%" >>%fVBS%
@echo file.Close >>%fVBS%
@echo End If >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查是否禁止tomcat列表显示文件'结束" >>%fVBS%
@echo Wscript.echo ">>>>>>>> '检查是否支持错误页面重定向'结束" >>%fVBS%

echo ^>^>^>^>^>^>^>^> 生成检查命令结束
echo ^>^>^>^>^>^>^>^> 离线检查开始

::运行脚本
@echo Wscript.Echo objFSO.getFolder(".").Path ^& "\" >> %fVBS%
@echo objFSO.MoveFile filePath, objFSO.getFolder(".").Path ^& "\" >>%fVBS%
@echo objFSO.DeleteFile(tempSqlPath) >> %fVBS%
@cscript -nologo %fVBS%
::删除脚本
@del %fVBS%

cls
echo ^>^>^>^>^>^>^>^> 离线检查结束
color c
echo 警告：请不要打开检查结果文件，否则可能影响检查结果！
pause
::---------------------------------------VBS end---------------------------------


::-----------------------------------------bat end-------------------------------