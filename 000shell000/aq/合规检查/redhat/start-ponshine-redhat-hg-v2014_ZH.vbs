#$language = "VBScript"
#$interface = "1.0"
' 鹏信linux/unix通用主方法
Dim crtTabSession
Dim crtTabScreen
Dim basePath
Dim crtTab
Dim cmCheckResultFile
Dim realIp
Dim retIsFunction
Dim overOnePage
Dim prompt

Sub Main
	Dim nTabCount
	'nTabCount赋值为打开的会话的总个数
	nTabCount = crt.GetTabCount
	'做循环
	For index = 1 to nTabCount
		'循环检查所有的CRT窗口
		Set crtTab = crt.GetTab(index)
		crtTab.activate
		'激活第index个会话
		crtTab.Screen.Synchronous = True
		crtTab.Screen.IgnoreEscape = True
		startCheckFun crtTab
	Next
	MsgBox "检查完成！"
End Sub

' 转换为指定的日期格式：YYYYMMDDhhmmss
Function formatDate(dateTime)
	Dim y, m, d, h, mm, s
	
	y = Year(datetime)
	m = Month(datetime)
	d = Day(datetime)
	h = Hour(datetime)
	mi = Minute(datetime)
	s = Second(datetime)
	
	If Len(m) = 1 Then
		m = "0" & m
	End If
	If Len(d) = 1 Then
		d = "0" & d
	End If
	If Len(h) = 1 Then
		h = "0" & h
	End If
	If Len(mi) = 1 Then
		mi = "0" & mi
	End If
	If Len(s) = 1 Then
		s = "0" & s
	End If
	
	formatDate = y & m & d & h & mi & s
	
End Function

Function regTest(patern, str)
	Dim regEx, retVal		
	Set regEx = New RegExp		
	regEx.Pattern = patern        
	regEx.IgnoreCase = False		
	retVal = regEx.Test(str)	
	If retVal Then
		regTest = 1
	Else
		regTest = 0
	End If
End Function

Function ipTest(ipString)
	Dim regEx, retValue, regExIpv6, retValueIpv6
	Set regEx = New RegExp	
	regEx.Pattern = "^((2[0-4][0-9]|25[0-5]|[01]?[0-9][0-9]?)\.){3}(2[0-4][0-9]|25[0-5]|[01]?[0-9][0-9]?)$"     
	regEx.IgnoreCase = False		
	Set retValue = regEx.Execute(ipString)
	Set regExIpv6 = New RegExp	
	regExIpv6.Pattern = "^((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?$"     
	regExIpv6.IgnoreCase = False		
	Set retValueIpv6 = regExIpv6.Execute(ipString)
	If (retValue.Count > 0 Or retValueIpv6.Count > 0 ) Then
		ipTest = 1	
	Else
		ipTest = 0			
	End If
End Function

Public Function startCheckFun(selectedCrtTab)
	Set crtTabSession = crtTab.Session
	Set crtTabScreen = crtTab.Screen
		
	If Not crtTabSession.Connected = True Then
		MsgBox "SecureCRT连接异常，请先检查连接状态！"
		Exit Function
	End If	

	Dim fsObj
	set fsObj = CreateObject("Scripting.FileSystemObject")
	basePath = fsObj.getFolder(".").Path
	Dim scriptFile
	'读取脚本配置文件
	scriptFile = readFromFile(basePath & "\script.res")
	'执行检查脚本				
	startCheck(scriptFile)	
	
	'判断日志文件是否存在，存在则删除
	If crtTabSession.Logging = True Then
		crtTabSession.Log False
	End If
	If fsObj.FileExists(crtTabSession.LogFileName) = True Then
			fsObj.DeleteFile crtTabSession.LogFileName
	End If
	
End Function

'读取文件方法
Public Function readFromFile(filePath)
	Dim fsObj
	set fsObj = CreateObject("Scripting.FileSystemObject")
	Replace filePath, "script.res", "script.txt"
	
	If Not fsObj.FileExists(filePath) Then
		MsgBox filePath & " not exist!"
		Exit Function
	End If
	'存在文件，则读取
	On Error Resume Next		'打开异常捕获
	Set adoStr = CreateObject("Adodb.Stream")
	adoStr.Type = 2
	adoStr.mode = 3
	adoStr.charset = "utf-8"
	adoStr.Open
	adoStr.LoadFromFile filePath
	readFromFile = adoStr.readtext
	adoStr.close
	If Err.number <> 0 Then	
		set oneFile = fsObj.OpenTextFile(filePath,1, False)
		readFromFile = oneFile.ReadAll
		oneFile.Close
	End If	
	On Error Goto 0				'关闭异常捕获
End Function

Public Function startCheck(scritpFile)
	Dim fsObj
	Set fsObj = CreateObject("Scripting.FileSystemObject")
	'判断是否存在结果目录
	If(fsObj.FolderExists(basePath & "\ponshie_offline_result") = False) Then
			fsObj.createFolder(basePath & "\ponshie_offline_result")
	End If
	'检查结果文件
	cmResultFileName = basePath & "\" & "ponshie_offline_result\" & crtTab.Caption & "_" & formatDate(Now()) & ".ponshine"
	fsObj.CreateTextFile cmResultFileName , True
	Set cmCheckResultFile = fsObj.OpenTextFile(cmResultFileName, 2)
	'判断日志文件是否存在，存在则删除
	If crtTabSession.Logging = True Then
		crtTabSession.Log False
	End If
	If fsObj.FileExists(crtTabSession.LogFileName) = True Then
		fsObj.DeleteFile crtTabSession.LogFileName
	End If
	
	' 创建日志文件
	logfilePath = basePath & "\" & "ponshie_offline_result\" & crtTab.Caption & ".log"
	crtTabSession.LogFileName = logfilePath
	
	Dim scrptLine
	Dim resStr
	
	Dim moreStr
	Dim isSendScriptRes '命令是否发送完
	Dim printEchoRes
       
	crtTabScreen.Send Chr(13) 
	crtTabScreen.WaitForString "$#@!!@#$",1 	
	
	' 获取提示符信息
	prompt = crtTabScreen.Get(crtTabScreen.CurrentRow,1,crtTabScreen.CurrentRow,crtTabScreen.CurrentColumn)
	prompt = Trim(prompt)
	
	isSendScriptRes = "N" 
	realIp = 0
	overOnePage = 0
	
	'循环执行脚本
	Dim scrptLines
	scrptLines = Split(scritpFile, Chr(10))
	For Each scrptLine In scrptLines
		If Len(scrptLine) > 0 Then							' 去除命令尾部的换行符
			If Right(scrptLine, 1) = Chr(13) Then
				scrptLine = Left(scrptLine, Len(scrptLine)-1)
			End If
			If Left(scrptLine,16) = "!PONSHINE_PRINT!" Then
				If(isSendScriptRes = "Y") Then 
					crtTabScreen.WaitForString "$#@!!@#$",1           
					crtTabSession.Log False
					printEchoRes = readFromFile(crtTabSession.LogFileName)
					if resetIP(scrptLine,printEchoRes,returnStrEcho) = 0 Then	'如果ip为空终止操作，删除result结果文件
						cmCheckResultFile.Close
						fsObj.DeleteFile(cmResultFileName)
						Exit Function
					End If
					If Not Len(returnStrEcho) = 0 Then
						resStr = resStr & returnStrEcho
					Else
						resStr = resStr & printEchoRes
					End If
					isSendScriptRes = "N"
				End If
				'直接输出内容
				resStr = resStr & Right(scrptLine, Len(scrptLine)-16) & vbCrLf
				If Len(resStr) > 0 Then
					On Error Resume Next
					Err.Clear
					cmCheckResultFile.Write resStr
					If Err.number <> 0 Then					
						cmCheckResultFile.Write "commanding script is not output" & vbCrLf
						If Len(Right(scrptLine, Len(scrptLine)-16))>0 Then
							cmCheckResultFile.Write Right(scrptLine, Len(scrptLine)-16) & vbCrLf
						End If
					End If
					On Error Goto 0
				End If
				resStr = Space(0)	' 清空结果字符串
			Else
				If(isSendScriptRes = "N") Then
					isSendScriptRes = "Y"
					crtTabSession.Log True
				End if
				'执行命令
				If Len(scrptLine)>0 Then
					crtTabScreen.Send scrptLine
					moreStr = crtTabScreen.WaitForString(scrptLine, 1)
					crtTabScreen.Send Chr(13)
					crtTabScreen.WaitForCursor	'等待光标移动        
					If NOT (Right(Trim(scrptLine),1)="\") Then	
					WaitThePrompt
					End If
				End If
			End if
		End If
	Next
	cmCheckResultFile.Close
	changeFileName(cmResultFileName)
End Function
	
public Function WaitThePrompt
	Dim comparePrompt
	Dim doublePrompt
	Dim matchId
	retIsFunction = "-1"
	Do
	crtTabScreen.ReadString prompt, "Input 'n': next page, others: exit", "Press Enter to continue or <ctrl-z> to abort", ">", "--More--", "--- More ---", "<--- More --->", "---- More ----", "--- more ---", "---more---", "---(more", "--- More", "--- more", "--More-- or (q)uit", "--More or (q)uit current module or <ctrl-z> to abort", 1
	matchId = crtTabScreen.MatchIndex
	comparePrompt = crtTabScreen.Get(crtTabScreen.CurrentRow,1,crtTabScreen.CurrentRow,crtTabScreen.CurrentColumn)
	comparePrompt = Trim(comparePrompt)
	Select Case matchId
		Case 0
			If overOnePage = 1 Then
			overOnePage = 0
			crtTabScreen.Send Chr(13)
			End If

			If Len(comparePrompt)=0 Then
				crtTabScreen.WaitForString "$#@!!@#$",3
				currentPrompt = crtTabScreen.Get(crtTabScreen.CurrentRow,1,crtTabScreen.CurrentRow,crtTabScreen.CurrentColumn)
				currentPrompt = Trim(currentPrompt)
				If Len(currentPrompt)=0 Then
					crtTabScreen.Send Chr(13)
				End If
			End If
			
			If StrComp(prompt,comparePrompt)=0 Then	
				Exit Do
			End If
			
			doublePrompt = prompt&prompt
			If StrComp(doublePrompt,comparePrompt)=0 Then	
				Exit Do
			End If

			If checkFunction(comparePrompt,2) = 1 Then
				Exit Do
			End If
		Case 1
			Exit Do
		Case 2
			crtTabScreen.Send "n"
		Case 3
			Exit Do
		Case 4
			If checkFunction(comparePrompt,2) = 1 Then
				Exit Do
			End If
		Case Else
			overOnePage = 1
			crtTabScreen.Send Chr(32)
	End Select
	Loop	
End Function


public Function checkFunction(inputStr,keyNum)
	checkFunction = 0
	
	If inputStr = ">" Or (Right(inputStr,1)=">" And InStr(inputStr,"<")=0) Then
		tempReturn = getLinePro(0)
		If(strComp(retIsFunction,"-1")=0) Then
			retIsFunction = tempReturn
		Else
			if(strComp(retIsFunction,tempReturn)=0) Then
				strlastline = getLinePro(1)
				if(strComp(strlastline,tempReturn))=0 Then
					keyNum = keyNum-1
				Else 
					Exit Function
				End If
			Else
				Exit Function
			End If
		End If
		
		If keyNum > 0 Then
			keyNum = keyNum-1
			crtTabScreen.Send Chr(13)
			crtTabScreen.ReadString ">",1
			nMat = crtTabScreen.MatchIndex
			if(nMat = 1) Then
				If checkFunction(tempReturn,keyNum)=1 Then
					checkFunction = 1
				End If
			Else
				checkFunction = 0
			End If
		Else
			checkFunction = 1	
		End If		
	
	End If
End Function	
	

public Function getLinePro(nLine)
		getLinePro = crtTabScreen.Get2(crtTabScreen.CurrentRow-nLine,1,crtTabScreen.CurrentRow-nLine,crtTabScreen.CurrentColumn)
		getLinePro = Trim(getLinePro)
		If Right(getLinePro,1)=Chr(10) Or Right(getLinePro,1)=Chr(13) Then
			getLinePro = Left(getLinePro,Len(getLinePro)-1)
		End If
		If Right(getLinePro,1)=Chr(10) Or Right(getLinePro,1)=Chr(13) Then
			getLinePro = Left(getLinePro,Len(getLinePro)-1)
		End If
		getLinePro = Trim(getLinePro)
End Function



'处理设备IP
Public Function resetIP(scrptLine,strEcho,returnStrEcho)
	resetIP = 1
	returnStrEcho = Space(0)
	If(regTest("\*#\*host_ip\*#\*", scrptLine) = 1) Then
		If getRealIpAddress(strEcho) = 1 Then
			returnStrEcho = realIp & vbcrlf
		Else
			realIp = 0
			resetIP = 0
			Exit Function 
		End If
	End If
End Function

'取IP地址
Public Function getRealIpAddress(echoRes)
	Dim firstIpMark	'是否第一次输入IP
	Dim putTempIp	'存临时IP
	Dim systemFindIp '自动检测到的IP
	Dim checkIpMark '是否检测IP
	Dim promptTip	'prompt提示
	Const IPREGEX = "((2[0-4][0-9]|25[0-5]|[01]?[0-9][0-9]?)\.){3}(2[0-4][0-9]|25[0-5]|[01]?[0-9][0-9]?)"
	Const IPV6REGEX = "^((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?$"
	firstIpMark = 0
	getRealIpAddress = 1
	If Not realIp = 0 Then
		Exit Function
	End If
	If Len(echoRes) = 0 Then 
		Dim boxTip
        boxTip = "读取IP错误,请检查SecureCRT设置,建议如下设置:" & Chr(13) & "1.选项->会话选项->仿真->终端->Linux;" & Chr(13) & "2.选项->会话选项->外观->字符编码->UTF-8;" & Chr(13) & "3.文件->关闭会话日志及会话原始日志."
        MsgBox boxTip
        getRealIpAddress = 0
		Exit Function
	End If
	checkIpMark = 1
	Set regEx = New RegExp
	regEx.Pattern = IPREGEX
	regEx.IgnoreCase = False
	Set retVala = regEx.Execute(echoRes)
	Set regExIpv6 = New RegExp
	regExIpv6.Pattern = IPV6REGEX
	regExIpv6.IgnoreCase = False
	Set retValaIpv6 = regExIpv6.Execute(echoRes)
	If (retVala.Count > 0) Then
		systemFindIp = retVala.Item(0)
		ElseIf  (retValaIpv6.Count > 0) Then
			systemFindIp = retValaIpv6.Item(0)
	End If
	Do While ((regTest(IPREGEX, realIp) = 0 And regTest(IPV6REGEX, realIp) = 0) Or ipTest(realIp) = 0 ) And checkIpMark = 1
		checkIpMark = 1
		If Not firstIpMark = 1 Then
			firstIpMark = 1
			If Len(systemFindIp) > 0 Then
               			promptTip = "系统检测到的当前主机IP为:" & systemFindIp & ",正确请输入Y," & Chr(13) & Chr(10) & "否则请输入准确的IP:"
        		Else
              			promptTip = "请输入待检设备准确的IP:"
        		End If
        		realIp = crt.Dialog.Prompt(promptTip, "获取设备IP中...", "", False)
        		putTempIp = realIp
		Else
			If Len(putTempIp) = 0 Then
				getRealIpAddress = 0
				Exit Do
			Else
                		promptTip = "请输入正确的IP,系统检测到的IP为:" & systemFindIp & ",正确请输入Y," & Chr(13) & Chr(10) & "上次输入IP为:" &putTempIp & ""
				
      			End If
        		realIp = crt.Dialog.Prompt(promptTip, "获取设备IP中...", "", False)
        		putTempIp = realIp
		End If
		If LCase(Trim(realIp)) = "y" Then
			checkIpMark = 0
			realIp = systemFindIp
			getRealIpAddress = 1
		End If
	Loop
End Function


'修改结果文件名格式
Public Function changeFileName(fileName)
	Dim fsObj
	Set fsObj = CreateObject("Scripting.FileSystemObject")
	realIp=Replace(realIp, ":", ".")
	If (fsObj.FileExists(fileName) = True) Then
		Dim newFileName
		newFileName = basePath & "\" & "ponshie_offline_result\" & "HG_OS_RedHat_" & realIp &"_"& formatDate(Now()) & ".ponshine"
		fsObj.MoveFile fileName, newFileName
	End If
End Function