#!/bin/bash
# 1、修改ssh banner
banner = `cat /etc/ssh/sshd_config |grep "Banner /etc/sshbanner" |wc -l`
if [[ $banner -eq 1 ]];
then
    echo -e "Authorized users only. All activity may be monitored and reported." > /etc/sshbanner
fi
service sshd restart
# 2、修改日志权限
find /var/log/ -type f -name "*log*" |xargs chmod 600
# 3、锁定用户并将其修改/bin/false
passwd -l lp
passwd -l uucp
passwd -l nobody
passwd -l games
passwd -l rpm
passwd -l smmsp
passwd -l nfsnobody
passwd -l listen
passwd -l gdm
passwd -l webservd
passwd -l nobody4
passwd -l noaccess
sed -i "s#nobody:x:99:99:Nobody:/:/sbin/nologin#nobody:x:99:99:Nobody:/:/bin/false#g" /etc/passwd
sed -i "s#games:x:12:100:games:/usr/games:/sbin/nologin#games:x:12:100:games:/usr/games:/bin/false#g" /etc/passwd
sed -i "s#lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin#lp:x:4:7:lp:/var/spool/lpd:/bin/false#g" /etc/passwd