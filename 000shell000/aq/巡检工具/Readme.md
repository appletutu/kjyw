# 使用文档

> 版本：CentOS_Check_V1.0 (江潮三号V1.0）

## 使用说明

为了执行命令方便，将江潮三号巡检工具sh

在工具所在目录，以***\*root\****身份运行 `sh CentOS_Check_V1.0.Sh`。此脚本只进行检查工作，不会对服务器进行修改，安全人员可以根据检查结果进行相应的排查。`<font color='red'>`高危ip存放在Dangerous_ip.txt中`</font>`，请将其与脚本放在同一目录，可以自行添加txt文件中。

## 主要检查事项

- 系统信息：包括系统基本信息，内存使用情况，系统负载等。
- 进程安全：消耗内存最多和消耗CPU最多的进程。
- 网络安全：监听端口，当前连接，防火墙启动状态，SSH配置策略等。
- 服务安全：开机启动的服务，当前用户的计划任务，syslog日志审计服务开启状态，xinetd服务开启状态。
- 审计安全：：检查secure、messages、cron、boot.log、dmesg日志文件是否存在、统计secure中入侵行为等。
- 用户安全：系统所有用户，系统所有组，非系统默认用户，系统特权用户，密码复杂度，空口令账户，登录失败锁定等。
- 安全事件检查：H2miner挖矿，亡命徒僵尸网络，高危IP。

## 检查结果示例

```
#注意:本脚本只是检查,未对服务器做任何修改,安全人员可根据结果进行相应安全整改                                                    
 
 
##########################################################################
#                                                                        #
#                               主机安全检测                             #
#                                                                        #
##########################################################################
 
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>系统基本信息<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
主机名:           localhost.localdomain
系统名称:         CentOS Linux
系统版本:         8.2.2004(Core)
内核版本:         4.18.0-193.el8.x86_64
系统类型:         x86_64
本机IP地址:       192.168.56.130/24	192.168.122.1/24
CPU型号:           AMD Ryzen 5 PRO 3500U w/ Radeon Vega Mobile Gfx
CPU核数:          4
机器型号:         Product Name: VMware Virtual Platform	Product Name: 440BX Desktop Reference Platform
系统时间:         2020年 11月 02日 星期一 00:51:16 EST
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>资源使用情况<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
总内存大小:           1.8Gi
已使用内存大小:       975Mi
可使用内存大小:       321Mi
系统运行时间:         up 1 day, 2:05
系统负载:             average: 0.27, 0.18, 0.07 
=============================dividing line================================
僵尸进程:
>>>无僵尸进程
=============================dividing line================================
耗CPU最多的进程:
nnnnn       9822  0.8 14.4 4067016 265604 tty3   Sl+  10月31  12:32              \_ /usr/bin/gnome-shell
nnnnn      94559  0.7  3.6 599604 66764 ?        Ssl  00:40   0:04  \_ /usr/libexec/gnome-terminal-server
root        8841  0.2  1.3 205144 24540 ?        Ss   10月31   3:31 /usr/libexec/sssd/sssd_kcm --uid 0 --gid 0 --logger=files
root        1939  0.2  0.3 124156  5848 ?        S    10月31   3:57      \_ /var/lib/pcp/pmdas/proc/pmdaproc -d 3
root        1199  0.1  0.3 221576  6972 ?        S    10月31   2:18  \_ /usr/libexec/sssd/sssd_nss --uid 0 --gid 0 --logger=files
=============================dividing line================================
耗内存最多的进程:
nnnnn       9822  0.8 14.4 4060968 265604 tty3   Sl+  10月31  12:32              \_ /usr/bin/gnome-shell
nnnnn      62209  0.1  4.9 1175712 91176 ?       Sl   11月01   0:24  \_ /usr/bin/nautilus --gapplication-service
nnnnn      10220  0.0  4.2 1385052 78392 tty3    Sl+  10月31   0:10              \_ /usr/bin/gnome-software --gapplication-service
nnnnn      94559  0.7  3.6 599604 66764 ?        Ssl  00:40   0:04  \_ /usr/libexec/gnome-terminal-server
gdm         9400  0.0  2.7 3559808 50932 tty1    Sl+  10月31   0:26  |           \_ /usr/bin/gnome-shell
=============================dividing line================================
环境变量:
LS_COLORS=rs=0:di=38;5;33:ln=38;5;51:mh=00:pi=40;38;5;11:so=38;5;13:do=38;5;5:bd=48;5;232;38;5;11:cd=48;5;232;38;5;3:or=48;5;232;38;5;9:mi=01;05;37;41:su=48;5;196;38;5;15:sg=48;5;11;38;5;16:ca=48;5;196;38;5;226:tw=48;5;10;38;5;16:ow=48;5;10;38;5;21:st=48;5;21;38;5;15:ex=38;5;40:*.tar=38;5;9:*.tgz=38;5;9:*.arc=38;5;9:*.arj=38;5;9:*.taz=38;5;9:*.lha=38;5;9:*.lz4=38;5;9:*.lzh=38;5;9:*.lzma=38;5;9:*.tlz=38;5;9:*.txz=38;5;9:*.tzo=38;5;9:*.t7z=38;5;9:*.zip=38;5;9:*.z=38;5;9:*.dz=38;5;9:*.gz=38;5;9:*.lrz=38;5;9:*.lz=38;5;9:*.lzo=38;5;9:*.xz=38;5;9:*.zst=38;5;9:*.tzst=38;5;9:*.bz2=38;5;9:*.bz=38;5;9:*.tbz=38;5;9:*.tbz2=38;5;9:*.tz=38;5;9:*.deb=38;5;9:*.rpm=38;5;9:*.jar=38;5;9:*.war=38;5;9:*.ear=38;5;9:*.sar=38;5;9:*.rar=38;5;9:*.alz=38;5;9:*.ace=38;5;9:*.zoo=38;5;9:*.cpio=38;5;9:*.7z=38;5;9:*.rz=38;5;9:*.cab=38;5;9:*.wim=38;5;9:*.swm=38;5;9:*.dwm=38;5;9:*.esd=38;5;9:*.jpg=38;5;13:*.jpeg=38;5;13:*.mjpg=38;5;13:*.mjpeg=38;5;13:*.gif=38;5;13:*.bmp=38;5;13:*.pbm=38;5;13:*.pgm=38;5;13:*.ppm=38;5;13:*.tga=38;5;13:*.xbm=38;5;13:*.xpm=38;5;13:*.tif=38;5;13:*.tiff=38;5;13:*.png=38;5;13:*.svg=38;5;13:*.svgz=38;5;13:*.mng=38;5;13:*.pcx=38;5;13:*.mov=38;5;13:*.mpg=38;5;13:*.mpeg=38;5;13:*.m2v=38;5;13:*.mkv=38;5;13:*.webm=38;5;13:*.ogm=38;5;13:*.mp4=38;5;13:*.m4v=38;5;13:*.mp4v=38;5;13:*.vob=38;5;13:*.qt=38;5;13:*.nuv=38;5;13:*.wmv=38;5;13:*.asf=38;5;13:*.rm=38;5;13:*.rmvb=38;5;13:*.flc=38;5;13:*.avi=38;5;13:*.fli=38;5;13:*.flv=38;5;13:*.gl=38;5;13:*.dl=38;5;13:*.xcf=38;5;13:*.xwd=38;5;13:*.yuv=38;5;13:*.cgm=38;5;13:*.emf=38;5;13:*.ogv=38;5;13:*.ogx=38;5;13:*.aac=38;5;45:*.au=38;5;45:*.flac=38;5;45:*.m4a=38;5;45:*.mid=38;5;45:*.midi=38;5;45:*.mka=38;5;45:*.mp3=38;5;45:*.mpc=38;5;45:*.ogg=38;5;45:*.ra=38;5;45:*.wav=38;5;45:*.oga=38;5;45:*.opus=38;5;45:*.spx=38;5;45:*.xspf=38;5;45:
XDG_MENU_PREFIX=gnome-
LANG=zh_CN.UTF-8
GDM_LANG=zh_CN.UTF-8
HISTCONTROL=ignoredups
MANAGERPID=9291
DISPLAY=:0
HOSTNAME=localhost.localdomain
INVOCATION_ID=f2e0045043ce43119b62acf4f65dda5a
COLORTERM=truecolor
USERNAME=nnnnn
XDG_VTNR=3
SSH_AUTH_SOCK=/run/user/1000/keyring/ssh
S_COLORS=auto
XDG_SESSION_ID=5
USER=nnnnn
DESKTOP_SESSION=gnome
WAYLAND_DISPLAY=wayland-0
GNOME_TERMINAL_SCREEN=/org/gnome/Terminal/screen/63a0ccbe_1c84_4c43_83f9_a5ed2754ae5d
PWD=/home/nnnnn/下载
HOME=/root
JOURNAL_STREAM=9:55993
XDG_SESSION_TYPE=wayland
XDG_DATA_DIRS=/root/.local/share/flatpak/exports/share:/home/nnnnn/.local/share/flatpak/exports/share/:/var/lib/flatpak/exports/share/:/usr/local/share/:/usr/share/
XDG_SESSION_DESKTOP=gnome
DBUS_STARTER_ADDRESS=unix:path=/run/user/1000/bus,guid=a54f087b4e05da16b58517405f9e2fa2
MAIL=/var/spool/mail/nnnnn
VTE_VERSION=5202
TERM=xterm-256color
SHELL=/bin/bash
QT_IM_MODULE=ibus
XMODIFIERS=@im=ibus
DBUS_STARTER_BUS_TYPE=session
XDG_CURRENT_DESKTOP=GNOME
GNOME_TERMINAL_SERVICE=:1.181
XDG_SEAT=seat0
SHLVL=4
GDMSESSION=gnome
GNOME_DESKTOP_SESSION_ID=this-is-deprecated
LOGNAME=nnnnn
DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus,guid=a54f087b4e05da16b58517405f9e2fa2
XDG_RUNTIME_DIR=/run/user/1000
PATH=/home/nnnnn/.local/bin:/home/nnnnn/bin:/home/nnnnn/.local/bin:/home/nnnnn/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin
HISTSIZE=1000
SESSION_MANAGER=local/unix:@/tmp/.ICE-unix/9735,unix/unix:/tmp/.ICE-unix/9735
LESSOPEN=||/usr/bin/lesspipe.sh %s
_=/usr/bin/env
=============================dividing line================================
路由表:
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         192.168.56.2    0.0.0.0         UG    100    0        0 ens33
192.168.56.0    0.0.0.0         255.255.255.0   U     100    0        0 ens33
192.168.122.0   0.0.0.0         255.255.255.0   U     0      0        0 virbr0
=============================dividing line================================
监听端口:
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name  
tcp        0      0 127.0.0.1:4330          0.0.0.0:*               LISTEN      93005/pmlogger    
tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN      1/systemd         
tcp        0      0 192.168.122.1:53        0.0.0.0:*               LISTEN      2038/dnsmasq      
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      1356/sshd         
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      8779/cupsd        
tcp        0      0 127.0.0.1:44321         0.0.0.0:*               LISTEN      1919/pmcd         
tcp6       0      0 ::1:4330                :::*                    LISTEN      93005/pmlogger    
tcp6       0      0 :::111                  :::*                    LISTEN      1/systemd         
tcp6       0      0 :::22                   :::*                    LISTEN      1356/sshd         
tcp6       0      0 ::1:631                 :::*                    LISTEN      8779/cupsd        
tcp6       0      0 ::1:44321               :::*                    LISTEN      1919/pmcd         
udp        0      0 192.168.122.1:53        0.0.0.0:*                           2038/dnsmasq      
udp        0      0 0.0.0.0:67              0.0.0.0:*                           2038/dnsmasq      
udp        0      0 0.0.0.0:49253           0.0.0.0:*                           1107/avahi-daemon:  
udp        0      0 0.0.0.0:111             0.0.0.0:*                           1/systemd         
udp        0      0 0.0.0.0:5353            0.0.0.0:*                           1107/avahi-daemon:  
udp        0      0 127.0.0.1:323           0.0.0.0:*                           1156/chronyd      
udp6       0      0 :::111                  :::*                                1/systemd         
udp6       0      0 :::41142                :::*                                1107/avahi-daemon:  
udp6       0      0 :::5353                 :::*                                1107/avahi-daemon:  
udp6       0      0 ::1:323                 :::*                                1156/chronyd      
=============================dividing line================================
当前建立的连接:
=============================dividing line================================
开机启动的服务:
cups.path                                  enabled  
abrt-ccpp.service                          enabled  
abrt-oops.service                          enabled  
abrt-vmcore.service                        enabled  
abrt-xorg.service                          enabled  
abrtd.service                              enabled  
accounts-daemon.service                    enabled  
atd.service                                enabled  
auditd.service                             enabled  
autovt@.service                            enabled  
avahi-daemon.service                       enabled  
bluetooth.service                          enabled  
chronyd.service                            enabled  
crond.service                              enabled  
cups.service                               enabled  
dbus-org.bluez.service                     enabled  
dbus-org.fedoraproject.FirewallD1.service  enabled  
dbus-org.freedesktop.Avahi.service         enabled  
dbus-org.freedesktop.ModemManager1.service enabled  
dbus-org.freedesktop.nm-dispatcher.service enabled  
dbus-org.freedesktop.timedate1.service     enabled  
display-manager.service                    enabled  
firewalld.service                          enabled  
gdm.service                                enabled  
getty@.service                             enabled  
import-state.service                       enabled  
irqbalance.service                         enabled  
iscsi-onboot.service                       enabled  
iscsi.service                              enabled  
kdump.service                              enabled  
ksm.service                                enabled  
ksmtuned.service                           enabled  
libstoragemgmt.service                     enabled  
libvirtd.service                           enabled  
loadmodules.service                        enabled  
lvm2-monitor.service                       enabled  
mcelog.service                             enabled  
mdmonitor.service                          enabled  
microcode.service                          enabled  
ModemManager.service                       enabled  
multipathd.service                         enabled  
NetworkManager-dispatcher.service          enabled  
NetworkManager-wait-online.service         enabled  
NetworkManager.service                     enabled  
nfs-convert.service                        enabled  
pmcd.service                               enabled  
pmie.service                               enabled  
pmlogger.service                           enabled  
rhsmcertd.service                          enabled  
rngd.service                               enabled  
rpcbind.service                            enabled  
rsyslog.service                            enabled  
rtkit-daemon.service                       enabled  
selinux-autorelabel-mark.service           enabled  
smartd.service                             enabled  
sshd.service                               enabled  
sssd.service                               enabled  
syslog.service                             enabled  
sysstat.service                            enabled  
timedatex.service                          enabled  
tuned.service                              enabled  
udisks2.service                            enabled  
vdo.service                                enabled  
vgauthd.service                            enabled  
vmtoolsd.service                           enabled  
avahi-daemon.socket                        enabled  
cups.socket                                enabled  
dm-event.socket                            enabled  
iscsid.socket                              enabled  
iscsiuio.socket                            enabled  
lvm2-lvmpolld.socket                       enabled  
multipathd.socket                          enabled  
rpcbind.socket                             enabled  
sssd-kcm.socket                            enabled  
virtlockd.socket                           enabled  
virtlogd.socket                            enabled  
nfs-client.target                          enabled  
remote-fs.target                           enabled  
dnf-makecache.timer                        enabled  
sysstat-collect.timer                      enabled  
sysstat-summary.timer                      enabled  
unbound-anchor.timer                       enabled  
 
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>系统用户情况<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
活动用户:
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
nnnnn    tty3     tty3             六23   26:05m 15:12   0.01s /usr/libexec/gsd-disk-utility-notify
=============================dividing line================================
系统所有用户:
root:x:0:0
bin:x:1:1
daemon:x:2:2
adm:x:3:4
lp:x:4:7
sync:x:5:0
shutdown:x:6:0
halt:x:7:0
mail:x:8:12
operator:x:11:0
games:x:12:100
ftp:x:14:50
nobody:x:65534:65534
dbus:x:81:81
systemd-coredump:x:999:997
systemd-resolve:x:193:193
tss:x:59:59
polkitd:x:998:996
geoclue:x:997:995
unbound:x:996:993
gluster:x:995:992
rtkit:x:172:172
pipewire:x:994:991
pulse:x:171:171
libstoragemgmt:x:993:988
qemu:x:107:107
apache:x:48:48
cockpit-ws:x:992:986
cockpit-wsinstance:x:991:985
usbmuxd:x:113:113
pcp:x:990:984
rpc:x:32:32
chrony:x:989:983
setroubleshoot:x:988:981
saslauth:x:987:76
postfix:x:89:89
dnsmasq:x:978:978
radvd:x:75:75
clevis:x:977:977
pegasus:x:66:65
abrt:x:173:173
grafana:x:976:976
sssd:x:975:975
flatpak:x:974:974
colord:x:973:973
gdm:x:42:42
rpcuser:x:29:29
pcpqa:x:972:972
gnome-initial-setup:x:971:971
sshd:x:74:74
pesign:x:970:970
avahi:x:70:70
rngd:x:969:969
dovecot:x:97:97
dovenull:x:968:968
tcpdump:x:72:72
nnnnn:x:1000:1000
=============================dividing line================================
系统所有组:
root:x:0
bin:x:1
daemon:x:2
sys:x:3
adm:x:4
tty:x:5
disk:x:6
lp:x:7
mem:x:8
kmem:x:9
wheel:x:10
cdrom:x:11
mail:x:12
man:x:15
dialout:x:18
floppy:x:19
games:x:20
tape:x:33
video:x:39
ftp:x:50
lock:x:54
audio:x:63
users:x:100
nobody:x:65534
dbus:x:81
utmp:x:22
utempter:x:35
input:x:999
kvm:x:36
render:x:998
systemd-journal:x:190
systemd-coredump:x:997
systemd-resolve:x:193
tss:x:59
polkitd:x:996
geoclue:x:995
printadmin:x:994
unbound:x:993
gluster:x:992
rtkit:x:172
pipewire:x:991
pulse-access:x:990
pulse-rt:x:989
pulse:x:171
libstoragemgmt:x:988
ssh_keys:x:987
qemu:x:107
apache:x:48
cockpit-ws:x:986
cockpit-wsinstance:x:985
usbmuxd:x:113
pcp:x:984
rpc:x:32
chrony:x:983
brlapi:x:982
setroubleshoot:x:981
saslauth:x:76
cgred:x:980
libvirt:x:979
postdrop:x:90
postfix:x:89
dnsmasq:x:978
radvd:x:75
clevis:x:977
pegasus:x:65
abrt:x:173
grafana:x:976
sssd:x:975
flatpak:x:974
colord:x:973
gdm:x:42
rpcuser:x:29
stapusr:x:156
stapsys:x:157
stapdev:x:158
pcpqa:x:972
gnome-initial-setup:x:971
sshd:x:74
pesign:x:970
avahi:x:70
slocate:x:21
rngd:x:969
dovecot:x:97
dovenull:x:968
tcpdump:x:72
nnnnn:x:1000
=============================dividing line================================
当前用户的计划任务:
no crontab for root
 
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>身份鉴别安全<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
>>>密码复杂度:已设置
=============================dividing line================================
>>>(root) 是一个未被锁定的账户,请管理员检查是否是可疑账户--------[需调整]
>>>(nnnnn) 是一个未被锁定的账户,请管理员检查是否是可疑账户--------[需调整]
=============================dividing line================================
>>>密码过期天数是99999天,请管理员改成90天------[需调整]
=============================dividing line================================
>>>登入失败处理:未开启,请加固登入失败锁定功能----------[需调整]
 
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>访问控制安全<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
系统中存在以下非系统默认用户:
>>>/etc/passwd里面的nobody的UID为65534，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的systemd-coredump的UID为999，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的polkitd的UID为998，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的geoclue的UID为997，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的unbound的UID为996，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的gluster的UID为995，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的pipewire的UID为994，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的libstoragemgmt的UID为993，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的cockpit-ws的UID为992，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的cockpit-wsinstance的UID为991，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的pcp的UID为990，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的chrony的UID为989，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的setroubleshoot的UID为988，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的saslauth的UID为987，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的dnsmasq的UID为978，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的clevis的UID为977，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的grafana的UID为976，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的sssd的UID为975，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的flatpak的UID为974，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的colord的UID为973，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的pcpqa的UID为972，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的gnome-initial-setup的UID为971，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的pesign的UID为970，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的rngd的UID为969，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的dovenull的UID为968，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
>>>/etc/passwd里面的nnnnn的UID为1000，该账户非系统默认账户，请管理员确认是否为可疑账户--------[需调整]
=============================dividing line================================
系统特权用户:
root
=============================dividing line================================
系统中空口令账户:
dbus该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
systemd-coredump该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
systemd-resolve该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
tss该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
polkitd该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
geoclue该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
unbound该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
gluster该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
rtkit该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
pipewire该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
pulse该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
libstoragemgmt该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
qemu该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
apache该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
cockpit-ws该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
cockpit-wsinstance该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
usbmuxd该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
pcp该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
rpc该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
chrony该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
setroubleshoot该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
saslauth该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
postfix该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
dnsmasq该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
radvd该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
clevis该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
pegasus该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
abrt该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
grafana该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
sssd该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
flatpak该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
colord该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
gdm该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
rpcuser该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
pcpqa该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
gnome-initial-setup该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
sshd该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
pesign该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
avahi该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
rngd该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
dovecot该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
dovenull该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
tcpdump该账户为空口令账户，请管理员确认是否为新增账户，如果为新建账户，请配置密码-------[需调整]
 
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>安全审计<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
正常情况下登录到本机30天内的所有用户的历史记录:
nnnnn    tty3         tty3             Sat Oct 31 23:46   still logged in
reboot   system boot  4.18.0-193.el8.x Sat Oct 31 23:45   still running
reboot   system boot  4.18.0-193.el8.x Fri Oct 30 05:07 - 05:08  (00:00)

wtmp begins Fri Oct 30 05:07:31 2020
=============================dividing line================================
查看syslog日志审计服务是否开启:
Redirecting to /bin/systemctl status rsyslog.service
   Active: active (running) since Sat 2020-10-31 23:45:33 EDT; 1 day 2h ago
>>>经分析,syslog服务已开启
=============================dividing line================================
查看syslog日志是否开启外发:
>>>经分析,客户端syslog日志未开启外发---------[无需调整]
=============================dividing line================================
审计的要素和审计日志:
module(load="imuxsock" 	  # provides support for local system logging (e.g. via logger command)
       SysSock.Use="off") # Turn off message reception via local log socket; 
			  # local messages are retrieved through imjournal now.
module(load="imjournal" 	    # provides access to the systemd journal
       StateFile="imjournal.state") # File to store the position in the journal
global(workDirectory="/var/lib/rsyslog")
module(load="builtin:omfile" Template="RSYSLOG_TraditionalFileFormat")
include(file="/etc/rsyslog.d/*.conf" mode="optional")
*.info;mail.none;authpriv.none;cron.none                /var/log/messages
authpriv.*                                              /var/log/secure
mail.*                                                  -/var/log/maillog
cron.*                                                  /var/log/cron
*.emerg                                                 :omusrmsg:*
uucp,news.crit                                          /var/log/spooler
local7.*                                                /var/log/boot.log
=============================dividing line================================
系统中关键文件修改时间:
>>>文件名：/bin/ps  最后修改时间：5月 11 2019
>>>文件名：/bin/ls  最后修改时间：4月 9 2020
>>>文件名：/bin/login  最后修改时间：4月 24 2020
>>>文件名：/etc/passwd  最后修改时间：10月 31 23:46
>>>文件名：/etc/shadow  最后修改时间：10月 31 23:46

###############################################################################################
#   ls文件:是存储ls命令的功能函数,被删除以后,就无法执行ls命令                                 #
#   login文件:login是控制用户登录的文件,一旦被篡改或删除,系统将无法切换用户或登陆用户         #
#   /etc/passwd是一个文件,主要是保存用户信息                                                  #
#   /bin/ps 进程查看命令功能支持文件,文件损坏或被更改后,无法正常使用ps命令                    #
#   /etc/shadow是/etc/passwd的影子文件,密码存放在该文件当中,并且只有root用户可读              #
###############################################################################################
=============================dividing line================================
检查重要日志文件是否存在:
>>>/var/log/secure日志文件存在
>>>/var/log/messages日志文件存在
>>>/var/log/cron日志文件存在
>>>/var/log/boot.log日志文件存在
>>>/var/log/dmesg日志文件不存在--------[需调整]
 
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>剩余信息保护<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
分区情况:
如果磁盘空间利用率过高，请及时调整---------[需调整]
文件系统             容量  已用  可用 已用% 挂载点
devtmpfs             872M     0  872M    0% /dev
tmpfs                901M     0  901M    0% /dev/shm
tmpfs                901M   19M  882M    3% /run
tmpfs                901M     0  901M    0% /sys/fs/cgroup
/dev/mapper/cl-root   17G  6.3G   11G   37% /
/dev/sda1            976M  194M  716M   22% /boot
tmpfs                181M  5.8M  175M    4% /run/user/1000
tmpfs                181M  1.2M  179M    1% /run/user/42
/dev/sr0             7.7G  7.7G     0  100% /run/media/nnnnn/CentOS-8-2-2004-x86_64-dvd
=============================dividing line================================
可用块设备信息:
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0   20G  0 disk 
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0   19G  0 part 
  ├─cl-root 253:0    0   17G  0 lvm  /
  └─cl-swap 253:1    0    2G  0 lvm  [SWAP]
sr0          11:0    1  7.7G  0 rom  /run/media/nnnnn/CentOS-8-2-2004-x86_64-dvd
=============================dividing line================================
文件系统信息:
/dev/mapper/cl-root     /                       xfs     defaults        0 0
UUID=17438951-0975-4f02-b2c2-a06aa40d2292 /boot                   ext4    defaults        1 2
/dev/mapper/cl-swap     swap                    swap    defaults        0 0
 
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>入侵防范安全<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
系统入侵行为:
>>>无入侵行为
=============================dividing line================================
用户错误登入列表:
>>>用户错误登入--------[需调整]
root     pts/0                         Sun Nov  1 21:16 - 21:16  (00:00)

btmp begins Sun Nov  1 21:16:50 2020
=============================dividing line================================
ssh暴力登入信息:
>>>无ssh暴力登入信息
 
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>恶意代码防范<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
检查是否安装病毒软件:
no crontab for root
>>>未安装ClamAV杀毒软件,请部署杀毒软件加固主机防护--------[无需调整]
 
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>资源控制安全<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
查看是否开启了xinetd服务:
>>>xinetd服务未开启-------[无需调整]
=============================dividing line================================
查看是否开启了ssh服务:
Redirecting to /bin/systemctl status sshd.service
   Active: active (running) since Sat 2020-10-31 23:45:32 EDT; 1 day 2h ago
10月 31 23:45:32 localhost.localdomain sshd[1356]: Server listening on 0.0.0.0 port 22.
10月 31 23:45:32 localhost.localdomain sshd[1356]: Server listening on :: port 22.
>>>SSH服务已开启
=============================dividing line================================
查看是否开启了Telnet-Server服务:
>>>Telnet-Server服务未开启--------[无需调整]
=============================dividing line================================
root        1221  0.0  0.3 290492  6112 ?        Ssl  10月31   0:00 /usr/libexec/platform-python -s /usr/sbin/firewalld --nofork --nopid
>>>防火墙已启用
Chain INPUT (policy ACCEPT 205K packets, 430M bytes)
num   pkts bytes target     prot opt in     out     source               destination       
1        0     0 ACCEPT     udp  --  virbr0 *       0.0.0.0/0            0.0.0.0/0            udp dpt:53
2        0     0 ACCEPT     tcp  --  virbr0 *       0.0.0.0/0            0.0.0.0/0            tcp dpt:53
3        0     0 ACCEPT     udp  --  virbr0 *       0.0.0.0/0            0.0.0.0/0            udp dpt:67
4        0     0 ACCEPT     tcp  --  virbr0 *       0.0.0.0/0            0.0.0.0/0            tcp dpt:67

Chain FORWARD (policy ACCEPT 0 packets, 0 bytes)
num   pkts bytes target     prot opt in     out     source               destination       
1        0     0 ACCEPT     all  --  *      virbr0  0.0.0.0/0            192.168.122.0/24     ctstate RELATED,ESTABLISHED
2        0     0 ACCEPT     all  --  virbr0 *       192.168.122.0/24     0.0.0.0/0         
3        0     0 ACCEPT     all  --  virbr0 virbr0  0.0.0.0/0            0.0.0.0/0         
4        0     0 REJECT     all  --  *      virbr0  0.0.0.0/0            0.0.0.0/0            reject-with icmp-port-unreachable
5        0     0 REJECT     all  --  virbr0 *       0.0.0.0/0            0.0.0.0/0            reject-with icmp-port-unreachable

Chain OUTPUT (policy ACCEPT 105K packets, 4340K bytes)
num   pkts bytes target     prot opt in     out     source               destination       
1        0     0 ACCEPT     udp  --  *      virbr0  0.0.0.0/0            0.0.0.0/0            udp dpt:68
=============================dividing line================================
查看系统SSH远程访问设置策略(host.deny拒绝列表):
more: 对 /etc/hosts.deny stat 失败: 没有那个文件或目录
>>>远程访问策略未设置--------[无需调整]
=============================dividing line================================
查看系统SSH远程访问设置策略(hosts.allow允许列表):
more: 对 /etc/hosts.allow stat 失败: 没有那个文件或目录
>>>远程访问策略未设置--------[无需调整]
=============================dividing line================================
当hosts.allow和host.deny相冲突时,以hosts.allow设置为准
=============================dividing line================================
>>>未设置登入超时限制,请设置,设置方法:在/etc/profile或者/etc/bashrc里面添加参数TMOUT=600 --------[需调整]
##########################################################################
#                                                                        #
#                               特殊事件排查                             #
#                                                                        #
##########################################################################
>>>>>>H2miner挖矿排查:
未发现kingsing可疑进程
未发现kdevtmpfsi可疑进程
>>>>>>亡命徒（outlow）僵尸网络攻击排查:
root          84  0.0  0.0      0     0 ?        S    10月31   0:08 [kswapd0]
警告：发现可疑进程kswapd0,请结合资源占用与远程连接情况进行排查
未发现远程连接可疑进程
未发现扫描可疑进程
=============================恶意IP排查================================
dos2unix: 正在转换文件 Dangerous_ip.txt 为Unix格式...
>>>>>>正在根据Dangerous_ip文件进行ip检查
>>>>>>OK，检查完毕
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>end<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

```
