#!/bin/sh
###
 # @Author: liangpingguo liangping880105@163.com
 # @Date: 2024-06-29 17:35:27
 # @LastEditors: liangpingguo liangping880105@163.com
 # @LastEditTime: 2024-06-29 17:36:59
 # @FilePath: \kjyw\shell\deploy\deploy.sh
 # @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
### 

echo $1
datestr=`date "+%Y%m%d%H%M"`
echo $datestr


if [ "$1" == "auth" ];then
  packname=`ls|grep  auth-service `
  expect -f auth  64422  192.168.1.5  ssh密码  $datestr $packname
  expect -f auth  64422  192.168.1.6 ssh密码  $datestr $packname
elif [ "$1" == "basic" ];then
  packname=`ls|grep  basic-service `
  expect -f basic  64422  192.168.1.5  ssh密码  $datestr $packname
  expect -f basic  64422  192.168.1.6 ssh密码  $datestr $packname
elif [ "$1" == "future" ];then
  packname=`ls|grep  future-gateway ` 
  expect -f future  64422  192.168.1.5  ssh密码  $datestr $packname
  expect -f future  64422  192.168.1.6 ssh密码  $datestr $packname
elif [ "$1" == "property" ];then
  packname=`ls|grep  property-service ` 
  expect -f property  64422  192.168.1.5  ssh密码  $datestr $packname
  expect -f property  64422  192.168.1.6 ssh密码  $datestr $packname
elif [ "$1" == "user" ];then
  packname=`ls|grep  user-service ` 
  expect -f user  64422  192.168.1.5  ssh密码  $datestr $packname
  expect -f user  64422  192.168.1.6 ssh密码  $datestr $packname
  expect -f user  64422  192.168.1.54 ssh密码  $datestr $packname
elif [ "$1" == "jim" ];then
  packname=`ls|grep  jim-service `
  expect -f jim  64422  192.168.1.5  ssh密码  $datestr $packname
  expect -f jim  64422  192.168.1.6 ssh密码  $datestr $packname 
elif [ "$1" == "operation" ];then
  packname=`ls|grep  operation-service `
  expect -f operation  64422  192.168.1.5  ssh密码  $datestr $packname
  expect -f operation  64422  192.168.1.6 ssh密码  $datestr $packname 
  expect -f operation  64422  192.168.1.54 ssh密码  $datestr $packname
elif [ "$1" == "druid" ];then
  packname=`ls|grep  future-druid-admin `
  expect -f druid  64422  192.168.1.5  ssh密码  $datestr $packname                                            
  expect -f druid  64422  192.168.1.6 ssh密码  $datestr $packname 
elif [ "$1" == "garden" ];then
  packname=`ls|grep  garden-service `
  expect -f garden  64422  192.168.1.5  ssh密码  $datestr $packname
  expect -f garden  64422  192.168.1.6 ssh密码  $datestr $packname
elif [ "$1" == "web" ];then                                                                                                
  packname=`ls|grep  dist-web `                                                                                    
  expect -f web  64422  10.212.124.228  ssh密码  $datestr $packname                                                
elif [ "$1" == "h5" ];then                                                                                                  
  packname=`ls|grep  dist-h5 `                                                                                              
  expect -f h5  64422  10.212.124.228  ssh密码  $datestr $packname                                                  
elif [ "$1" == "restart-operation" ];then
  expect -f restart-operation  64422  192.168.1.5  ssh密码  $datestr 
  expect -f restart-operation  64422  192.168.1.6 ssh密码  $datestr 
  expect -f restart-operation  64422  192.168.1.54 ssh密码  $datestr
elif [ "$1" == "restart-user" ];then                                                                               
  packname=`ls|grep  user-service `                                                                                          
  expect -f restart-user  64422  192.168.1.5  ssh密码  $datestr $packname 
  expect -f restart-user  64422  192.168.1.6 ssh密码  $datestr $packname 
  expect -f restart-user  64422  192.168.1.54 ssh密码  $datestr $packname
else
  echo -e "not find sevice\web\n usage: auth  basic  future  property user jim operation druid web h5"
fi
