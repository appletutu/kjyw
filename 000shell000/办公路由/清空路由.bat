chcp 65001

@echo off

title 清空路由表，不会删除持久性的路由

:: 检查是否以管理员权限运行
net session >nul 2>&1
if %errorlevel% neq 0 (
    echo 请求管理员权限...
    PowerShell -Command "Start-Process '%~dpnx0' -Verb RunAs"
    exit /b
)

timeout /nobreak /t 2

route -f 

Echo.
Echo Route deletion completed.
Echo.
route print -4
pause
