chcp 65001

@echo off

title 添加永久路由

:: 检查是否以管理员权限运行
net session >nul 2>&1
if %errorlevel% neq 0 (
    echo 请求管理员权限...
    PowerShell -Command "Start-Process '%~dpnx0' -Verb RunAs"
    exit /b
)

timeout /nobreak /t 2
::OA
route add 10.70.0.0 mask 255.255.0.0 10.73.185.1 -p
::办公区体育场办公区
route add 10.73.0.0 mask 255.255.0.0 10.73.185.1 -p
::信数部中兴云桌面
route add 10.75.0.0 mask 255.255.0.0 10.73.185.1 -p
::网管4A
route add 10.76.0.0 mask 255.255.0.0 10.73.185.1 -p
::网管4A-CMDB
route add 188.102.7.40 mask 255.255.255.255 10.73.185.1 -p
route add 188.104.246.86 mask 255.255.255.255 10.73.185.1 -p
route add 188.99.0.79 mask 255.255.0.0 10.73.185.1 -p

::信数部4A
route add 10.78.0.0 mask 255.255.0.0 10.73.185.1 -p
route add 188.99.0.78 mask 255.255.0.0 10.73.185.1 -p
::moa
route add 10.79.0.0 mask 255.255.0.0 10.73.185.1 -p
::办公区体育场办公区-五楼小机房
route add 20.21.1.0 mask 255.255.255.0 10.73.185.1 -p
route add 20.21.3.0 mask 255.255.255.0 10.73.185.1 -p

::OA邮件
route add 172.21.0.0 mask 255.255.0.0 10.73.185.1 -p
route add 172.16.0.0 mask 255.255.0.0 10.73.185.1 -p
route add 188.104.246.86 mask 255.255.255.255 10.73.185.1 -p
route add 112.17.28.227 mask 255.255.255.255 10.73.185.1 -p
route add 121.37.167.200 mask 255.255.255.255 10.73.185.1 -p

::139邮件
::route add 120.232.169.0 mask 255.255.255.0 10.73.185.1 -p

::华为云
::route add 60.204.206.21 mask 255.255.255.255 10.73.185.1 -p

Echo.
Echo Route added successfully
Echo.
route print -4
pause
