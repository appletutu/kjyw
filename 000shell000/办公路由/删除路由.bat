chcp 65001

@echo off

title 删除永久路由

:: 检查是否以管理员权限运行
net session >nul 2>&1
if %errorlevel% neq 0 (
    echo 请求管理员权限...
    PowerShell -Command "Start-Process '%~dpnx0' -Verb RunAs"
    exit /b
)

timeout /nobreak /t 2

route delete 0.0.0.0
route delete 10.70.0.0
route delete 10.73.0.0
route delete 10.75.0.0
route delete 10.76.0.0
route delete 10.78.0.0
route delete 10.79.0.0
route delete 10.179.0.0
route delete 20.0.0.0
route delete 20.21.1.0
route delete 20.21.3.0
route delete 172.21.0.0
route delete 172.16.0.0
route delete 188.104.246.86
route delete 112.17.28.227
route delete 121.37.167.200

route delete 120.232.169.0
route delete 60.204.206.21

Echo.
Echo Route deletion completed.
Echo.
route print -4
pause
