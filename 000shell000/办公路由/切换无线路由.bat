chcp 65001

@echo off

title 切换无线路由

:: 检查是否以管理员权限运行
net session >nul 2>&1
if %errorlevel% neq 0 (
    echo 请求管理员权限...
    PowerShell -Command "Start-Process '%~dpnx0' -Verb RunAs"
    exit /b
)

timeout /nobreak /t 2

route delete 0.0.0.0
route add 0.0.0.0 mask 0.0.0.0 192.168.0.1

Echo.
Echo Route deletion completed.
Echo.
route print -4
pause
