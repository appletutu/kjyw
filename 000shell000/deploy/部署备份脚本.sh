echo  "This is a test page." >> index.html
# 默认部署目录
mkdir -p /data/setuptmp

mkdir -p /data/project/{front,jar}
mkdir -p /data/project/front/{ipark-app,ipark-web,ipark-portal,ipark-manage,static}
mkdir -p /data/project/jar/{bin,conf,logs,tmp}

mkdir -p /data/update_tmp/{front,jar,sql}/logs
mkdir -p /data/backup

mkdir -p /data/project/front
mkdir -p /data/project/jar
mkdir -p /data/project/sql

mkdir -p /data/update_tmp/front/logs
mkdir -p /data/update_tmp/jar/logs
mkdir -p /data/update_tmp/sql/logs

===================================================================
### 测试华为云下载地址是否可用
wget --user devops --password Zyjc@2023 -O test.tar http://60.204.206.21:38000/download/updatedist/test.tar
wget --user devops --password Zyjc@2023 -O ipark-web-155.jar http://60.204.206.21:38000/download/updatejar/ipark-web-155.jar

wget --user devops --password Zyjc@2023  http://60.204.206.21:38000/download/updatedist/ipark-manage-70.tar
wget --user devops --password Zyjc@2023  http://60.204.206.21:38000/download/updatedist/ipark-manage-70.tar
wget --user devops --password Zyjc@2023  http://60.204.206.21:38000/download/updatejar/ipark-web-155.jar


--spring.config.location=/home/ipark-api/application-api-prod.yml

===================================================================
# 获取取脚本所在的路径，而不是运行脚本目录的路径
LocalDir=$(cd "$(dirname $0)";pwd)
echo $LocalDir

===================================================================
# 复制本地到远程
scp -v -r *.jar 192.168.1.23:/data/update_tmp/jar/
# 复制远程到本地
scp -v -r -P 22180 192.168.1.166:/data/update_tmp/jar/*.jar ./
scp -v -r -P 22180 192.168.1.166:/data/update_tmp/sql/*.sql ./
##-v是debug模式，可以看到命令执行的详细情况；也可以使用-vvv查看更详细的情况。

# sshpass 输入密码 scp
sshpass -p "123456" scp -P 45272 root@192.168.0.5:/data/update_tmp/ipark-web.jar $PWD/ipark-web-8201.jar
sshpass -p "123456" scp -P 88180 root@192.168.1.166:/data/update_tmp/jar/*.jar ./
sshpass -p "123456" scp root@192.168.0.16:/data/update_tmp/jar/*.jar /data/update_tmp/
sshpass -p "123456" scp root@192.168.0.16:/data/update_tmp/jar/ipark-web-22.jar $LocalDir/ipark-web-8201.jar

===================================================================
select * from  sys_dict_data WHERE PARAM_TYPE='MIDDLEWARE_THIRD_PARTY_CODE' and id='1876818704235368450';
update sys_dict_data set CAMPUS_ID=0 WHERE PARAM_TYPE='MIDDLEWARE_THIRD_PARTY_CODE' and id='1876818704235368450';
# 查看版本
SHOW VARIABLES WHERE Variable_name = 'version';

# 查看用户列表
select Host,User,plugin,authentication_string from user;

# 登陆数据库
mysql -urongxueyuan -prongxueyuan_1# -h10.76.168.86 -P7001 -D rongxueyuan
mysql -uroot -pR4lVgYgTPERzdVvu  lssy_user
mysql -uroot -pQ3rhr2v7iPyci1WU  ipark_scalpel

# 导入数据库|  import-sql.sh
mysql -uroot -ppC9^fZ4[hM3# ipark-db-cs < 20141212.sql


# 导出数据库 | dbbackup-sql.sh
mysqldump -uroot -p ipark-db  sys_menu>sys_menu-$(date +%F).sql
mysqldump -uroot -p xxl_job > xxl_job-$(date +%F).sql
mysqldump -urongxueyuan -p -h10.76.168.86 -P7001 rongxueyuan | gzip > rongxueyuan-$(date +%F).sql
mysqldump -uroot -pR4lVgYgTPERzdVvu  lssy_user > lssy_user-$(date +%F).sql
mysqldump -uroot -pQ3rhr2v7iPyci1WU  ipark_scalpel > ipark_scalpel-bk-$(date +%F).sql
# 备份所有数据库到指定目录下
mysqldump -u$user -p$password --all-databases > $backup_dir/$backup_name

## 只能用select
mysql -uuser -ppwd -hhost -Pport dbname -A -e "set names gbk;select * from t_wg_voucher_used_deal_201406" > /data/backup/dbbackup/rongxueyuan-bk-$(date +%F);

# docker 导入数据库
docker exec -it apollo-db mysql -P13306 -uroot --set-gtid-purged=off <apolloconfigdb.sql
docker exec -it mysql-client mysql -h10.195.159.201 -P33390 -ujc_yq -pZhyq@2024 jc_yq < sys_menu-2024-05-30.sql
##拷贝SQL文件到mysql容器中
docker cp xxx.sql mysql容器名:/xxx.sql
docker exec -it mysql mysql -h 192.168.6.88 -uroot -p12345 
mysql>source xxx.sql

# docker 导出数据库 | mysql-client 是容器名称
docker exec -it mysql-client mysqldump -h10.195.159.201 -P3306 -uroot -p123456 $dbname --set-gtid-purged=off --single-transaction >/data/backup/$dbname-bk-$(date +%F).sql
docker exec -i  mysql-client mysqldump -h10.195.159.201 -P33390 -ujc_yq -pZhyq@2024 jc_yq --set-gtid-purged=off --single-transaction | gzip > /data/dbbackup/jc_yq-$(date +%F).sql.gz


# 删除7天前的备份文件
find $backup_dir -mtime +7 -name "*.sql" -exec rm -rf {} \;

====================================================================
# 更新前备份包
tar -zcvf /data/backup/xxx-$(date +%F).tar.gz dist
tar -zcvPf /data/backup/xxx-$(date +%F).tar.gz dist
ls /data/bak
# tar: Removing leading `/' from member names
# tar zcvPf test.tar.gz /path/to/directory  # 解决，注意-p参数的位置，要将其放在-z和-f之间

======================================================================
# 异机部署
vi deploy.sh

#!/bin/sh
SrcPackageName=ipark-api.jar
DesPackageName=ipark-api-8203.jar
sshpass -p "Aa159357@" scp -P45272  root@192.168.0.5:/data/update_tmp/$SrcPackageName $PWD/ $LocalDir/
mv $LocalDir/$SrcPackageName $LocalDir/$DesPackageName
ls -l $LocalDir/

===================================================================
# 回滚脚本
tar -xvf /data/backup/pkgbackup/rxy-gateway.jar-2024-06-06.tar.gz -C $PWD/rxy-gateway.jar
vi rollBack.sh

#!/bin/sh

BackupDir=/data/backup/pkgbackup
PackageName=log.jar
cd /data/project/jar/log
tar -xvPf $BackupDir/${PackageName}-$(date +%F).tar.gz -C $PWD/
ls -l

===================================================================
# 备份应用包
mkdir -p /data/backup/pkgbackup
cat > backup.sh <<EOF

EOF




find /data/bak/ -type f -name "*.tar.gz" -mtime +7
find logs/ -mtime +30 -name "*.log" -exec rm -rf {} \;

