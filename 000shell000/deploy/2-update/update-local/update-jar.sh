#!/bin/bash

# 设置参数
UP_PATH="/data/update_tmp/jar"              # 更新包临时存放路径
APP_PATH="/data/project/jar/ipark-web-8201" # 软件包部署路径
BK_PATH="/data/backup"                      # 备份文件存放路径
LOG_FILE="${UP_PATH}/logs/update.log"       # 更新日志存放路径
BK_TIME=$(date '+%Y%m%d%H%M%S')

UP_NAME="ipark-web"       # 更新包的名称-研发给的
APP_NAME="ipark-web-8201" # 实际部署的软件包名称

# step1.备份原文件
##如果备份文件存放路径不存在则创建
[ ! -d "$BK_PATH" ] && mkdir -p "$BK_PATH"
cd ${APP_PATH} || {echo ERROR "无法切换到 ${APP_PATH} 目录" exit 1}
mv ${APP_NAME}.jar ${BK_PATH}/${APP_NAME}.jar_bk_${BK_TIME}
ls -l ${BK_PATH} | grep ${APP_NAME}.jar_bk_${BK_TIME}

# step2.部署更新包
read -p "update_version(don't input appname):" update_version
if [ -n "${update_version}" ]; then
    cp -r ${UP_PATH}/${UP_NAME}-${update_version}.jar ${APP_PATH}/${APP_NAME}.jar
else
    cp -r ${UP_PATH}/${UP_NAME}.jar ${APP_PATH}/${APP_NAME}.jar
fi

# step3.重启应用
sh start.sh
sleep 10
tail -f logs/nohup.log