#!/bin/bash

# 设置参数
UP_PATH="/data/update_tmp/jar"        # 更新包临时存放路径
APP_PATH="/data/project/jar/libs"     # 应用程序包部署路径
BK_PATH="/data/backup"                # 备份文件存放路径
LOG_FILE="${UP_PATH}/logs/update.log" # 更新日志存放路径
BK_TIME=$(date '+%Y%m%d%H%M%S')
APP_TYPE='.jar'

# 实际部署的应用程序名称列表（键值对形式）
declare -A APP_MAP
APP_MAP["1"]="ipark-api"
APP_MAP["2"]="ipark-base-web"
APP_MAP["3"]="ipark-job-executor"
APP_MAP["4"]="ipark-operation"
APP_MAP["5"]="ipark-web"

# 显示应用包列表并让用户选择
show_app_list() {
    echo "请选择要更新的应用程序编号（多个编号用空格分隔）："
    for ((i = 1; i <= ${#APP_MAP[@]}; i++)); do
        echo "$i) ${APP_MAP[$i]}"
    done
}

# 读取用户输入的编号，并转换为数组
get_user_selections() {
    read -p "请输入编号选择应用程序: " SELECTED_APPS
    SELECTED_APPS=(${SELECTED_APPS})
    # 检查输入是否为有效编号
    for app_index in "${SELECTED_APPS[@]}"; do
        if [[ ! "${APP_MAP[$app_index]}" ]]; then
            echo "输入的编号 $app_index 无效，请重新运行脚本并选择有效编号。"
            exit 1
        fi
    done
}

# 备份原文件并部署更新包
deploy_app() {
    local app_index=$1
    local update_version=$2
    APP_NAME=${APP_MAP[$app_index]}

    # step1.备份
    # 如果备份路径不存在，则创建
    [ ! -d "$BK_PATH" ] && mkdir -p "$BK_PATH"
    # 备份原文件
    echo "正在备份 ${APP_NAME}..."
    BACKUP_FILE="${BK_PATH}/${APP_NAME}${APP_TYPE}_bk_${BK_TIME}"
    mv ${APP_PATH}/${APP_NAME}${APP_TYPE} ${BACKUP_FILE}
    # 检查备份是否成功
    if [ $? -eq 0 ]; then
        echo "备份成功，文件路径：${BACKUP_FILE}"
        ls -l ${BACKUP_FILE}
    else
        echo "备份失败。"
        # exit 1
    fi

    # step2.部署
    echo "正在部署 ${APP_NAME}..."
    APP_FILE=${APP_PATH}/${APP_NAME}${APP_TYPE}
    if [ -n "$update_version" ]; then
        cp -r ${UP_PATH}/${APP_NAME}-${update_version}${APP_TYPE} ${APP_FILE}
    else
        cp -r ${UP_PATH}/${APP_NAME}${APP_TYPE} ${APP_FILE}
    fi
    echo "部署成功，文件路径：${APP_FILE}"
    ls -l ${APP_FILE}

    # step3.重启应用
    # 切换到应用程序包部署路径
    cd ${APP_PATH} || {
        echo "ERROR: 无法切换到目录：${APP_PATH} "
        exit 1
    }
    
    # sh restart-${APP_NAME}.sh
    # sleep 3
    # tail -f logs/nohup-${APP_NAME}.log     
}

# 主流程
show_app_list
get_user_selections
echo "请输入每个应用程序的更新版本号（不要输入应用程序名称）："
for app_index in "${SELECTED_APPS[@]}"; do
    read -p "版本号 for ${APP_MAP[$app_index]}: " update_version
    deploy_app "$app_index" "$update_version"
done

# 批量启动
sh restart-batch.sh
sleep 3

echo "更新完成，请查看日志..."


