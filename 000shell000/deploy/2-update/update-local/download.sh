#!/bin/bash

DOWN_ADD="http://ops.patstech.net:38000/download/updatejar"  # dist or jar !!!
# DOWN_ADD="http://60.204.206.21:38000/download/updatedist"  # dist or jar !!!
UP_PATH="/data/update_tmp/jar"
BK_PATH="/data/backup"

DOWN_NAME="ipark-api"
APP_NAME="ipark-api"
APP_TYPE='.jar'

read -p "update_version(don't input appname):" update_version

# 构建完整的下载URL和目标文件路径
if [ -n "$update_version" ]; then
    DOWN_URL="${DOWN_ADD}/${APP_NAME}-${update_version}${APP_TYPE}"
    TARGET_FILE="${UP_PATH}/${APP_NAME}-${update_version}${APP_TYPE}"
else
    DOWN_URL="${DOWN_ADD}/${APP_NAME}${APP_TYPE}"
    TARGET_FILE="${UP_PATH}/${APP_NAME}${APP_TYPE}"
fi

# 使用wget的spider选项检查文件是否存在
if wget --user devops --password Zyjc@2023 --spider "$DOWN_URL" 2>/dev/null; then
    echo "下载地址存在: $DOWN_URL"
    # 文件存在，使用wget下载文件
    wget --user devops --password Zyjc@2023 -O "$TARGET_FILE" "$DOWN_URL"
    if [ $? -eq 0 ]; then
        echo "文件下载成功，保存在 $TARGET_FILE"
    else
        echo "文件下载失败"
    fi
else
    echo "下载地址不存在: $DOWN_URL"
fi