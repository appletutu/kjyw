#!/bin/bash

# 基础下载URL
DOWN_ADD="http://ops.patstech.net:38000/download/updatejar"
# 下载使用的用户名和密码
USERNAME="devops"
PASSWORD="Zyjc@2023"

# 应用程序名称列表（键值对形式）
declare -A APP_MAP
APP_MAP["1"]="ipark-api"
APP_MAP["2"]="ipark-base-web"
APP_MAP["3"]="ipark-job-executor"
APP_MAP["4"]="ipark-operation"
APP_MAP["5"]="ipark-web"

# 更新临时路径
UP_PATH="/data/update_tmp/jar"
APP_TYPE='.jar'

# 显示应用包列表并让用户选择
show_app_list() {
    echo "请选择要更新的应用程序编号（多个编号用空格分隔）："
    for ((i = 1; i <= ${#APP_MAP[@]}; i++)); do
        echo "$i) ${APP_MAP[$i]}"
    done
}

# 读取用户输入的编号，并转换为数组
get_user_selections() {
    read -p "请输入编号选择应用程序: " SELECTED_APPS
    # 将输入的编号字符串分割成数组
    SELECTED_APPS=(${SELECTED_APPS})
    # 检查输入是否为有效编号
    for app_index in "${SELECTED_APPS[@]}"; do
        if [[ ! "${APP_MAP[$app_index]}" ]]; then
            echo "输入的编号 $app_index 无效，请重新运行脚本并选择有效编号。"
            exit 1
        fi

    done
}

# 构建URL并下载应用程序
download_selected_apps() {
    local app_index
    local update_version

    echo "请输入每个应用程序的更新版本号（不要输入应用程序名称）："
    for app_index in "${SELECTED_APPS[@]}"; do
        read -p "版本号 for ${APP_MAP[$app_index]}: " update_version
        SELECTED_APP_NAME=${APP_MAP[$app_index]}

        # 构建完整的下载URL和目标文件路径
        if [ -n "$update_version" ]; then
            # 如果提供了版本号，则包含版本号构建URL
            DOWN_URL="${DOWN_ADD}/${SELECTED_APP_NAME}-${update_version}${APP_TYPE}"
            TARGET_FILE="${UP_PATH}/${SELECTED_APP_NAME}-${update_version}${APP_TYPE}"
        else
            # 如果没有提供版本号，则不包含版本号构建URL
            DOWN_URL="${DOWN_ADD}/${SELECTED_APP_NAME}${APP_TYPE}"
            TARGET_FILE="${UP_PATH}/${SELECTED_APP_NAME}${APP_TYPE}"
        fi

        # 使用wget的spider选项检查文件是否存在
        if wget --user "$USERNAME" --password "$PASSWORD" --spider "$DOWN_URL" 2>/dev/null; then
            echo "下载地址存在: $DOWN_URL"
            wget --user "$USERNAME" --password "$PASSWORD" -O "$TARGET_FILE" "$DOWN_URL"
            if [ $? -eq 0 ]; then
                echo "文件下载成功，保存在 $TARGET_FILE"
            else
                echo "文件下载失败"
            fi
        else
            echo "下载地址不存在: $DOWN_URL"
        fi
    done
    echo "所有选定的应用程序下载完成。"
}

# 主流程
show_app_list
get_user_selections
if [ ${#SELECTED_APPS[@]} -eq 0 ]; then
    echo "没有选择任何应用程序进行下载。"
    exit 1
fi
download_selected_apps
