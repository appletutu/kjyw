#!/bin/bash

# 基础下载URL
DOWN_ADD="http://ops.patstech.net:38000/download/updatejar"
# 应用程序名称列表（键值对形式）
declare -A APP_MAP
APP_MAP["1"]="ipark-api"
APP_MAP["2"]="ipark-base-web"
APP_MAP["3"]="ipark-job-executor"
APP_MAP["4"]="ipark-operation"
APP_MAP["5"]="ipark-web"

# 更新临时路径
UP_PATH="/data/update_tmp/jar"
APP_TYPE='.jar'

# 获取当前时间戳
CURR_DATE=$(date '+%Y%m%d%H%M%S')

# 显示应用包列表并让用户选择
show_app_list() {
    echo "请选择要更新的应用程序编号："
    # for key in "${!APP_MAP[@]}"; do
    #     echo "$key) ${APP_MAP[$key]}"
    # done
    # 获取数组的长度
    max_index=${#APP_MAP[@]}
    # 从 1 开始递增到数组长度
    for ((i = 1; i <= max_index; i++)); do
        echo "$i) ${APP_MAP[$i]}"
    done
}

# 读取用户输入的编号
get_user_selection() {
    read -p "请输入编号选择应用程序: " SELECTED_APP
    # 检查输入是否为有效编号
    if [[ ! "${APP_MAP[$SELECTED_APP]}" ]]; then
        echo "输入的编号无效，请重新运行脚本并选择有效编号。"
        exit 1
    fi
}

# 构建URL
build_URL() {
    # 获取用户选择的应用程序名称
    SELECTED_APP_NAME=${APP_MAP[$SELECTED_APP]}
    # 提示用户输入更新版本号
    read -p "Enter update version (don't input appname): " update_version
    # 构建完整的下载URL和目标文件路径
    if [ -n "$update_version" ]; then
        DOWN_URL="${DOWN_ADD}/${SELECTED_APP_NAME}-${update_version}${APP_TYPE}"
        TARGET_FILE="${UP_PATH}/${SELECTED_APP_NAME}-${update_version}${APP_TYPE}"
    else
        DOWN_URL="${DOWN_ADD}/${SELECTED_APP_NAME}${APP_TYPE}"
        TARGET_FILE="${UP_PATH}/${SELECTED_APP_NAME}${APP_TYPE}"
    fi
}

# 使用wget的spider选项检查文件是否存在
download_app() {
    if wget --user devops --password Zyjc@2023 --spider "$DOWN_URL" 2>/dev/null; then
        wget --user devops --password Zyjc@2023 -O "$TARGET_FILE" "$DOWN_URL"
        if [ $? -eq 0 ]; then
            echo "文件下载成功，保存在 $TARGET_FILE"
        else
            echo "文件下载失败"
        fi
    else
        echo "下载地址不存在: $DOWN_URL"
    fi
}

# 询问用户是否继续下载
ask_to_continue() {
    read -p "是否继续下载其他应用程序？(y/n): " CONT
    case $CONT in
        [Yy]* )
            show_app_list
            get_user_selection
            build_URL
            download_app
            ;;
        [Nn]* )
            echo "退出下载程序。"
            exit 0
            ;;
        * )
            echo "请输入 Y 继续下载或 N 退出。"
            ask_to_continue
            ;;
    esac
}


# 主流程
show_app_list
get_user_selection
build_URL
download_app
ask_to_continue