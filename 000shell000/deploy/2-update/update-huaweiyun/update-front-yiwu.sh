#!/bin/bash

DOWN_ADD="http://ops.patstech.net:38000/download/updatedist"
# DOWN_ADD="http://60.204.206.21:38000/download/updatedist"
APP_NAME="ipark-manage"
DOWN_NAME="ipark-manage"
APP_PATH="/home/ipark-web/html/dist/"
UP_PATH="/home/update_tmp"
BK_PATH="/home/backup"

DATE=$(date '+%Y%m%d%H%M%S')

read -p "update_version(don't input appname):" update_version
if [  -n "${update_version}"  ];then
    echo "update_url is:" ${DOWN_ADD}/${APP_NAME}-${update_version}.tar >> ${UP_PATH}/logs/update.log
    wget --user devops --password Zyjc@2023 -O ${UP_PATH}/${APP_NAME}.tar ${DOWN_ADD}/${DOWN_NAME}-${update_version}.tar

elif [ -z "${update_version}"  ];then
    echo "update_url is:" ${DOWN_ADD}/${APP_NAME}.tar >> ${UP_PATH}/logs/update.log
    wget --user devops --password Zyjc@2023 -O ${UP_PATH}/${APP_NAME}.tar ${DOWN_ADD}/${DOWN_NAME}.tar
else
    echo "update_url is:" ${DOWN_ADD}/${APP_NAME}.tar >> ${UP_PATH}/logs/update.log
fi


mv ${APP_PATH}/ipark-manage ${BK_PATH}/ipark-manage.bk_${DATE}
mv ${APP_PATH}/micro ${BK_PATH}/micro.bk_${DATE}

cd ${UP_PATH}/
tar -xvf ipark-manage-*.tar
cd dist
mv ipark-manage ${APP_PATH}/
mv micro ${APP_PATH}/
cd ${UP_PATH}/
rm -rf dist
mv ipark-manage-*.tar ipark-manage/

