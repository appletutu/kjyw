#!/bin/bash

DOWN_ADD="http://ops.patstech.net:38000/download/updatedist"
# DOWN_ADD="http://60.204.206.21:38000/download/updatedist"
APP_NAME="ipark-app"
DOWN_NAME="ipark-app"
APP_PATH="/data/project/front/wzyq/ipark-app"
UP_PATH="/data/update_tmp/front"
BK_PATH="/data/backup"

DATE=$(date '+%Y%m%d%H%M%S')

read -p "update_version(don't input appname):" update_version
if [  -n "${update_version}"  ];then
    echo "update_url is:" ${DOWN_ADD}/${APP_NAME}-${update_version}.tar >> ${UP_PATH}/logs/update.log
    wget --user devops --password Zyjc@2023 -O ${UP_PATH}/${APP_NAME}.tar ${DOWN_ADD}/${DOWN_NAME}-${update_version}.tar

elif [ -z "${update_version}"  ];then
    echo "update_url is:" ${DOWN_ADD}/${APP_NAME}.tar >> ${UP_PATH}/logs/update.log
    wget --user devops --password Zyjc@2023 -O ${UP_PATH}/${APP_NAME}.tar ${DOWN_ADD}/${DOWN_NAME}.tar
else
    echo "update_url is:" ${DOWN_ADD}/${APP_NAME}.tar >> ${UP_PATH}/logs/update.log
fi

cd ${APP_PATH}/
mv dist ${BK_PATH}/${APP_NAME}_bk_${DATE}
cp ${UP_PATH}/${APP_NAME}.tar ${APP_PATH}/${APP_NAME}.tar
tar -xvf ${APP_NAME}.tar
ls -a
