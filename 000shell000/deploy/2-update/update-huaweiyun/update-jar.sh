#!/bin/bash

DOWN_ADD="http://ops.patstech.net:38000/download/updatejar"
# DOWN_ADD="http://60.204.206.21:38000/download/updatejar"
APP_NAME="ipark-api-8103"
DOWN_NAME="ipark-api"
APP_PATH="/data/project/jar/ipark-api/myapp"
UP_PATH="/data/update_tmp/jar"
BK_PATH="/data/backup"

DATE=$(date '+%Y%m%d%H%M%S')
read -p "update_version(don't input appname):" update_version
if [ -n "${update_version}" ]; then
    echo "update_url is:" ${DOWN_ADD}/${APP_NAME}-${update_version}.jar >> ${UP_PATH}/logs/update.log
    wget --user devops --password Zyjc@2023 -O ${UP_PATH}/${APP_NAME}.jar ${DOWN_ADD}/${DOWN_NAME}-${update_version}.jar
else
    echo "update_url is:" ${DOWN_ADD}/${APP_NAME}.jar >> ${UP_PATH}/logs/update.log
    wget --user devops --password Zyjc@2023 -O ${UP_PATH}/${APP_NAME}.jar ${DOWN_ADD}/${DOWN_NAME}.jar
fi
cd ${APP_PATH}/
mv ${APP_NAME}.jar ${BK_PATH}/${APP_NAME}.jar_bk_${DATE}
cp ${UP_PATH}/${APP_NAME}.jar ${APP_PATH}/${APP_NAME}.jar
docker restart ${APP_NAME}
sleep 10
docker logs -f ${APP_NAME}

