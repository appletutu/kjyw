#!/bin/bash
# @Date: 2024-07-27 20:17:13

DOWN_ADD="http://ops.patstech.net:38000/download/updatedist"
#DOWN_ADD="http://60.204.206.21:38000/download/updatedist"
UP_PATH="/data/update_tmp/front"      # 更新包临时存放路径
APP_PATH="/data/project/front"        # 软件包部署路径
BK_PATH="/data/backup"                # 备份文件存放路径
LOG_FILE="${UP_PATH}/logs/update.log" # 更新日志存放路径
BK_TIME=$(date '+%Y%m%d%H%M%S')       # 备份时间

UP_NAME="ipark-h5-gxq-68"  # 更新包的名称-研发给的
APP_NAME="ipark-h5-gxq-68" # 实际部署的软件包名称

mkdir -p "${LOG_FILE}"
mkdir -p "${BK_PATH}"

# 定义一个函数来输出带时间戳和级别的日志
function log_message {
    local level=$1
    local message=$2
    local timestamp=$(date '+%Y-%m-%d %H:%M:%S')
    echo -e "${timestamp} [${level}] ${message}" | tee -a "${LOG_FILE}"
}

# 开始日志
log_message INFO "更新开始>>>>>>"
read -p "update_version(don't input appname):" update_version
if [ -n "${update_version}" ]; then
    log_message INFO "即将从 ${DOWN_ADD}/${APP_NAME}.tar 下载更新"
    wget --user devops --password Zyjc@2023 -O ${UP_PATH}/${APP_NAME}.tar ${DOWN_ADD}/${DOWN_NAME}-${update_version}.tar

else
    log_message INFO "即将从 ${DOWN_ADD}/${APP_NAME}.tar 下载更新"
    wget --user devops --password Zyjc@2023 -O ${UP_PATH}/${APP_NAME}.tar ${DOWN_ADD}/${DOWN_NAME}.tar
fi

# 检查wget是否成功
if [ $? -ne 0 ]; then
    log_message ERROR "下载失败，更新中断，请检查下载地址是否正常"
    exit 1 # 非零退出表示失败
fi
log_message INFO "下载成功"
cd ${APP_PATH}/ || {
    log_message ERROR "无法切换到 ${APP_PATH} 目录"
    exit 1
}
mv dist ${BK_PATH}/${APP_NAME}_bk_${BK_TIME} || {
    log_message ERROR "备份 ${APP_PATH} 目录"
    exit 1
} && log_message INFO "dist 备份至 ${BK_PATH}/${APP_NAME}_bk_${BK_TIME} 成功"
cp ${UP_PATH}/${APP_NAME}.tar ${APP_PATH}/${APP_NAME}.tar
tar -xf ${APP_NAME}.tar

# 结束日志
log_message INFO ">>>>>>更新完成"
# 在日志文件中追加一行空行以分隔不同的执行记录
echo "" >>"${LOG_FILE}"
