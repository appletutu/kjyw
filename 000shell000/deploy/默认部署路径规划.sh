
mkdir -p /data/project/{task}
mkdir -p /data/project/front/{ipark-app,ipark-data-view,ipark-manage-web,ipark-portal-web,template,static}
mkdir -p /data/project/jar/{bin,libs,conf,logs,tmp}

mkdir -p /data/project/update_tmp/{front,jar,sql,histroty,logs}
mkdir -p /data/backup/dbbackup

/data/project/
├── task
│   ├── 定时备份
│   └── 定时安全检查
├── front
│   ├── ipark-app
│   ├── ipark-data-view
│   ├── ipark-manage-web
│   ├── ipark-portal-web
│   ├── template
│   └── static
└── jar
    ├──  ipark-api-8103.jar
    ├──  ipark-base-web-8333.jar
    ├──  ipark-job-admin-8888.jar
    ├──  ipark-job-executor-8102.jar
    ├──  ipark-operation-8104.jar
    ├──  ipark-web-8101.jar
    ├──  bin
    │   ├── backup.sh
    │   ├── start.sh
    │   ├── stop.sh
    │   └── restart.sh
    ├── conf
    │   ├── application-api.yml
    │   ├── application-base-web.yml
    │   ├── application-job-executor.yml
    │   ├── application-operation.yml
    │   └── application-web.yml
    ├── logs
    └── tmp

/data/mid/
├── mysql
├── minio
├── nacos
└── xxl-job-admin

/data/update_tmp/
├── front
│   ├── histroty
│   ├── app1.tar
│   └── update-front-app1.sh
├── jar
│   ├── histroty
│   ├── api1.jar
│   └── update-api1.sh
├── logs
└── sql
    ├── histroty
    ├── 20241107.sql
    ├── backup.sh
    └── import.sh




