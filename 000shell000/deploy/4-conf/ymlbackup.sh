#!/bin/bash

BK_PATH="/data/backup/ymlbackup"
APP_PATH="/data/project/jar/"
DATE=$(date '+%Y-%m-%dT%H-%M-%S')

mkdir -p ${BK_PATH}/${DATE}

cp ${APP_PATH}/ipark-*/myapp/*.yml ${BK_PATH}/${DATE}

echo "yml back addr:"${BK_PATH}/${DATE}
ls -l ${BK_PATH}/${DATE}
