# 定义MinIO API和控制台的上游服务器
upstream minio_api {
    least_conn;
    server minio-server-ip:9000; # 替换为MinIO服务器的实际IP和端口
}

upstream minio_console {
    least_conn;
    server minio-server-ip:9001; # 替换为MinIO控制台的实际IP和端口
}

# 配置用于代理MinIO API的server块
server {
    listen 29000;
    server_name minio.example.com; # 替换为你的域名

    location /api/ {
        proxy_pass http://minio_api/;
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_http_version 1.1;
        proxy_set_header Connection "";
        chunked_transfer_encoding off;
    }
}

# 配置用于代理MinIO控制台的server块
server {
    listen 29001;
    server_name minio-console.example.com; # 替换为你的域名

    location /minio/ {
        rewrite ^/minio/(.*) /$1 break;
        proxy_pass http://minio_console/;
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        # 以下设置用于支持WebSocket，如果使用WebSocket，则需要这些设置
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        chunked_transfer_encoding off;
    }
}