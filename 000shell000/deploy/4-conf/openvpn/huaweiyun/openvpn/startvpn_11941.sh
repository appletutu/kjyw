if [ ! -f "/var/run/idc_s2s_server_11941.pid" ];then
  openvpn s2s_idc_11941.conf &
  echo $! > /var/run/idc_s2s_server_11941.pid
else
  echo "idc_s2s_server_11941 was started"
fi
