#!/bin/sh

echo $1
datestr=`date "+%Y%m%d%H%M"`
echo $datestr


if [ "$1" == "auth" ];then
  packname=`ls|grep  auth-service `
  expect -f auth  22  192.168.0.64  Zyjc@2020  $datestr $packname
elif [ "$1" == "basic" ];then
  packname=`ls|grep  basic-service `
  expect -f basic  22  192.168.0.64  Zyjc@2020  $datestr $packname
elif [ "$1" == "business" ];then
  packname=`ls|grep  business-service `
  expect -f business  22  192.168.0.64  Zyjc@2020  $datestr $packname
elif [ "$1" == "datasource" ];then
  packname=`ls|grep  datasource-service `
  expect -f datasource  22  192.168.0.64  Zyjc@2020  $datestr $packname
elif [ "$1" == "gateway" ];then
  packname=`ls|grep  future-gateway ` 
  expect -f gateway  22  192.168.0.64  Zyjc@2020  $datestr $packname
elif [ "$1" == "neighborhood" ];then
  packname=`ls|grep  neighborhood-service ` 
  expect -f neighborhood  22  192.168.0.64  Zyjc@2020  $datestr $packname
elif [ "$1" == "traffic" ];then
  packname=`ls|grep  traffic-service `
  expect -f traffic  22  192.168.0.64  Zyjc@2020  $datestr $packname
elif [ "$1" == "user" ];then
  packname=`ls|grep  user-service ` 
  expect -f user  22  192.168.0.64  Zyjc@2020  $datestr $packname
elif [ "$1" == "pay" ];then
  packname=`ls|grep  pay-service `
  expect -f pay  22  192.168.0.64  Zyjc@2020  $datestr $packname
elif [ "$1" == "mall" ];then
  packname=`ls|grep  mall-service `
  expect -f mall  22  192.168.0.64  Zyjc@2020  $datestr $packname
else
  echo -e "not find sevice\web\n usage: auth  basic  future  property user operation"
fi
