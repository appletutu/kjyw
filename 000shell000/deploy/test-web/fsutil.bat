@echo off
:: 创建一个大小为 200M 的文件，默认单位：字节（B）
fsutil file createnew D:\test.txt 1000
if %ERRORLEVEL% == 0 (
    echo 文件创建成功
) else (
    echo 文件创建失败
)
pause