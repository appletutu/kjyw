#!/bin/bash
###
# @Date: 2024-11-12 16:08:39
# @Description: 示例目录结构：
# /data/project/jar/
# ├── conf
# │   ├── application-app1.yml
# │   ├── application-app2.yml
# │   └── application-app3.yml
# ├── libs
# │   ├── app1.jar
# │   ├── app2.jar
# │   └── app3.jar
# ├── logs
# └── restart-batch.sh
###

# 时间戳
timestamp=$(date +"%Y%m%d%H%M%S")
# 应用程序包部署路径
APP_PATH="/data/project/jar/libs"
LOG_PATH="$APP_PATH/logs"
[ ! -d "$LOG_PATH" ] && mkdir -p "$LOG_PATH"

# 应用程序名称列表（键值对形式）
declare -A APP_MAP
APP_MAP["1"]="ipark-api"
APP_MAP["2"]="ipark-base-web"
APP_MAP["3"]="ipark-job-executor"
APP_MAP["4"]="ipark-operation"
APP_MAP["5"]="ipark-web"

# 显示应用包列表并让用户选择
show_app_list() {
    echo "请选择要启动的应用程序编号（多个编号用空格分隔）："
    for ((i = 1; i <= ${#APP_MAP[@]}; i++)); do
        echo "$i) ${APP_MAP[$i]}"
    done
}

# 读取用户输入的编号，并转换为数组
get_user_selections() {
    read -p "请输入编号选择应用程序: " SELECTED_APPS
    SELECTED_APPS=(${SELECTED_APPS})
    # 检查输入是否为有效编号
    for app_index in "${SELECTED_APPS[@]}"; do
        if [[ ! "${APP_MAP[$app_index]}" ]]; then
            echo "输入的编号 $app_index 无效，请重新运行脚本并选择有效编号。"
            exit 1
        fi
    done
}

# 启动应用
start_app() {
    local app_index=$1
    APP_NAME="${APP_MAP[$app_index]}"
    APP_FILE="${APP_PATH}/${APP_NAME}.jar"
    CONF_FILE="${APP_PATH}/application-${APP_NAME}.yml"
    LOG_FILE="${LOG_PATH}/${APP_NAME}.log"
    # ERR_FILE="$LOG_DIR/${APP_NAME}.err"

    # 检查并终止正在运行的同名JAR文件的进程
    echo "检查并终止正在运行的进程：$APP_NAME..."
    pkill -f "$APP_NAME"
    sleep 2 # 等待进程终止

    # 检查nohup控制台日志文件是否存在，如果存在则备份
    if [ -f "$LOG_FILE" ]; then
        # 创建一个带有时间戳的备份文件名
        backup_log_file="${LOG_FILE}.${timestamp}"
        mv "$LOG_FILE" "$backup_log_file"
        echo "已备份日志文件到 $backup_log_file"
    fi

    # 检查jar文件是否存在
    if [ ! -f "$APP_FILE" ]; then
        echo "Error: Jar 文件 '$APP_FILE' 不存在。"
        exit 1
    fi
    echo "正在启动： ${APP_NAME}..."
    # nohup java -jar "$JAR_PATH" >"$LOG_FILE" 2>"$ERR_FILE" &
    ### 基本启动，定 JVM 选项，配置内置
    nohup java -Xms128m -Xmx256m -jar "$JAR_PATH" >"$LOG_FILE" 2>&1 &
    ### 指定 JVM 选项，配置外挂
    # nohup java -Xms128m -Xmx256m -Dspring.config.location=${CONF_FILE} -jar ${APP_FILE} >>${LOG_FILE} 2>&1 &
    ### 指定 JVM 选项，配置内置，选择环境
    # nohup java -Xms128m -Xmx256m --spring.profiles.active=prod -jar ${APP_FILE} >>${LOG_FILE} 2>&1 &
    # echo "日志正在写入： $LOG_FILE 和 $ERR_FILE。"
    echo "日志写入： $LOG_FILE "
    echo -e "启动成功： $APP_FILE \n"
    sleep 3
}

# 主流程
show_app_list
get_user_selections
if [ ${#SELECTED_APPS[@]} -eq 0 ]; then
    echo "没有选择任何应用程序，将启动所有应用程序。"
    for i in "${!APP_MAP[@]}"; do
        start_app "$i"
    done
else
    for app_index in "${SELECTED_APPS[@]}"; do
        start_app "$app_index"
    done
fi

echo "全部启动完成."
