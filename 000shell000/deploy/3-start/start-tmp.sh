### Spring Boot 参数（如 --spring.profiles.active）需要在应用程序启动时设置，以便应用程序可以读取这些参数并根据它们加载配置。
### 如果你颠倒了 -D 或 --spring 参数和 -jar 参数的顺序，JVM 可能无法正确读取这些参数，导致应用程序无法按预期启动或运行。
###JVM options：这些是影响 JVM 行为的选项，比如 -Xms、-Xmx、-Duser.timezone 等。它们必须放在 -jar 选项之前。
# JVM参数
#JVM_OPTS="-Dname=$AppName  -Duser.timezone=Asia/Shanghai -Xms128m -Xmx256m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=256m -XX:+HeapDumpOnOutOfMemoryError -XX:+PrintGCDateStamps  -XX:+PrintGCDetails -XX:NewRatio=1 -XX:SurvivorRatio=30 -XX:+UseParallelGC -XX:+UseParallelOldGC"

### 对于启动 Spring Boot 应用程序，正确的命令格式如下：
java -jar yourapp.jar [应用程序参数]
java [JVM 参数] -jar yourapp.jar
java [JVM 参数] -jar yourapp.jar [应用程序参数]

java -Xms128m -Xmx256m -Duser.timezone=Asia/Shanghai -Dspring.config.location=/data/project/jar/conf/application.yml -jar yourapp.jar
# 启动Java应用程序，并将PID写入文件
### $!是当前shell的最后一个后台命令的PID，这里用来获取Java应用程序的PID，并将其写入PID_FILE。
nohup java -jar /data/project/jar/yourapp.jar > "/data/project/jar/logs/nohup.log" 2>&1 & echo $! > "/data/project/jar/logs/yourapp.pid"
nohup su - www -c "java -Xms128m -Xmx256m -Dspring.config.location=$dir/application-web-test.yml -jar $dir/$processor >> $dir/logs/nohup.log 2>&1 &" 
nohup su - $username -c "java -Xms128m -Xmx256m -Dspring.config.location=$dir/application-web-test.yml -jar $dir/$processor >> $dir/logs/nohup.log 2>&1" & echo $! > "/data/project/jar/logs/$processor.pid"


# 指定时区
java -Duser.timezone=Asia/Shanghai -jar yourapp.jar
# 指定日志配置文件
java -Djava.util.logging.config.file=logging.properties -jar yourapp.jar
# 指定日志目录,/结尾
java -Dlogging.file.path=/data/project/jar/yourapp-log/ -jar yourapp.jar
# 指定日志文件
java -Dlogging.file.name=/data/project/jar/log/yourapp.log -jar yourapp.jar

# 指定日志目录
java -jar yourapp.jar --logging.level.org.springframework.web=DEBUG

# -Dspring.config.location 设置 Spring Boot 配置文件位置，-D放jar前边
java -Dspring.config.location=/data/project/jar/conf/application.yml -jar yourapp.jar
# --spring.config.location 设置 Spring Boot 配置文件位置，--放jar后边
java -jar yourapp.jar --spring.config.location=/data/project/jar/conf/application.yml --spring.profiles.active=prod
