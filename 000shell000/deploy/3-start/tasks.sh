crontab -e
0 0 * * * sh /data/scripts/dbbackup.sh

# 工作日（周一至周五）早上8点开启，晚上21点关闭
0 8 * * 1-5 /path/to/your/script/on.sh
0 21 * * 1-5 /path/to/your/script/off.sh

# 周末（周六和周日）保持关闭状态，不需要开启————不需要额外的脚本来“保持关闭”

# 周末（周六和周日）每小时检查一次，确保服务处于关闭状态
* * * * 6,7 /path/to/your/script/off.sh