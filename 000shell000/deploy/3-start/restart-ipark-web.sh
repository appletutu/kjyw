#!/bin/bash

#dir=/data/project/jar/ipark-web
dir=/data/project/jar/
processor=ipark-web-8501.jar
time=$(date '+%Y-%m-%dT%H-%M-%S')
total=$(ps -ef | grep $processor | grep -v grep | wc -l)
if [[ $total -ge 1 ]]; then
    echo "停止 $processor 服务（kill 进程: $total）..."
    ps -ef | grep "$processor" | grep -v grep | awk '{print $2}' | xargs kill -9
    # ps -ef | grep $processor | grep -v grep | awk '{print "kill -9 "$2}' | sh
    sleep 2
else
    echo "没有找到 $processor 进程"
    sleep 1
fi
if [[ ! -d $dir/logs ]]; then
    mkdir $dir/logs
fi
mv $dir/logs/nohup.log $dir/logs/nohup.log.$time
# nohup java -jar  > $dir/logs/web.logs &
nohup java -Xms128m -Xmx256m -Dspring.config.location=$dir/application-web-test.yml -jar $dir/$processor >>$dir/logs/nohup.log 2>&1 &
# nohup java -Xms128m -Xmx256m -jar $dir/$processor --spring.profiles.active=prod >>$dir/logs/nohup-$processor.log 2>&1 &
# nohup java -Xms128m -Xmx256m -jar $dir/$processor --spring.config.location=/data/project/jar/conf/application.yml >>$dir/logs/nohup-$processor.log 2>&1 &
# nohup su - www -c "java -Xms128m -Xmx256m -Dspring.config.location=$dir/application-web-test.yml -jar $dir/$processor >> $dir/logs/nohup.log 2>&1 &" 
sleep 1
echo "启动 $processor 成功!"
tail -f "$dir/logs/nohup.log"