#!/bin/sh
# minio version:RELEASE.2024-06-11T03-13-30Z
export MINIO_ROOT_USER=minio
export MINIO_ROOT_PASSWORD=minio@123456
export MINIO_SERVER_URL="http://20.21.1.110:9000/"
export MINIO_BROWSER_REDIRECT_URL="http://20.21.1.110:9001/minio/ui"

/mnt/sdb/minio2024/minio server --address :9010 --console-address :9011 /mnt/sdb/minio2024/data > /mnt/sdb/minio2024/logs/minio.log 2>&1 &
echo "Start success!"
