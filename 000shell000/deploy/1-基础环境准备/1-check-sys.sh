#!/bin/bash

# 检查CPU信息
echo "Checking CPU information..."
model_name=$(lscpu | grep "Model name"| awk '{print $3, $4}')
core_count=$(lscpu | grep "^CPU(s):" | awk '{print $2}')
echo "Model name: $model_name"
echo "Number of cores: $core_count"

# 检查内存信息
echo "Checking memory information..."
mem_info=$(free -h | awk '/Mem:/ {print $2"GB"}')
echo "Total memory: $mem_info"

# 检查存储信息
echo "Checking storage information..."
disk_info=$(df -h | grep "^/dev")
echo $disk_info

fdisk_info=$(fdisk -l | grep "Disk /dev")
echo $fdisk_info


xxl.job.login.username=admin
xxl.job.login.password=123456 ## 数据库中 password 值: e10adc3949ba59abbe56e057f20f883e