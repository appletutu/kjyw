@echo off
setlocal enabledelayedexpansion

:: 设置根目录变量
set "rootDir=D:\yunmas"

:: 设置父目录变量
set "parentDir=D:\yunmas"

:: 检查父目录是否存在，如果不存在则创建
if not exist "%parentDir%" mkdir "%parentDir%"

:: 创建子目录
for %%d in (setuptmp scripts updatetmp backup sql ) do (
    if not exist "%parentDir%\%%d" mkdir "%parentDir%\%%d"
)

echo Directories created successfully.
endlocal