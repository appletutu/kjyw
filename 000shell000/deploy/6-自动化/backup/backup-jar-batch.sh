#!/bin/bash
###
# @Date: 2024-11-12 16:08:39
# @Description: 示例目录结构：
# /data/project/jar/
# ├── conf
# │   ├── application-app1.yml
# │   ├── application-app2.yml
# │   └── application-app3.yml
# ├── libs
# │   ├── app1.jar
# │   ├── app2.jar
# │   └── app3.jar
# ├── logs
# └── backup-jar-batch.sh
###

Backup_PATH=/data/backup/jars
Logs_PATH=/data/backup/logs
APP_PATH=/data/project/jar
APP_DIR="libs"
days=180

# 确保备份目录和日志目录存在
[ ! -d "$Backup_PATH" ] && mkdir -p "$Backup_PATH"
[ ! -d "$Logs_PATH" ] && mkdir -p "$Logs_PATH"

# 定义日志文件路径
log_file="$Logs_PATH/backup.log"
# 函数：添加时间戳并写入日志
log_with_timestamp() {
    local message="$1"
    local timestamp=$(date "+%Y-%m-%d %H:%M:%S")
    echo "${timestamp} - ${message}" | tee -a "$log_file"
}

# 备份项目JAR
log_with_timestamp "Info: Starting backup of $APP_PATH JAR files..."
BackupTimestamp=$(date +%Y%m%d%H%M%S)
cd $APP_PATH
tar -czvf $APP_DIR-$BackupTimestamp.tar.gz  $APP_DIR
mv libs-$BackupTimestamp.tar.gz $Backup_PATH
# 检查备份操作是否成功
if [ "$?" -eq 0 ]; then
    log_with_timestamp "Info: Backup of $APP_PATH JAR files succeeded."
else
    log_with_timestamp "Error: Backup of $APP_PATH JAR files failed."
    exit 1
fi

# 删除180天前的备份文件
log_with_timestamp "Info: Deleting JAR files older than $days days from $Backup_PATH..."
find "$Backup_PATH" -type f -name "libs-*.tar.gz" -mtime +$days -exec rm -rf {} \;
# 检查删除操作是否成功
if [ "$?" -eq 0 ]; then
    log_with_timestamp "Info: Deleting JAR files older than $days days from $Backup_PATH succeeded."    
else
    log_with_timestamp "Error: Deleting JAR files older than $days days from $Backup_PATH failed."
    exit 1
fi


