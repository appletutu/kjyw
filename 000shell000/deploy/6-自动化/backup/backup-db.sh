#!/bin/bash
#数据库信息
dbhost='10.10.10.10'
dbport='3306'
dbuser='root'
dbpasswd='123456'
dbnames=("db1" "db2" "db3")
#备份时间
backtime=$(date '+%Y-%m-%dT%H-%M-%S')
#备份路径
datapath=/data/backup/dbbackup
logpath=$datapath/logs

#如果备份的路径文件夹存在就使用，否则创建
[ ! -d "$datapath" ] && mkdir -p "$datapath"
[ ! -d "$logpath" ] && mkdir -p "$logpath"

# 循环备份数据库
for dbname in "${dbnames[@]}"; do
    echo "$(date '+%Y-%m-%d %H:%M:%S') ,$dbname start backup..." >>"$logpath/dbbackup.log"
    /usr/bin/mysqldump -h$dbhost -P$dbport -u$dbuser -p$dbpasswd $dbname --set-gtid-purged=off --single-transaction | gzip >"$datapath/$dbname-$backtime.sql.gz"

    # 检查备份是否成功
    if [ "$?" == 0 ]; then
        echo "====$(date '+%Y-%m-%d %H:%M:%S'),$dbname 备份成功.====" >>$logpath/dbbackup.log
        #备份成功,删除30天前的备份，只保存30天
        cd $datapath        
        find $datapath -name "*.sql.gz" -type f -mtime +30 -exec rm -rf {} \; >/dev/null 2>&1
        echo "====$(date '+%Y-%m-%d %H:%M:%S'),删除30天前的备份完成.====" >>$logpath/dbbackup.log
    else
        echo "====$(date '+%Y-%m-%d %H:%M:%S'),$dbname 备份失败.====" >>$logpath/dbbackup.log
    fi
done