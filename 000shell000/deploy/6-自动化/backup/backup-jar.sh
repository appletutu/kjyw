#!/bin/bash
###
# @Date: 2024-11-12 16:08:39
# @Description: 示例目录结构：
# /data/project/jar/
# ├── conf
# │   ├── application-app1.yml
# │   ├── application-app2.yml
# │   └── application-app3.yml
# ├── libs
# │   ├── app1.jar
# │   ├── app2.jar
# │   └── app3.jar
# ├── logs
# └── backup-jar.sh
###

Backup_PATH=/data/backup/jars
Logs_PATH=/data/backup/logs

APP_PATH=/data/project/jar/libs
App_Name=ipark-web.jar
App_File=$APP_PATH/$App_Name
days=180

# 确保备份目录和日志目录存在
[ ! -d "$Backup_PATH" ] && mkdir -p "$Backup_PATH"
[ ! -d "$Logs_PATH" ] && mkdir -p "$Logs_PATH"

# 定义日志文件路径
log_file="$Logs_PATH/backup.log"
# 函数：添加时间戳并写入日志
log_with_timestamp() {
    local message="$1"
    local timestamp=$(date "+%Y-%m-%d %H:%M:%S")
    echo "${timestamp} - ${message}" | tee -a "$log_file"
}

# 检查原文件是否存在
if [ ! -f "$App_File" ]; then
    log_with_timestamp "Error: $App_File does not exist."
    exit 1
fi

# 备份项目JAR
BackupTimestamp=$(date +%Y%m%d%H%M%S)
log_with_timestamp "Info: Starting backup of $App_Name JAR files..."
cp -r "$App_File" "$Backup_PATH/$App_Name.$BackupTimestamp"
if [ "$?" -eq 0 ]; then
    log_with_timestamp "Info: Backup of $App_Name JAR succeeded."
else
    log_with_timestamp "Error: Backup of $App_Name JAR failed."
    exit 1
fi

# 删除180天前的JAR备份文件
log_with_timestamp "Info: Deleting JAR files older than $days days from $Backup_PATH..."
find "$Backup_PATH" -type f -name "$App_Name.*" -mtime +$days -exec rm -rf {} \;
if [ "$?" -eq 0 ]; then
    log_with_timestamp "Info: Deleting JAR files older than $days days from $Backup_PATH succeeded."
else
    log_with_timestamp "Error: Deleting JAR files older than $days days from $Backup_PATH failed."
    exit 1
fi
#ls -l ${Backup_PATH}