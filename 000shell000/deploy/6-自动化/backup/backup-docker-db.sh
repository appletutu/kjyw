#!/bin/bash
#db info
dbhost='10.10.10.10'
dbport='3306'
dbuser='root'
dbpasswd='123456'
dbnames='xxl_job db01 db02 db03'
#backup time
backtime=$(date '+%Y-%m-%dT%H-%M-%S')
#backup path
datapath=/data/backup/dbbackup
logpath=$datapath/logs

[ ! -d "$datapath" ] && mkdir -p "$datapath"
[ ! -d "$logpath" ] && mkdir -p "$logpath"

#start backup db
for dbname in $dbnames; do
    echo "$(date '+%Y-%m-%d %H:%M:%S'),$table start backup..." >>$logpath/dbbackup.log
    docker exec mysql-client mysqldump -h$dbhost -P$dbport -u$dbuser -p$dbpasswd $table --set-gtid-purged=off --single-transaction | gzip >$datapath/$table-$backtime.sql.gz
    #check
    if [ "$?" == 0 ]; then
        echo "$(date '+%Y-%m-%d %H:%M:%S'),$table backup success..." >>$logpath/dbbackup.log
        #del after 14 day backup
        cd $datapath        
        find $datapath -name "*.sql.gz" -type f -mtime +14 -exec rm -rf {} \; >/dev/null 2>&1
        echo "$(date '+%Y-%m-%d %H:%M:%S'),del after 14 day backup success." >>$logpath/dbbackup.log
    else
        echo "$(date '+%Y-%m-%d %H:%M:%S'),$table backup faild!!" >>$logpath/dbbackup.log
    fi
done
