#!/bin/sh
#备份路径
LocalDir=$(cd "$(dirname $0)";pwd)
BackupDir=/data/backup/front-backup
SrcPackageName=dist
DesPackageName=sass-data-vision
#如果备份的路径文件夹存在就使用，否则创建
[ ! -d "$BackupDir"  ]  && mkdir -p "$BackupDir" 
if [ -f "$BackupDir/${PackageName}-$(date +%F).tar.gz" ];then
echo "======${PackageName}备份文件已存在："
else
    tar -zcvPf ${DesPackageName}-$(date +%F).tar.gz  $LocalDir/${SrcPackageName}
    mv ${DesPackageName}-$(date +%F).tar.gz $BackupDir/
fi
ls -l /data/backup/pkgbackup/