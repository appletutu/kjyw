#!/bin/bash
###
# @Date: 2024-11-12 16:08:39
# @Description: 示例目录结构：
# /data/project/jar/
# ├── conf
# │   ├── application-app1.yml
# │   ├── application-app2.yml
# │   └── application-app3.yml
# ├── libs
# │   ├── app1.jar
# │   ├── app2.jar
# │   └── app3.jar
# ├── logs
# └── backup-jar-cycle.sh
###

Backup_PATH=/data/backup/jars
Logs_PATH=/data/backup/logs
APP_PATH=/data/project/jar/libs
days=180

# 应用程序名称列表（键值对形式）
declare -A APP_MAP
APP_MAP["1"]="ipark-api.jar"
APP_MAP["2"]="ipark-base-web.jar"
APP_MAP["3"]="ipark-job-executor.jar"
APP_MAP["4"]="ipark-operation.jar"
APP_MAP["5"]="ipark-web.jar"

# 确保备份目录和日志目录存在
[ ! -d "$Backup_PATH" ] && mkdir -p "$Backup_PATH"
[ ! -d "$Logs_PATH" ] && mkdir -p "$Logs_PATH"

# 定义日志文件路径
log_file="$Logs_PATH/backup.log"
# 函数：添加时间戳并写入日志
log_with_timestamp() {
    local message="$1"
    local timestamp=$(date "+%Y-%m-%d %H:%M:%S")
    echo "${timestamp} - ${message}" | tee -a "$log_file"
}

# 备份项目JAR
BackupTimestamp=$(date +%Y%m%d%H%M%S)
# 按备份时间创建一个备份目录
curBackup_PATH=$Backup_PATH/libs-${BackupTimestamp}
[ ! -d "$curBackup_PATH" ] && mkdir -p "$curBackup_PATH"
# 循环备份项目所有JAR
for app_key in "${!APP_MAP[@]}"; do
    App_Name=${APP_MAP[$app_key]}
    log_with_timestamp "Info: Starting backup of $App_Name JAR files..."
    cp -r "$APP_PATH/$App_Name" "$curBackup_PATH/"

    # 检查备份是否成功
    if [ "$?" -eq 0 ]; then
        log_with_timestamp "Info: Backup of $App_Name JAR files succeeded."
    else
        log_with_timestamp "Error: Backup of $App_Name JAR files failed."
        exit 1
    fi

done

# 删除180天前的备份文件
log_with_timestamp "Info: Deleting JAR files older than $days days from $Backup_PATH..."
find "$Backup_PATH" -type d -mtime +$days -exec rm -rf {} \;
# 检查删除操作是否成功
if [ "$?" -eq 0 ]; then
    log_with_timestamp "Info: Deleting JAR files older than $days days from $Backup_PATH succeeded."
else
    log_with_timestamp "Error: Deleting JAR files older than $days days from $Backup_PATH failed."
    exit 1
fi
