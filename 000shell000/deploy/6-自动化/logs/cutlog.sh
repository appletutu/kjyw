#!/bin/bash
# @Date: 2024-07-29 17:51:01
YESTERDAY=$(date --date "yesterday"  +"%Y-%m-%d")
LOGS_PATH=/data/nginx/logs
PID_PATH=/data/nginx/logs/nginx.pid
mv ${LOGS_PATH}/access.log ${LOGS_PATH}/access_${YESTERDAY}.log
mv ${LOGS_PATH}/error.log ${LOGS_PATH}/error_${YESTERDAY}.log
# nginx自己编写了代码处理当接到USR1信号的时候，让nginx重新打开日志文件如果文件不存在，会自动创建一个新的文件xxx.log
kill -USR1 `cat ${PID_PATH}`
