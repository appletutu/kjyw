rpm -qa | grep logrotate

yum install logrotate -y


sudo vi /etc/logrotate.d/weblogs
/data/web/logs/*.log {
    daily
    rotate 183
    missingok
    notifempty
    compress
    delaycompress
    create 640 root root
    dateext
    dateformat .%Y-%m-%d
    postrotate
        # 如果你的web服务是nginx，使用以下命令重启nginx
        # systemctl restart nginx > /dev/null
        # 如果你的web服务是apache，使用以下命令重启apache
        # systemctl restart httpd > /dev/null
        # 如果你的程序是一个Java应用程序（如myapp.jar）
        # [ -f /var/run/myapp.pid ] && kill -HUP `cat /var/run/myapp.pid`
    endscript
}

sh /data/web/start.sh
logrotate -f /etc/logrotate.d/weblogs

crontab -e
0 0 * * * logrotate -f /etc/logrotate.d/weblogs

配置文件的含义如下：

/data/web/logs/*.log：指定了需要轮转的日志文件路径和通配符。
daily：指定日志文件按天轮转。
rotate 183：指定保留183天的日志文件。
missingok：如果日志文件缺失，不报错。
notifempty：如果日志文件为空，则不进行轮转。
compress：轮转后的日志文件进行压缩。
delaycompress：在第一次轮转后延迟压缩，以确保日志文件被完全写入。
create 640 root root：创建新的日志文件，并设置权限和所有者。
dateext：这个指令告诉logrotate在轮转时添加日期后缀。
dateformat .%Y-%m-%d：这个指令定义了日期后缀的格式。%Y代表四位数的年份，%m代表月份，%d代表日。点（.）是后缀的分隔符。
postrotate和endscript：在日志轮转后执行的命令，这里用于重启web服务。