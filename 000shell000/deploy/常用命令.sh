############################## crontab ############################################################
yum -y install vixie-cron
yum -y install crontabs

systemctl start crond.service
systemctl status crond.service
systemctl restart crond.service
systemctl enable crond.service

crontab -l
crontab -e

vim /etc/crontab         
crontab /etc/crontab

0 0 * * * sh /data/nginx/conf/vhost/aqcheck.sh

############################## find sed grep ############################################################
ls -i   # 查看文件i节点号
find . -inum 123456 -exec rm {} \;                      # 删除节点为123456的文件；
find ./ -inum 123456 -print -exec rm {} -rf \;          # 删除乱码名的文件夹
find . -inum 123456 -exec mv {} hello.txt \;            # 重命名一个乱码名的文件
find ./ -mtime +7 -name "*.log.ge" -exec rm -rf {} \;   # 删除30天前文件，按文件名删除
find ./ -type f -mtime +180 -exec rm -rf {} \;          # 删除180天前的文件，按文件类型删
find /path/to/directory -type f -exec grep -P -l $'[^\x20-\x7E]' {} \;                    # 查找乱码文件-无效
find /path/to/directory -type f -exec grep -P -l $'[^\x20-\x7E]' {} \; -exec rm -f {} \;  # 删除乱码文件-无效
ls -il /path/to/directory                                        # 查询-inum号
find /path/to/directory -inum [inode-number] -exec rm -f {} \;   # 删除乱码文件，根据-inum号


# 查找最大文件的 3 种方法 |ls、find、du
sudo ls -lSh /data | head -10

sudo find /data -type f -exec ls -s {} \; | sort -n | tail -n 10
sudo find /data -type f -printf "%s\t%p\n" | sort -nr | head -10
sudo find /data -type f -printf "%s\t%p\n" | sort -n | tail -10

sudo find / -size +100M-ls                 # 显示 / 目录下大于100MiB(注意不是100MB)
sudo find / -size +100M-size -200M-ls      # 显示 / 目录下 100MiB 和 200MiB 之间的文件

# du
sudo du -ach --max-depth=1 / | sort -n          # 查看哪个目录占用过高
sudo du -a | sort -n -r | head -n 10            # 查找当前文件夹中最大的 10 个目录
sudo du -hs * | sort -rh | head -n 10           # 显示可读的KB、MB、GB信息，可以加上 -h 参数
sudo du -Sh| sort -rh | head -n 10              # 查找最大目录/文件(包括子文件夹)
sudo du -h -a / | grep "[0-9]G\b"               # 查找大小在 GB 范围内的所有文件--同时使用 du 命令和 grep 命令
sudo du -a /data | sort -n -r | head -n 10      # 查找 / 下前 10 个最大的文件



grep -Ev "^$|^#" filename                               # 去掉空行及注释命令
grep -Ev "^$|^.*#|^[#;]" /etc/squid/squid.conf          # 搜索不包含#注释行、空行的行，#前有空格也不包含
grep "关键字" *   # 查询当前目录下所有文件中的关键字所在行

## notepad++批量删除注释、去除行号、注释&取消注释  | 正则表达式
/\*{1,2}[\s\S]*?\*/          # 删除注释 /* */
\/\/[\s\S]*?\n               # 删除注释 //
\/\/[^\n]*                   # 删除注释 //,[^\n]* 除换行符之外的任何字符
(?<!http:|https:)\/\/[^\n]*  # 删除单行注释,
<!--[\s\S]*?-->              # 删除注释 <!-- -->
^\s*\n                       # 删除空白行
## 末尾的 /g 是指全局匹配

cat /dev/null > minioc.log

tail -n 10  test.log                       # 查询日志尾部最后10行的日志;
head -n 10  test.log                       # 查询日志文件中的头10行日志;
cat error.log | grep -B 5 'nick'           # 显示nick及前5行
cat error.log | grep -A 5 'nick'           # 显示nick及后5行
cat error.log | grep -C 5 'nick'           # 显示file文件里匹配nick字串那行以及上下5行
cat error.log | grep -n -B10 -A10 5 'nick' # 显示file文件里匹配nick字串前后10行

grep '2024-03-26 15' nohup.out | head -n 10
sed -n '/2014-12-17 16:17:20/,/2014-12-17 16:17:36/p'  test.log  # 特别说明:该命令中的两个日期值必须是日志文件中包含的值,否则该命令无效.； 先 grep '2024-03-26 15' nohup.log 来确定日志中是否有该时间点
cat -n test.log |grep "debug" |more        # 分页打印,通过点击空格键翻页
cat -n test.log |grep "debug"  >debug.txt  
grep -n '日志排查' test.log | grep '日志'  # and 条件
grep -n -E '日志排查|hello' test.log       # or 条件用“-E”，满足两个关键字的都可以找出来

ssh 10.79.242.3
ssh root@192.168.0.203
ssh root@192.168.0.203 "uname -a"
ssh -p 2200 root@192.168.0.203 uname -a
scp local_file remote_username@remote_ip:remote_folder
scp -r vhost.bak.tar  root@192.168.0.252:/root/ 
scp -r vhost.bak.tar  root@192.168.0.252:/root/
scp test.txt 10.79.242.9:/data/update/


############################## 查看进程 ##############################################################
ps -ef | grep nginx
ps -ef | grep java 
ps -ef | grep java | awk '{print $1" "$2" "$5" "$7" "$12" "$13}'
ps -ef | grep '[j]ava' | awk -F'java' '{print $2}'

ps -ef | grep wlsq | grep -v grep | awk '{print $1" "$2" "$9}'

ps -ef | grep "xxx.jar" | grep -v grep | awk '{print $2}'| xargs kill -9
sleep 2

# kill 端口
fuser -k 22/tcp

############################## firewalld 防火墙、SELINUX ###############################################
systemctl status firewalld
systemctl start firewalld
systemctl stop firewalld
systemctl reload firewalld
firewall-cmd --list-all
firewall-cmd --add-port=80-/tcp --permanent

getenforce 0


systemctl stop firewalld
sed -i  '/^SELINUX/s/=.*/=disabled/' /etc/selinux/config  # 关闭 selinux
setenforce 0

firewall-cmd --list-all

sudo yum install -y yum-utils

### win ###
netsh firewall show state
netsh firewall set opmode disable
netsh  firewall set opmode enable
############################## Nginx ##############################################
echo  "This is a test page." >> index.html

sudo systemctl enable nginx
sudo systemctl start nginx
sudo systemctl stop nginx
sudo systemctl restart nginx
sudo nginx -t
sudo systemctl reload nginx


sudo service start nginx
sudo service stop nginx
sudo service restart nginx
sudo service reload nginx



ps -ef | grep nginx
nginx -v
./nginx
./nginx -c nginx.conf
./nginx -s stop
./nginx -s quit
kill -9 nginx

./nginx -t         # 检查配置是否有语法错误
./nginx -T         # 查看当前Nginx最终的配置。
./nginx -s reload  # 重新加载配置文件
./nginx -s reope   # 重启

ln -s /data/nginx/sbin/nginx /usr/local/sbin/
echo "/data/nginx/sbin/nginx -t" >>ntest.sh
echo "/data/nginx/sbin/nginx -s reload" >>nreload.sh

############################## yum #########################################################
# 修改yum源
cd /etc/yum.repos.d/
mkdir bak
mv *repo bak/

cd /etc/yum.repos.d/
wget http://mirrors.163.com/.help/CentOS6-Base-163.repo
vim CentOS6-Base-163.repo
修改gpgcheck=0，原值gpgcheck=1

curl -o /etc/yum.repos.d/BCLinux.repo https://mirrors.cmecloud.cn/bclinux/oe21.10/OS/x86_64/
curl -o /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo
curl -o /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-7.repo
curl -o /etc/yum.repos.d/docker-ce.repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

# 修改 yum 配置文件，替换掉$releasever
vi Centos-7.repo
:%s/$releasever/7/g  # 把整个文件下的 $releasever 替换为 7(真实的版本)
sed -i 's/gpgcheck=1/gpgcheck=0/g' /etc/yum.repos.d/Centos-7.repo
sudo sed -i 's+https://download.docker.com+http://mirrors.cmecloud.cn/docker-ce+' /etc/yum.repos.d/docker-ce.repo
sudo sed -i 's+$releasever+7+' /etc/yum.repos.d/docker-ce.repo

# 生成缓存
sudo yum clean all && yum makecache
sudo yum clean all
sudo yum makecache
sudo yum makecache fast  # 更新索引列表
rpm --rebuilddb
sudo yum update
sudo yum install -y epel-release  # 安装epel扩展源

### yum install 解压缩工具 
yum list | grep zip/unzip
yum install zip -y
yum install unzip -y

unzip test.zip             # 解压至当前目录
unzip -o test.zip -d tmp/  # -d 解压至指定目录tmp/，-o 如果存在相同文件则覆盖
unzip -O CP936 test.zip    # 中文文件名会乱码，指定字符集(用GBK, GB18030也可以) ，-O是大写
unzip -l test.zip          # 看一下zip压缩包中包含哪些文件，不进行解压缩

zip test.zip  test.txt     # 压缩文件
zip -r test.zip test/      # 压缩文件夹

yum install -y tar
tar -cvf  xxx.tar  test.txt      # 压缩
tar -xvf  xxx.tar                # 解压缩包
tar -xvf  xxx.tar -C tmp/        # 解压至指定目录tmp/
tar -cvzf xxx.tar.gz  test.txt   # 以tar.gz方式打包并gz方式压缩，-f必须是最后
tar -xvzf xxx.tar.gz -C tmp/     # 解压至指定目录tmp/

### yum install 字符集
echo $LANG                          # 查看系统当前字符集
locale                              # 查看当前系统默认采用的字符集
yum -y groupinstall chinese-support # 安装中文字符集
export LANG="zh_CN.UTF-8"           # 修改系统字符集为中文，临时修改(当前终端生效)
echo 'export LANG="zh_CN.UTF-8"'  >> /etc/proflile   # 修改系统字符集为中文，永久修改
source /etc/profile


############################## docker ###########################################################################
### 安装docker
sudo yum install -y yum-utils
sudo yum list docker-ce --showduplicates | sort -r 
sudo yum install -y docker-ce

sudo systemctl enable docker   # 开机启动
sudo systemctl daemon-reload   # 重载systemctl
sudo systemctl start docker    # 启动服务
sudo systemctl stop docker     # 停止服务
sudo systemctl restart docker  # 重启服务
sudo systemctl status docker   # 查看状态
sudo systemctl disable docker  # 开机禁用
sudo systemctl stop docker.socket  # 停止访问自动唤醒（docker在关闭状态下被访问自动唤醒机制）

### 修改docker数据目录
systemctl stop docker
mkdir -p /data/lib/
cd /var/lib/
mv docker /data/lib/
ln -s /data/lib/docker /var/lib/docker
systemctl restart docker


### docker 常用命令
docker images  # 查看镜像

docker logs [容器名 | 容器ID ]
docker logs -f  [容器名 | 容器ID ] #表示实时的跟踪日志输出
docker logs --since 30m myredis # 此处 --since 30m 是查看此容器30分钟之内的日志情况。
docker logs --since="07-29" --tail=10 myweb
docker logs --tail=1000  myweb
docker logs myweb | grep esop
dmesg | grep xxx   # 查看日志

df -h
docker system df
docker rmi <IMAGE_ID_1>  # 删除指定的镜像
docker system prune -a   # 清理 Docker 系统中的不再使用的镜像、缓存、容器和网络等资源


docker stop redis
docker start redis
docker inspect 容器ID

docker exec -it  redis bash

docker export f299f501774c > redis.tar
docker import - redis < redis.tar
docker import  my_ubuntu_v3.tar runoob/ubuntu:v4

docker save 0fdf2b4c26d3 > redis.tar
docker save -o images.tar postgres:9.6 mongo:3.4   # -o  有名字
docker load < images.tar


############################## 特定软件的离线yum源 ###########################################
yum install yum-utils -y 
repotrack docker-ce    #（推荐）
yumdownloader --resolve --destdir=/data/local_repo/ docker-ce
yum install --downloadonly --downloaddir=/data/local_repo docker-ce 

rpm -Uvh --force --nodeps *.rpm                 # 少量rpm的直接安装
rpm -ivh --force --nodeps *.rpm

createrepo /data/local_repo/docker-ce           # 生成repodata索引依赖

 vi /etc/yum.repos.d/docker-ce.repo
[docker-ce-stable]
name=dokcer-ce
baseurl=file:///data/local_repo/docker-ce       # 本地rpm存放路径
gpgcheck=0
enabled=1


############################## 网络 域名 SSl ############################################################
ping www.qq.com -t               # 不间断ping
ping www.qq.com -l 1300          # 1300字节为例ping测试
ping www.qq.com -n 10            # 以10个数据包为例
ping www.qq.com -S 192.168.1.11  # 指定源IP为192.168.1.11 ping QQ站点连通性
telnet www.baidu.com
telnet www.baidu.com 443
netstat -a
netstat -n
netstat -r
tracert ops.patstech.net
traceroute ops.patstech.net
traceroute -d www.qq.com                # 路由跟踪过程不进行主机名解析，可较为明显缩短tracert等待时长
tracert -S 192.168.1.10  www.baidu.com  # 指定源IP为192.168.1.10
ipconfig
ipconfig /all
ipconfig /release  # 释放当前获取的所有IP参数
ipconfig /renew    # 重新向DHCP服务器请求IP参数
route print
route add 10.10.10.1 mask 255.255.255.0 192.168.1.254 metric 5
route delete 10.10.10.1
nslookup www.baidu.com
nslookup www.baidu.com 114.114.114.114
nbtstat -n
arp -a             # 
arp -a IP
arp -s 192.168.1.3 11-22-33-44-55-77    # ARP高速缓存中人工输入一个静态项目
arp -d IP                               # 使用本命令能够人工删除一个静态项目

# 获取本机 ip 地址
ip addr | awk '/^[0-9]+: / {}; /inet.*global/ {print gensub(/(.*)\/(.*)/, "\\1", "g", $2)}'
ifconfig -a|grep inet|grep -v 127.0.0.1|grep -v inet6|awk '{print $2}'|tr -d "addr:"

# nslookup 
yum install bind-utils

curl ip.sb
curl cip.cc
curl ifconfig.me
curl ipinfo.io/ip

# 查看证书有效期
openssl x509 -in /data/nginx/conf/www.nbqy.gov.cn.pem -noout -dates
openssl x509 -in /data/nginx/conf/mzxzzhyq.com.pem -noout -dates

# 查看证书详情
openssl x509 -in /data/nginx/conf/www.nbqy.gov.cn.pem -text -noout

# 查看 SSL 证书本身（编码）
echo | openssl s_client -servername www.nbqy.gov.cn -connect www.nbqy.gov.cn 2>/dev/null | openssl x509

# 在 Linux 中检查 SSL 证书有效性
echo | openssl s_client -servername www.mzxzzhyq.com -connect www.nbqy.gov.cn:443 2>/dev/null | openssl x509 -noout -dates


# 显示 SSL 证书的所有上述信息
echo | openssl s_client -servername www.nbqy.gov.cn -connect www.nbqy.gov.cn:443 2>/dev/null | openssl x509 -noout -issuer -subject -dates


###################################################################################################
############################## 工具平台 ############################################################
SQLAdvisor    # 开源SQL审计平台
Archery       # 开源漏洞评估系统

#### 常规做法
淘宝密文字段检索方案：https://open.taobao.com/docV3.htm?docId=106213&docType=1
阿里巴巴文字段检索方案：https://jaq-doc.alibaba.com/docs/doc.htm?treeId=1&articleId=106213&docType=1
拼多多密文字段检索方案：https://open.pinduoduo.com/application/document/browse?idStr=3407B605226E77F2
京东密文字段检索方案：https://jos.jd.com/commondoc?listId=345
#### 超神做法
数据库中字符数据的模糊匹配加密方法：https://www.jiamisoft.com/blog/6542-zifushujumohupipeijiamifangfa.html
一种基于BloomFilter的改进型加密文本模糊搜索机制研究：http://kzyjc.cnjournals.com/html/2019/1/20190112.htm
支持快速查询的数据库如何加密：https://www.jiamisoft.com/blog/5961-kuaisuchaxunshujukujiami.html
基于Lucene的云端搜索与密文基础上的模糊查询：https://www.cnblogs.com/arthurqin/p/6307153.html
http://jeit.ie.ac.cn/fileDZYXXXB/journal/article/dzyxxxb/2017/7/PDF/160971.pdf