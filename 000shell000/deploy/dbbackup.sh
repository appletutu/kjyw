mkdir -p /data/backup/dbbackup
mkdir -p /data/scripts
mkdir -p /data/update_tmp/sql/logs

vi import-sql.sh
vi backup-sql.sh

SHOW DATABASES;
SELECT DATABASE();
SHOW TABLES;
DESC table_name;

# 连接数据库
mysql -uroot -pUZlgBSJnHvHa6kAn -D zhyq_mz

# 备份数据库 - 指定表
mysqldump -uroot -p ipark-db sys_menu >sys_menu-$(date +%F).sql
# 备份数据库 - 指定数据库
mysqldump -uroot -p ipark_scalpel >ipark_scalpel-$(date +%F).sql
mysqldump -uroot -p xxl_job >xxl_job-$(date +%F).sql
# 备份数据库 - 全库
mysqldump -uroot -p --set-gtid-purged=off --single-transaction --all-databases > alldb.sql
# 备份数据库 - 压缩备份
mysqldump -uroot -p --set-gtid-purged=off --single-transaction --all-databases | gzip > alldb.sql


# 物业巡检码
mysqldump -uroot -p7Zj676gqwg1pgrSP wyxjm >wyxjm-$(date +%F).sql
mysql -uroot -p7Zj676gqwg1pgrSP -D wyxjm 

# 融学院
mysql -urongxueyuan -prongxueyuan_1# -h10.76.168.86 -P7001 -D rongxueyuan
mysqldump -urongxueyuan -p -h10.76.168.86 -P7001 rongxueyuan | gzip >rongxueyuan-$(date +%F).sql

# 江北
mysqldump -uroot -pJSx2w9S2OFgwG7bh itown_nbjb >itown_nbjb-$(date +%F).sql

# 路桥
mysqldump -uroot -pQ3rhr2v7iPyci1WU ipark_scalpel >ipark_scalpel-$(date +%F).sql

# 义乌陆港
/usr/local/mysql/bin/mysqldump -uroot -ppC9^fZ4[hM3# ipark-db-cs >ipark-db-cs_bk_$(date '+%Y%m%d%H%M%S').sql
/usr/local/mysql/bin/mysqldump -uroot -ppC9^fZ4[hM3# ipark-db-cs >ipark-db-cs_bk_$(date '+%Y%m%d%H%M%S').sql

## 导出指定数据：只能用select
mysql -uuser -ppwd -hhost -Pport dbname -A -e "use rongxueyuan; set names gbk;select * from t_wg_voucher_used_deal_201406" >/data/backup/dbbackup/rongxueyuan-$(date +%F)

====docker - mysql====================================================
# 连接数据库
docker exec -it mysql-client mysql -h10.195.159.201 -P33390 -ujc_yq -pZhyq@2024 jc_yq

# 导出数据库
docker exec -i mysql-client mysqldump -h10.195.159.201 -P33390 -ujc_yq -pZhyq@2024 jc_yq --set-gtid-purged=off --single-transaction | gzip >/data/dbbackup/jc_yq-$(date +%F).sql.gz
docker exec -i mysql-client mysqldump -h10.195.159.201 -P33390 -ujc_yq -pZhyq@2024 jc_yq --set-gtid-purged=off --single-transaction >./bk_$(date '+%Y%m%d%H%M%S').sql

docker exec -i mysql-client mysqldump -hIP -P33390 -u数据库用户名 数据库名 --set-gtid-purged=off --single-transaction >./bk_$(date '+%Y%m%d%H%M%S').sql

# 导入导数据库
docker exec -i mysql-client mysql -h10.195.159.201 -P33390 -ujc_yq -pZhyq@2024 jc_yq <sys_menu-2024-05-30.sql

mysql -uroot -pUZlgBSJnHvHa6kAn -D zhyq_mz <

# 更新前备份包
tar -zcvf /data/backup/scalpel-h5-$(date +%F).tar.gz dist
tar -zcvf /data/backup/scalpel-manage-$(date +%F).tar.gz dist
ls /data/backup

show table status \G

crontab -e
0 0 * * * sh /data/scripts/dbbackup.sh

