#!/bin/bash
#
###
# Filename: install_redis.sh
# Author: ping.liang - appletutututu@gmail.com
# Description: 
# Last Modified: 2020-05-05 15:15
# centos 6
# 部署redis  sentinel脚本
###

Host=$2

REDIS_PORT="7002"
REDIS_AUTH="5JwH3j2PyWf8"
REDIS_CONFIG_FILE="/home/yunmas/redis/etc/${REDIS_PORT}.conf"
REDIS_LOG_FILE="/home/yunmas/redis/logs/sentinel_26381.log"

case "$1" in

start)
### start
/home/yunmas/redis/bin/redis-server /home/yunmas/redis/etc/${REDIS_PORT}.conf &
/home/yunmas/redis/bin/redis-server /home/yunmas/redis/etc/sentinel-26381.conf --sentinel &
echo Redis start successfully 
### start
 ;;

 stop)
### stop
ps -ef | grep redis | grep sentinel | grep -v grep | grep -v sh | awk '{print $2}' | xargs kill
ps -ef | grep redis | grep server | grep -v grep | grep -v sh | awk '{print $2}' | xargs kill
echo Redis stop successfully
### stop
 ;;

 restart)
echo restart
 ;;
 
 master)
###master
sed -i "s@^# masterauth <master-password>@masterauth '"${REDIS_AUTH}"'@" ${REDIS_CONFIG_FILE}
echo config master ok.
### master
 ;;

  slave)
###slave
sed -i "s@^# masterauth <master-password>@masterauth '"${REDIS_AUTH}"'@" ${REDIS_CONFIG_FILE}
echo "slaveof $Host ${REDIS_PORT} " >> ${REDIS_CONFIG_FILE}
echo "slave-read-only yes" >> ${REDIS_CONFIG_FILE}
echo config slave ok.
### slave
 ;;

  sentinel)
###sentinel
cat >/home/yunmas/redis/etc/sentinel-26381.conf<<EOF
port 26381
# sentinel announce-ip <ip>
# sentinel announce-port <port>
daemonize yes
protected-mode no
pidfile /var/run/redis-sentinel.pid
logfile "${REDIS_LOG_FILE}"
dir /tmp
################################# master001 #################################
sentinel monitor mymaster $Host ${REDIS_PORT} 2
sentinel auth-pass mymaster ${REDIS_AUTH}
sentinel down-after-milliseconds mymaster 30000
sentinel parallel-syncs mymaster 1
sentinel failover-timeout mymaster 18000
sentinel deny-scripts-reconfig yes
# sentinel notification-script <master-name> <script-path>
# sentinel client-reconfig-script <master-name> <script-path>
# 可以配置多个master节点
################################# master002 #################################
EOF
echo sentinel.conf ok.
### sentinel
 ;;

 uninstall)
echo uninstall
 ;;

 *)
 echo "Usage: $SCRIPTNAME {start|stop|restart|install|uninstall}" >&2
 exit 3
 ;;
esac
