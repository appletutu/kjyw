#!/bin/bash
# Author:  liangping
# Create:  2020-07-02


# 以下参数将会在调用脚本时传给脚本: 
# <master-name> <role> <state> <from-ip> <from-port> <to-ip> <to-port> 
# 目前<state>总是“failover”, 
# <role>是“leader”或者“observer”中的一个。 
# 参数 from-ip, from-port, to-ip, to-port是用来和旧的master和新的master(即旧的slave)通信的

DATE=$(date "+%Y-%m-%d %H:%M:%S")
hostip=$(/sbin/ifconfig |grep inet|grep -v 127.0.0.1|grep -v inet6|awk '{print $2}'|tr -d "addr:")

echo "$DATE 发生了 reconfig 事件." >>sendmail.log

mastername=$1
role=$2 
state=$3
fromip=$4
fromport=$5
toip=$6
toport=$7

echo -e "[主机地址]:$hostip\n\
[事件时间]:$DATE\n\
[事件标题]:redis异常\n\
[事件描述]:\n\
    masterName:$mastername\n\
    role:$role\n\
    state:$state\n\
    fromIp:$fromip\n\
    fromPort:$fromport\n\
    toIp:$toip\n\
    toPort:$toport" |mail -s "【YUNMAS 告警信息！！！】" -c "18767126025@139.com" "hzliangping@139.com"
