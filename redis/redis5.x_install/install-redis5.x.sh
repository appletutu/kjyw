#!/bin/bash
#
###
# Filename: install_redis.sh
# Author: ping.liang - appletutututu@gmail.com
# Description: 
# Last Modified: 2020-05-05 15:15
# Version: 1.0
###

# set parameters
REDIS_VERSION="5.0.8"
REDIS_USER=""
REDIS_AUTH="5JwH3j2PyWf8"

REDIS_INSTALL_DIR="/home/yunmas/redis"
REDIS_PORT="7002"
REDIS_CONFIG_FILE="${REDIS_INSTALL_DIR}/etc/${REDIS_PORT}.conf"
REDIS_LOG_FILE="${REDIS_INSTALL_DIR}/logs/redis_${REDIS_PORT}.log"
REDIS_DATA_DIR="${REDIS_INSTALL_DIR}/data"	# dump.rdb文件存放路径
REDIS_EXEC="${REDIS_INSTALL_DIR}/bin/redis-server"
CLI_EXEC="${REDIS_INSTALL_DIR}/bin/redis-cli"
PIDFILE="/var/run/redis_${REDIS_PORT}.pid"


# install needed packages
for P in gcc gcc-c++ tcl
do
 rpm -qa $P &> /dev/null
 if [ $? -ne 0 ];then
 echo -n "install $P..."
 yum install -y $P &> /dev/null
 echo "ok"
 else
 echo "$P already installed."
 fi
done

# check tar file
if [ ! -e redis-${REDIS_VERSION}.tar.gz ];then
 echo "downloading redis..."
 wget -c http://download.redis.io/releases/redis-${REDIS_VERSION}.tar.gz
fi

tar -zxvf redis-${REDIS_VERSION}.tar.gz
pushd redis-${REDIS_VERSION}
echo -n "install redis-${REDIS_VERSION}..."
make MALLOC=libc    #直接make会报错，需要
make test
make PREFIX=${REDIS_INSTALL_DIR} install # make install &> /dev/null
mkdir ${REDIS_INSTALL_DIR}
mkdir ${REDIS_INSTALL_DIR}/etc
mkdir ${REDIS_INSTALL_DIR}/logs
mkdir ${REDIS_DATA_DIR}
cp redis.conf ${REDIS_CONFIG_FILE}


if [ $? -eq 0 ];then
 make test &> /dev/null
 [ $? -eq 0 ] && echo "ok" || echo "failed"
else
 echo "instal redis-${REDIS_VERSION} failed."
 exit 1
fi

# init redis server
echo "${REDIS_PORT}
${REDIS_CONFIG_FILE}
${REDIS_LOG_FILE}
${REDIS_DATA_DIR}
${REDIS_EXEC}

" | utils/install_server.sh

popd

# get IP
if [ -f '/sbin/ifconfig' ];
then
    #echo 'ifconfig已经安装'
    localIp=$(/sbin/ifconfig |grep inet|grep -v 127.0.0.1|grep -v inet6|awk '{print $2}'|tr -d "addr:")
else
    yum -y install net-tools
    localIp=$(/sbin/ifconfig |grep inet|grep -v 127.0.0.1|grep -v inet6|awk '{print $2}'|tr -d "addr:")
fi
#echo $localIp

# config redis server
sed -i 's@bind 127.0.0.1@bind '"${localIp}"'@' ${REDIS_CONFIG_FILE}
sed -i 's@port 6379@port '"${REDIS_PORT}"'@g' ${REDIS_CONFIG_FILE}
sed -i 's@daemonize no@daemonize yes@g' ${REDIS_CONFIG_FILE}
sed -i "s@^# requirepass foobared@requirepass '"${REDIS_AUTH}"'@" ${REDIS_CONFIG_FILE}
#sed -i "s@^# masterauth <master-password>@masterauth '"${REDIS_AUTH}"'@" ${REDIS_CONFIG_FILE} #masterauth 设置

sed -i 's@pidfile /var/run/redis_6379.pid@'"${PIDFILE}"'@g' ${REDIS_CONFIG_FILE}
sed -i 's@logfile ""@'"${REDIS_LOG_FILE}"'@g' ${REDIS_CONFIG_FILE}
sed -i 's@dir ./@dir '"${REDIS_DATA_DIR}"'@g' ${REDIS_CONFIG_FILE}

ln -s /home/yunmas/redis/bin/* /usr/bin/
# start redis service
/etc/init.d/redis_${REDIS_PORT} restart