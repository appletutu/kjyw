#!/bin/bash
# Author:  liangping
# Create:  2020-07-02

DATE=$(date "+%Y-%m-%d %H:%M:%S")
hostip=$(/sbin/ifconfig |grep inet|grep -v 127.0.0.1|grep -v inet6|awk '{print $2}'|tr -d "addr:")

echo "$DATE 发生了 Notify 事件." >>sendmail.log

eventtype=$1
eventdescription=$2

echo -e "[主机地址]:$hostip\n[事件时间]:$DATE\n[事件标题]:redis异常\n[事件类型]:$eventtype\n[事件描述]:$eventdescription" |mail -s "【YUNMAS 告警信息！！！】" -c "18767126025@139.com" "hzliangping@139.com"
