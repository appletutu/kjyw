#!/bin/bash
#Date：2018-5-20 13:14:00
#Author Blog:
#	https://www.yangxingzhen.com
#	https://www.i7ti.cn
#Author WeChat：
#	微信公众号：小柒博客
#Author mirrors site:
#	https://mirrors.yangxingzhen.com
#About the Author
#	BY：YangXingZhen
#	Mail：xingzhen.yang@yangxingzhen.com
#	QQ：675583110
#Automatic configuration system initialization
 
source /etc/init.d/functions
 
System_Version=$(awk -F. '{print $1}' /etc/redhat-release |awk '{print $NF}')
Software="lrzsz openssh-server ntp ntpdate cmake gcc gcc-c++ zlib zlib-devel openssl openssl-devel pcre pcre-devel curl rsync gd perl sysstat man mtr openssl-perl subversion nscd"
 
# 检查脚本运行用户是否为Root
if [ $(id -u) != 0 ];then
	echo -e "\033[31mError! You must be root to run this script! \033[0m"
	exit 1
fi
 
function set_config_yum () {
# 配置系统使用163yum源
if [ ${System_Version} -eq 7 ];then
	yum -y install wget
	mkdir -p /etc/yum.repos.d/bak
	\mv /etc/yum.repos.d/*.repo /etc/yum.repos.d/bak
	wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.163.com/.help/CentOS7-Base-163.repo
	yum clean all
	yum makecache
else
	yum -y install wget
	mkdir -p /etc/yum.repos.d/bak
	\mv /etc/yum.repos.d/*.repo /etc/yum.repos.d/bak
	wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.163.com/.help/CentOS6-Base-163.repo
	yum clean all
	yum makecache
fi
}
 
function set_install_soft (){
# 安装epel源
yum -y install epel-release
# 安装bash命令tab自动补全组件
yum -y install bash-completion
# 安装vim编辑器
yum -y install vim screen lrzsz tree psmisc
# 安装压缩解压工具
yum -y install zip unzip bzip2 gdisk
# 安装网络及性能监控工具
yum -y install telnet net-tools sysstat iftop lsof iotop htop dstat
# 安装源码编译工具及开发组件
yum -y install ${Software}
# 禁用不需要的服务
systemctl stop postfix.service
systemctl disable postfix 
systemctl disable auditd
systemctl disable kdump
# 设置系统默认编辑器为vim
grep -qw "vim" /etc/profile
[ $? -ne 0 ] && echo "alias vi=vim" >>/etc/profile
source /etc/profile >/dev/null
}
 
function set_config_user (){
# 删除无效用户
userdel adm
userdel lp
userdel shutdown
userdel operator
userdel games
userdel uucp
# 删除无效用户组
groupdel adm
groupdel lp
groupdel games
}
 
function set_config_open_file () {
# 修改ulimit参数
\cp /etc/security/limits.conf /etc/security/limits.conf.bak
cat >>/etc/security/limits.conf <<EOF
* soft nproc 65535
* hard nproc 65535
* soft nofile 65535
* hard nofile 65535
EOF
echo "ulimit -SHn 65535" >> /etc/profile
echo "ulimit -SHn 65535" >> /etc/rc.local
}
 
function set_config_ntp (){
# 配置时区及时间同步
if [ "`cat /etc/crontab | grep ntpdate`" = "" ]; then
	echo "10 * * * * root /usr/sbin/ntpdate cn.pool.ntp.org >> /var/log/ntpdate.log" >> /etc/crontab
fi
rm -rf /etc/localtime
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
ntpdate cn.pool.ntp.org && hwclock -w
}
 
function set_config_sshd (){
# 配置SSH
sed -i "s/\#UseDNS yes/UseDNS no/g" /etc/ssh/sshd_config
sed -i "s/GSSAPIAuthentication yes/GSSAPIAuthentication no/g" /etc/ssh/sshd_config
}
 
function set_close_iptables (){
# 配置Selinux、Iptables(Firewalld)
if [ ${System_Version} -eq 7 ];then
	systemctl stop firewalld.service
	systemctl disable firewalld.service
	sed -i '/SELINUX/s/enforcing/disabled/g' /etc/selinux/config
	setenforce 0
else
	/etc/init.d/iptables stop
	chkconfig iptables off
	sed -i '/SELINUX/s/enforcing/disabled/g' /etc/selinux/config
	setenforce 0
fi
}
 
function set_config_passwd (){
# 设置密码最小过期天数
sed -i "/PASS_MIN_DAYS/s/0/80/" /etc/login.defs
# 设置密码最小长度
sed -i "/PASS_MIN_LEN/s/5/12/" /etc/login.defs
}
 
System_Value="net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.core.netdev_max_baklog =  32768
net.core.somaxconn = 32768
net.core.wmem_default = 8388608
net.core.rmem_default = 8388608
net.core.rmem_max = 16777216
net.core.wmem_max = 16777216
net.ipv4.ip_forward = 1
net.ipv4.tcp_max_syn_baklog = 65536
net.ipv4.tcp_timestamps = 0
net.ipv4.tcp_synack_retries = 2
net.ipv4.tcp_syn_retries = 2
net.ipv4.tcp_tw_recycle = 1
net.ipv4.tcp_tw_len = 1
net.ipv4.tcp_tw_reuse = 1
net.ipv4.tcp_mem = 94500000 915000000 927000000
net.ipv4.tcp_max_orphans = 3276800
net.ipv4.tcp_fin_timeout = 120
net.ipv4.tcp_keepalive_time = 120
net.ipv4.ip_local_port_range = 1024 65535
net.nf_conntrack_max = 16404388
net.netfilter.nf_conntrack_tcp_timeout_established = 10800
net.ipv4.tcp_max_tw_buckets = 30000
fs.file-max = 655350
kernel.sysrq = 0"
 
function set_config_kernel (){ 
# 配置内核参数
Sysctl_File="/etc/sysctl.conf"
if [ ${System_Version} -eq 6 ];then
	/etc/init.d/sshd restart
	if [ ! -f ${Sysctl_File} ];then
		touch ${Sysctl_File}
	fi
	if [ $(grep -wc "net.ipv4.tcp_max_tw_buckets" ${Sysctl_File}) -eq 0 ];then
		echo "${System_Value}" >${Sysctl_File}		
		/sbin/sysctl -p
	fi
else
	systemctl restart sshd.service
	if [ ! -f ${Sysctl_File} ];then
		touch ${Sysctl_File}
	fi
	if [ $(grep -wc "net.ipv4.tcp_max_tw_buckets" ${Sysctl_File}) -eq 0 ];then
		echo "${System_Value}" >${Sysctl_File}		
		/sbin/sysctl -p
	fi
fi
}
 
function main (){
set_config_yum
set_install_soft
set_config_user
set_config_open_file
set_config_ntp
set_config_sshd
set_close_iptables
set_config_passwd
set_config_kernel
}
 
main