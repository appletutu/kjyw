#!/bin/bash
# Filename:    centos7-init.sh
# Author:     zy
#判断是否为root用户
if [ `whoami` != "root" ];then
	echo " only root can run it"
exit 1
fi
#执行前提示
echo -e "\033[31m 这是centos7系统初始化脚本，将更新系统内核至最新版本，请慎重运行！\033[0m"
read -s -n1 -p "Press any key to continue or ctrl+C to cancel"
echo "Your inputs: $REPLY"
#0.网络配置
## Set IPADDR
ip_config()
{
cat >> /etc/sysconfig/network-scripts/ifcfg-ens33 << EOF
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=ens33
UUID=0c9ad6ee-b21d-493b-af70-367857ff8d0e
DEVICE=ens33
ONBOOT=yes
IPADDR=192.168.255.101
GATEWAY=192.168.255.2
NETMASK=255.255.255.0
DNS1=8.8.8.8
DNS2=114.114.114.114
EOF
}
## 添加公网的DNS
dns_config(){
cat >> /etc/resolv.conf << EOF
nameserver 114.114.114.114
EOF
}

#1.定义配置yum源的函数
yum_config(){
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
wget -O /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-7.repo
yum clean all && yum makecache
}
#2.定义配置NTP的函数
ntp_config(){
yum install -y chrony
systemctl start chronyd && systemctl enable chronyd
timedatectl set-timezone Asia/Shanghai && timedatectl set-ntp yes
}
#3.定义关闭防火墙的函数
close_firewalld(){
systemctl stop firewalld.service &> /dev/null
systemctl disable firewalld.service &> /dev/null
systemctl stop NetworkManager
systemctl disable NetworkManager
}
#4.定义关闭selinux的函数
close_selinux(){
setenforce 0
sed -i 's/enforcing/disabled/g' /etc/selinux/config
}
#5.定义安装常用工具的函数
yum_tools(){
yum install –y vim wget lrzsz rsync unzip bzip2 bzip2-devel tree dos2unix screen
yum install –y curl curl-devel telnet traceroute lynx iptraf bash-completion sysstat iftop lsof iotop iostat htop dstat
yum install –y gcc gcc-c++ make cmake autoconf openssl-devel openssl-perl net-tools tcp tcpdump nc mtr nmap ntpdate 
source /usr/share/bash-completion/bash_completion
}
#5.设置最大打开文件描述符数
set_limits(){
echo "ulimit -SHn 65535" >> /etc/rc.local
cat >> /etc/security/limits.conf << EOF
* soft nproc 65535
* hard nproc 65535
* soft nofile 65535
* hard nofile 65535
EOF
}
#6.定义升级最新内核的函数
#update_kernel (){
#rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
#rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm
rpm -ivh http://mirrors.aliyun.com/epel/epel-release-latest-7.noarch.rpm
#yum --enablerepo=elrepo-kernel install -y kernel-ml
#grub2-set-default 0
#grub2-mkconfig -o /boot/grub2/grub.cfg
#}
#执行脚本
main(){
    ip_config
    dns_config;
    yum_config;
    ntp_config;
    close_firewalld;
    close_selinux;
    yum_tools;
    set_limits;
 #   update_kernel;
}
main


source /etc/profile

# 使用tables键的时候补全systemctl命令
yum install -y bash-completion

# 安装killall命令
yum install -y 
